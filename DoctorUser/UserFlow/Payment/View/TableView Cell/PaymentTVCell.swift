//
//  PaymentTVCell.swift
//  DoctorUser
//
//  Created by VATSAL on 12/24/21.
//

import UIKit

class PaymentTVCell: UITableViewCell {
    
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblPrice : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setCellData(data:DataPaymentHistory) {
        lblName.text = data.subscription_plan_name
        lblDate.text = data.created_date
        lblPrice.text = data.price
    }
}
