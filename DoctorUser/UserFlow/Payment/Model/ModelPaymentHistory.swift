

import Foundation
struct ModelPaymentHistory : Codable {
	let msg : String?
	let data : [DataPaymentHistory]?
	let status : Bool?

	enum CodingKeys: String, CodingKey {

		case msg = "msg"
		case data = "data"
		case status = "status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		msg = try values.decodeIfPresent(String.self, forKey: .msg)
		data = try values.decodeIfPresent([DataPaymentHistory].self, forKey: .data)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
	}

}
struct DataPaymentHistory : Codable {
    let total_month : String?
    let holder_name : String?
    let expiry_date : String?
    let is_expired : String?
    let paymentType : String?
    let month_name : String?
    let modified_date : String?
    let user_id : String?
    let product_created : String?
    let expiry_year : String?
    let created_date : String?
    let _id : String?
    let email_id : String?
    let month : String?
    let expiry_month : String?
    let subscription_plan_name : String?
    let year : String?
    let subscription_plan_id : String?
    let mobile_no : String?
    let product_id : String?
    let card_number : String?
    let is_log : String?
    let payment_by : String?
    let product_paid : String?
    let price : String?
    let cvv : String?
    let is_auto_subscribed : String?
    let month_duration : String?
    let user_name : String?

    enum CodingKeys: String, CodingKey {

        case total_month = "total_month"
        case holder_name = "holder_name"
        case expiry_date = "expiry_date"
        case is_expired = "is_expired"
        case paymentType = "paymentType"
        case month_name = "month_name"
        case modified_date = "modified_date"
        case user_id = "user_id"
        case product_created = "product_created"
        case expiry_year = "expiry_year"
        case created_date = "created_date"
        case _id = "_id"
        case email_id = "email_id"
        case month = "month"
        case expiry_month = "expiry_month"
        case subscription_plan_name = "subscription_plan_name"
        case year = "year"
        case subscription_plan_id = "subscription_plan_id"
        case mobile_no = "mobile_no"
        case product_id = "product_id"
        case card_number = "card_number"
        case is_log = "is_log"
        case payment_by = "payment_by"
        case product_paid = "product_paid"
        case price = "price"
        case cvv = "cvv"
        case is_auto_subscribed = "is_auto_subscribed"
        case month_duration = "month_duration"
        case user_name = "user_name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        total_month = try values.decodeIfPresent(String.self, forKey: .total_month)
        holder_name = try values.decodeIfPresent(String.self, forKey: .holder_name)
        expiry_date = try values.decodeIfPresent(String.self, forKey: .expiry_date)
        is_expired = try values.decodeIfPresent(String.self, forKey: .is_expired)
        paymentType = try values.decodeIfPresent(String.self, forKey: .paymentType)
        month_name = try values.decodeIfPresent(String.self, forKey: .month_name)
        modified_date = try values.decodeIfPresent(String.self, forKey: .modified_date)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        product_created = try values.decodeIfPresent(String.self, forKey: .product_created)
        expiry_year = try values.decodeIfPresent(String.self, forKey: .expiry_year)
        created_date = try values.decodeIfPresent(String.self, forKey: .created_date)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        email_id = try values.decodeIfPresent(String.self, forKey: .email_id)
        month = try values.decodeIfPresent(String.self, forKey: .month)
        expiry_month = try values.decodeIfPresent(String.self, forKey: .expiry_month)
        subscription_plan_name = try values.decodeIfPresent(String.self, forKey: .subscription_plan_name)
        year = try values.decodeIfPresent(String.self, forKey: .year)
        subscription_plan_id = try values.decodeIfPresent(String.self, forKey: .subscription_plan_id)
        mobile_no = try values.decodeIfPresent(String.self, forKey: .mobile_no)
        product_id = try values.decodeIfPresent(String.self, forKey: .product_id)
        card_number = try values.decodeIfPresent(String.self, forKey: .card_number)
        is_log = try values.decodeIfPresent(String.self, forKey: .is_log)
        payment_by = try values.decodeIfPresent(String.self, forKey: .payment_by)
        product_paid = try values.decodeIfPresent(String.self, forKey: .product_paid)
        price = try values.decodeIfPresent(String.self, forKey: .price)
        cvv = try values.decodeIfPresent(String.self, forKey: .cvv)
        is_auto_subscribed = try values.decodeIfPresent(String.self, forKey: .is_auto_subscribed)
        month_duration = try values.decodeIfPresent(String.self, forKey: .month_duration)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
    }

}
