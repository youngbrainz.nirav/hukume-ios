

import Foundation
struct ModelCard : Codable {
	let msg : String?
	let status : Bool?
	let data : [DataCard]?

	enum CodingKeys: String, CodingKey {

		case msg = "msg"
		case status = "status"
		case data = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		msg = try values.decodeIfPresent(String.self, forKey: .msg)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		data = try values.decodeIfPresent([DataCard].self, forKey: .data)
	}

}
struct DataCard : Codable {
    let expiry_date : String?
    let modified_date : String?
    let _id : String?
    let user_id : String?
    let created_date : String?
    let month_name : String?
    let date : String?
    let holder_name : String?
    let year : String?
    let month : String?
    let card_number : String?
    let cvv : String?
    let is_default_card : String?

    enum CodingKeys: String, CodingKey {

        case expiry_date = "expiry_date"
        case modified_date = "modified_date"
        case _id = "_id"
        case user_id = "user_id"
        case created_date = "created_date"
        case month_name = "month_name"
        case date = "date"
        case holder_name = "holder_name"
        case year = "year"
        case month = "month"
        case card_number = "card_number"
        case cvv = "cvv"
        case is_default_card = "is_default_card"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        expiry_date = try values.decodeIfPresent(String.self, forKey: .expiry_date)
        modified_date = try values.decodeIfPresent(String.self, forKey: .modified_date)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        created_date = try values.decodeIfPresent(String.self, forKey: .created_date)
        month_name = try values.decodeIfPresent(String.self, forKey: .month_name)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        holder_name = try values.decodeIfPresent(String.self, forKey: .holder_name)
        year = try values.decodeIfPresent(String.self, forKey: .year)
        month = try values.decodeIfPresent(String.self, forKey: .month)
        card_number = try values.decodeIfPresent(String.self, forKey: .card_number)
        cvv = try values.decodeIfPresent(String.self, forKey: .cvv)
        is_default_card = try values.decodeIfPresent(String.self, forKey: .is_default_card)
    }

}
