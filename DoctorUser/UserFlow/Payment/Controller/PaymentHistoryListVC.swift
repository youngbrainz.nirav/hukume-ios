//
//  PaymentHistoryListVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/24/21.
//

import UIKit

class PaymentHistoryListVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var tblView : UITableView!
    
    //MARK: - Varibles
    var arrPaymentHistory:[DataPaymentHistory] = []
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        wsGetPaymentHistory()
        // Do any additional setup after loading the view.
    }
    //MARK: - Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    //MARK: - API Calling
    func wsGetPaymentHistory(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_USER_PAYMENT_HISTORY, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            self.arrPaymentHistory.removeAll()
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelPaymentHistory.self, from: data!)
                    self.arrPaymentHistory = responseModel.data ?? []
                    self.tblView.reloadData()
                }
                catch {
                    
                }
            }
        }
    }
}

//MARK:- TableView Delegate Method
extension PaymentHistoryListVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPaymentHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PaymentTVCell
        cell.setCellData(data: arrPaymentHistory[indexPath.row])
        return cell
    }
  
}

