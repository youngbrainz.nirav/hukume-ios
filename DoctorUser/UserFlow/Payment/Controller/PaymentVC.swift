//
//  PaymentVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/28/21.
//

import UIKit

class PaymentVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtCvv: UITextField!
    @IBOutlet weak var txtMonth: UITextField!
    @IBOutlet weak var txtYear: UITextField!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    
    //MARK:- Varibles
    var subscription_plan_id = ""
    var price = ""
    var isForChat = false
    var user_id = ""
    var user_name = ""
    var isFromChat = false
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        txtCvv.isSecureTextEntry = true
        // Do any additional setup after loading the view.
        self.txtCardNumber.delegate = self
        self.txtMonth.delegate = self
        self.txtYear.delegate = self
        btnSave.setImage(UIImage.init(named: "ic_check")?.sd_tintedImage(with: .themeColor), for: .selected)
        lblPrice.text = price
    }
    
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        btnSave.isSelected = !btnSave.isSelected
    }
    @IBAction func btnPayAction(_ sender: Any) {
        if checkValidatiom() {
                wsPaymentForSubscription()
                
            
        }
    }
    func checkValidatiom()->Bool{
        if txtName.isEmpty {
            Common().showAlert(strMsg: "Please enter card holder name.", view: self)
            return false
        } else  if txtCardNumber.isEmpty {
            Common().showAlert(strMsg: "Please enter card number.", view: self)
            return false
        }
        else  if txtMonth.isEmpty {
            Common().showAlert(strMsg: "Please enter month.", view: self)
           return false
       }
        else  if txtYear.isEmpty {
            Common().showAlert(strMsg: "Please enter year.", view: self)
           return false
       }
        else  if txtCvv.isEmpty {
            Common().showAlert(strMsg: "Please enter cvv.", view: self)
           return false
       }
        
        return true
    }
    //MARK: - API Calling
    func wsAddCard(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.device_type] = "1"
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.holder_name] = txtName.text
        dictParam[PARAMS.cvv] = txtCvv.text
        dictParam[PARAMS.card_number] = txtCardNumber.text
        dictParam[PARAMS.expiry_date] = (txtMonth.text!.count == 1 ? "0\(txtMonth.text!)":txtMonth.text!) + "/" + txtYear.text!
        
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.ADD_USER_CARD, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {

                if preferenceHelper.getUserType() == UserDefaultKeyForValue.Customeruser {
                    if self.isForChat {
                        self.wsStartConversation()
                    } else {
                    self.wsBookSession()
                    }
                } else {
                    preferenceHelper.setIsSubscription(true)
                    appDel.gotoHome()
                    
                }
            }
        }
    }
    func wsPaymentForSubscription(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.subscription_plan_id] = subscription_plan_id
        dictParam[PARAMS.holder_name] = txtName.text
        dictParam[PARAMS.cvv] = txtCvv.text
        dictParam[PARAMS.card_number] = txtCardNumber.text
        dictParam[PARAMS.expiry_date] = (txtMonth.text!.count == 1 ? "0\(txtMonth.text!)":txtMonth.text!) + "/" +  txtYear.text!
        
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.MAKE_SUBSCRIPTION_PAYMENT, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                if self.btnSave.isSelected {
                    self.wsAddCard()
                }
                else {
                    if preferenceHelper.getUserType() == UserDefaultKeyForValue.Customeruser {
                        if self.isForChat {
                            self.wsStartConversation()
                        } else {
                        self.wsBookSession()
                        }
                    } else {
                        
                        preferenceHelper.setIsSubscription(true)
                        appDel.gotoHome()
                        
                    }
                    
                    
                }
                
            }
        }
    }
    func wsStartConversation(){
        var dictParam : [String:Any] = [:]
        dictParam[PARAMS.sender_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.sender_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.recipient_id] = user_id
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.START_CONVERSATION, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response) {
                let data = response["data"] as! [String:Any]
                var conversationID = ""
                if let conv_id = data["conversation_id"] as? Int {
                    conversationID = "\(conv_id)"
                } else {
                    conversationID = data["conversation_id"] as? String ?? ""
                }
                if self.isFromChat {
                    if let vcs = self.navigationController?.viewControllers {
                        for vc in vcs {
                            if vc is ChatVC {
                                self.navigationController?.popToViewController(vc, animated: true)
                            }
                        }
                        
                    }
                } else {
                let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                vc.conversationId = conversationID
                vc.recipient_id = self.user_id
                vc.receiver_name = self.user_name
                vc.isFromPayment = true
                self.pushToVC(vc)
                }
            }
        }
    }
    func wsBookSession(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.category_id] = AppSingleton.shared.bookingObject.category_id
        dictParam[PARAMS.sub_category_id] = AppSingleton.shared.bookingObject.sub_category_id
        dictParam[PARAMS.availability_id] = AppSingleton.shared.bookingObject.availability_id
        dictParam[PARAMS.booking_date] = AppSingleton.shared.bookingObject.booking_date
        dictParam[PARAMS.service_provider_id] = AppSingleton.shared.bookingObject.service_provider_id
        dictParam[PARAMS.description] = AppSingleton.shared.bookingObject.description
        
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.MAKE_SESSION_BOOKING, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "ThankYouVC") as! ThankYouVC
                self.pushToVC(vc)
            }
        }
    }
}
//MARK:- UITextFieldDelegate Method
extension PaymentVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let currentText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) else { return true }
        
        if textField == txtCardNumber {
            if (currentText.utf16.count) <= 19{
                textField.setText(to: currentText.grouping(every: 4, with: " "), preservingCursor: true)
                return false
            }
        }else if textField == txtMonth {
            if (currentText.utf16.count) <= 2{
                textField.setText(to: currentText.grouping(every: 2, with: "/"), preservingCursor: true)
                return false
            }
        }else if textField == txtYear {
            if (currentText.utf16.count) <= 4{
                textField.setText(to: currentText.grouping(every: 4, with: "/"), preservingCursor: true)
                return false
            }
        }else if textField == txtCvv {
            if (currentText.utf16.count) == 3{
        // textField.setText(to: currentText.grouping(every: 4, with: "-"), preservingCursor: true)
                return false
            }
            else{
                return true
            }
        }else {
            
        }
        return false
    }
}
