//
//  CardListVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/28/21.
//

import UIKit

class CardListVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblCard : UITableView!
    @IBOutlet weak var heightConstTblcard : NSLayoutConstraint!
    @IBOutlet weak var btnAdd : UIView!
    @IBOutlet weak var lblPrice: UILabel!
    //MARK:- Varibles
    var arrCard:[DataCard] = []
    var subscription_plan_id = ""
    var selectedIndex = 0
    var cvv = ""
    var isForChat = false
    var user_id = ""
    var user_name = ""
    var isFromChat = false
    var amount = ""
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lblPrice.text = amount
        
        // Do any additional setup after loading the view.
      //  tblCard.tableFooterView =  btnAdd
        self.tblCard.estimatedRowHeight = 180
        self.tblCard.rowHeight = UITableView.automaticDimension
        if preferenceHelper.getUserType() == UserDefaultKeyForValue.Customeruser {
            wsGetsubscription()
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        wsGetCardList()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.heightConstTblcard.constant =  CGFloat(5 * 180) + 60//tblCard.contentSize.height
    }
    
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnPayAction(_ sender: Any) {
        if arrCard.count > 0 {
            if let cell = tblCard.cellForRow(at: IndexPath.init(row: selectedIndex, section: 0)) as? CardTVCell {
                if cell.txtCVV.isEmpty {
                    Common().showAlert(strMsg: "Please enter cvv.", view: self)
                } else {
                    cvv = cell.txtCVV.text!
                    wsPaymentForSubscription()
                }
            }
            
        
        } else {
            Common().showAlert(strMsg: "Please add card first.", view: self)
        }
    }
    @IBAction func btnAddNewAction(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.Payment, bundle: nil).instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
        vc.subscription_plan_id = subscription_plan_id
        vc.price = lblPrice.text!
        vc.isForChat = isForChat
        vc.user_id = user_id
        vc.user_name = user_name
        vc.isFromChat = isFromChat
        self.pushToVC(vc)
    }
    @objc func didTapDeleteCard(sender:UIButton) {
        wsDeleteCard(cardId: arrCard[sender.tag]._id ?? "")
    }
    @objc func didTapCheckMark(sender:UIButton) {
        sender.isSelected = true
        selectedIndex = sender.tag
        tblCard.reloadData()
    }
    //MARK: - API Calling
    func wsGetCardList(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_USER_CARD, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            self.arrCard.removeAll()
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelCard.self, from: data!)
                    self.arrCard = responseModel.data ?? []
                    self.tblCard.reloadData()
                    self.heightConstTblcard.constant = self.tblCard.contentSize.height
                    DispatchQueue.main.async {
                        self.heightConstTblcard.constant = self.tblCard.contentSize.height
                    }
                }
                catch {
                    
                }
            } else {
                self.tblCard.reloadData()
                self.heightConstTblcard.constant = self.tblCard.contentSize.height
                DispatchQueue.main.async {
                    self.heightConstTblcard.constant = self.tblCard.contentSize.height
                }
            }
        }
    }
    func wsDeleteCard(cardId:String){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.card_id] = cardId
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.DELETE_USER_CARD, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                self.wsGetCardList()
            }
        
    }
    }
    
    func wsBookSession(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.category_id] = AppSingleton.shared.bookingObject.category_id
        dictParam[PARAMS.sub_category_id] = AppSingleton.shared.bookingObject.sub_category_id
        dictParam[PARAMS.availability_id] = AppSingleton.shared.bookingObject.availability_id
        dictParam[PARAMS.booking_date] = AppSingleton.shared.bookingObject.booking_date
        dictParam[PARAMS.service_provider_id] = AppSingleton.shared.bookingObject.service_provider_id
        dictParam[PARAMS.description] = AppSingleton.shared.bookingObject.description
        
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.MAKE_SESSION_BOOKING, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "ThankYouVC") as! ThankYouVC
                self.pushToVC(vc)
            }
        }
    }
    func wsPaymentForSubscription(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.subscription_plan_id] = subscription_plan_id
        dictParam[PARAMS.card_id] = arrCard[selectedIndex]._id
        dictParam[PARAMS.cvv] = cvv
        dictParam[PARAMS.is_save_card] = 1
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.MAKE_SUBSCRIPTION_PAYMENT, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                if preferenceHelper.getUserType() == UserDefaultKeyForValue.Customeruser {
                    if self.isForChat {
                        self.wsStartConversation()
                    } else {
                    self.wsBookSession()
                    }
                } else {
                    preferenceHelper.setIsSubscription(true)
                    appDel.gotoHome()
                }
                
            }
        }
    }
    func wsStartConversation(){
        var dictParam : [String:Any] = [:]
        dictParam[PARAMS.sender_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.sender_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.recipient_id] = user_id
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.START_CONVERSATION, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response) {
                let data = response["data"] as! [String:Any]
                var conversationID = ""
                if let conv_id = data["conversation_id"] as? Int {
                    conversationID = "\(conv_id)"
                } else {
                    conversationID = data["conversation_id"] as? String ?? ""
                }
                if self.isFromChat {
                    self.navigationController?.popViewController(animated: true)
                } else {
                let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                vc.conversationId = conversationID
                vc.recipient_id = self.user_id
                vc.receiver_name = self.user_name
                vc.isFromPayment = true
                self.pushToVC(vc)
                }
            }
        }
    }
    func wsGetsubscription(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_SUBSCRIPTION_PLAN, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam,isShowLoading: false) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelSubscription.self, from: data!)
                    if let subData = responseModel.data?.first {
                        self.lblPrice.text = subData.price
                        self.subscription_plan_id = subData._id ?? ""
                    }
                    
                }
                catch {
                    
                }
                
            }
        }
    }
}
//MARK:- TableView Delegate Method
extension CardListVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCard.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CardTVCell
        cell.btnDeleteCard.tag = indexPath.row
        cell.btnCheck.tag = indexPath.row
        if indexPath.row == selectedIndex {
            cell.btnCheck.isSelected = true
        } else {
            cell.btnCheck.isSelected = false
        }
        cell.btnDeleteCard.addTarget(self, action: #selector(didTapDeleteCard(sender:)), for: .touchUpInside)
        cell.btnCheck.addTarget(self, action: #selector(didTapCheckMark(sender:)), for: .touchUpInside)
        cell.setData(card: arrCard[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
}

