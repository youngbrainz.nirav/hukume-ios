
import Foundation
struct ModelNotifications : Codable {
	let status : Bool?
	let data : [DataNotifications]?
	let msg : String?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case data = "data"
		case msg = "msg"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		data = try values.decodeIfPresent([DataNotifications].self, forKey: .data)
		msg = try values.decodeIfPresent(String.self, forKey: .msg)
	}

}
struct DataNotifications : Codable {
    let _id : String?
    let notification_type : String?
    let message : String?
    let from_id : String?
    let session_id : String?
    let from_user_profile : String?
    let from_user_name : String?
    let time : String?
    let chat_data:Last_chat_list?
    enum CodingKeys: String, CodingKey {

        case _id = "_id"
        case notification_type = "notification_type"
        case message = "message"
        case from_id = "from_id"
        case session_id = "session_id"
        case from_user_profile = "from_user_profile"
        case from_user_name = "from_user_name"
        case time = "time"
        case chat_data = "chat_data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        notification_type = try values.decodeIfPresent(String.self, forKey: .notification_type)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        from_id = try values.decodeIfPresent(String.self, forKey: .from_id)
        session_id = try values.decodeIfPresent(String.self, forKey: .session_id)
        from_user_profile = try values.decodeIfPresent(String.self, forKey: .from_user_profile)
        from_user_name = try values.decodeIfPresent(String.self, forKey: .from_user_name)
        time = try values.decodeIfPresent(String.self, forKey: .time)
        do {
        chat_data = try values.decodeIfPresent(Last_chat_list.self, forKey: .chat_data)
        } catch {
            chat_data = nil
        }
    }

}
