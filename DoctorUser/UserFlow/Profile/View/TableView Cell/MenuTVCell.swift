//
//  MenuTVCell.swift
//  DoctorUser
//
//  Created by VATSAL on 12/21/21.
//

import UIKit
import Cosmos

class MenuTVCell: UITableViewCell {

    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var btnRight : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class NotificationTVCell: UITableViewCell {

    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var imgView : UIImageView!
    var didTapDelete:((_ id: String)->())?
    var noti_id = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(data:DataNotifications) {
        lblName.text = data.message
        lblTime.text = data.time
        imgView.downloadedFrom(link: data.from_user_profile ?? "")
        noti_id = data._id ?? ""
    }
    @IBAction func btnDeleteAction(_ sender: Any) {
        didTapDelete!(noti_id)
    }
    

}

class SPTVCell: UITableViewCell {

    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var ratingView : CosmosView!
    @IBOutlet weak var viewColor : UIView!
    @IBOutlet weak var lblAvailable : UILabel!
    @IBOutlet weak var imgView : UIImageView!
    @IBOutlet weak var imgRating : UIImageView!
    @IBOutlet weak var lblRating : UILabel!
    @IBOutlet weak var viewShadow: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if ratingView != nil {
        ratingView.settings.fillMode = .half
        }
    }
    @IBOutlet weak var lblCategoryValue: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

//        self.ratingView.isUserInteractionEnabled = false
        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        if viewShadow != nil {
        viewShadow.addShadowOnBottom()
        }
        
    }

    func setData(data:DataSessionList) {
        lblCategoryValue.text = data.category_name ?? " "
        lblDate.text = data.booking_date
        lblTime.text = data.start_time
        lblName.text = data.user_name
        if ratingView != nil && !ratingView.isHidden{
            ratingView.rating = Double(data.avg_rating ?? "0") ?? 0
        }
        if lblRating != nil {
            lblRating.text = data.avg_rating
            imgRating.isHidden = data.avg_rating!.isEmpty || data.status == "3"
        }
        imgView.downloadedFrom(link: data.profile_image ?? "")
    }
    func setSPData(data:DataSPList) {
        lblName.text = data.user_name
        ratingView.rating = data.rating?.toDouble() ?? 0.0
        imgView.downloadedFrom(link: data.profile_image ?? "")
        
    }
    func setSPRating(data:Reviews){
        lblName.text = data.user_name
        ratingView.rating = data.rating?.toDouble() ?? 0.0
        imgView.downloadedFrom(link: data.profile_image ?? "")
        lblAvailable.text = data.description
    }
    
    
}

class CardTVCell: UITableViewCell {

    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblCardNumber : UILabel!
    @IBOutlet weak var lblMonth : UILabel!
    @IBOutlet weak var txtCVV : UITextField!
    @IBOutlet weak var btnCheck : UIButton!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var btnDeleteCard: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if txtCVV != nil {
            txtCVV.isSecureTextEntry = true
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
        // Configure the view for the selected state
    }
    func setData(card:DataCard) {
        lblName.text = card.holder_name
        let num = (card.card_number?.prefix(4) ?? "") + "-XXXX-XXXX-" + (card.card_number?.suffix(4) ?? "")
        lblCardNumber.text = String(num)
        lblMonth.text = card.expiry_date
    }
    func setDataAvailability(data:DataAvailability){
        lblMonth.text = (data.total_availability_min ?? "") //+ " min"
        lblName.text = (data.availability_from_time ?? "") + " to " + (data.availability_to_time ?? "")
        
    }

}
class CategoryTVCell: UITableViewCell {
    
    
    @IBOutlet weak var lblSubcategory: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnCheck.setImage(UIImage.init(named: "ic_check")?.sd_tintedImage(with: .themeColor)?.sd_resizedImage(with: CGSize.init(width: 35, height: 35), scaleMode: .aspectFill), for: .selected)
        btnCheck.setImage(UIImage.init(named: "ic_Uncheck")?.sd_resizedImage(with: CGSize.init(width: 30, height: 30), scaleMode: .aspectFill), for: .normal)
        btnCheck.isUserInteractionEnabled = false
    }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    

}
