//
//  MenuVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/21/21.
//

import UIKit

class MenuVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tblMenu : UITableView!
    @IBOutlet weak var viewBottom : UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    
    //MARK:- Varibles
    let arrMenu = ["My Sessions","Payment History","About Us","Share App","Review & Rating to App", "Deactivate Account"]
    
    
    //MARK:- View Lifc Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tblMenu.tableFooterView =  viewBottom
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblUserName.text = preferenceHelper.getUserName()
        imgUser.downloadedFrom(link: preferenceHelper.getUserPic())
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imgUser.applyCornerRadiusWith(radius: 10)
    }
    //MARK:- Button Action

    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnViewProfileAction(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.Profile, bundle: nil).instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func btnLogoutAction(_ sender: Any) {
        print("logout")
       // UserDefaults.standard.removeObject(forKey: UserDefaultKey.userType)
       
        alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "Are you sure you want to logout?") { (status) in
            if status {
                self.wsLogout()
            }
        }
        
    }
    
    //MARK:- API Calling
    func wsLogout(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.LOGOUT_USER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                appDel.gotoLogin()
            }
        }
    }
    func wsDeactivateAccount(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.DEACTIVATE_ACCOUNT, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                appDel.gotoLogin()
            }
        }
    }

}
//MARK:- TableView Delegate Method
extension MenuVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuTVCell
        let item = self.arrMenu[indexPath.row]
        cell.lblName.text = item
        return cell
    }
    
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("\(arrMenu[indexPath.row])")
        if indexPath.row == 0 {
            let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "SessionListVC") as! SessionListVC
            self.pushToVC(vc)
        }else if indexPath.row == 1{
            let vc = UIStoryboard(name: STORYBOARDNAME.Payment, bundle: nil).instantiateViewController(withIdentifier: "PaymentHistoryListVC") as! PaymentHistoryListVC
            self.pushToVC(vc)
        }else if indexPath.row == 2{
            let vc = UIStoryboard(name: STORYBOARDNAME.Profile, bundle: nil).instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
            self.pushToVC(vc)
        }else if indexPath.row == 3{
            guard let url = URL.init(string: APP_URL) else {return}
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url)
                    }
            
        }else if indexPath.row == 4{
            guard let url = URL.init(string: APP_URL) else {return}
                    if UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url)
                    }
        }
        else if indexPath.row == 5{
            print("Deactivate")
            alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "If you deactivate or remove your account, all your records including payment will be removed and you will be required to sign up again if you like to use the app. Do you want to proceed ?") { (status) in
                if status {
                    self.wsDeactivateAccount()
                }
            }
            
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    
    
}

