//
//  ProfileVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/21/21.
//

import UIKit

class ProfileVC: UIViewController {

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        wsGetProfileData()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imgUser.applyCornerRadiusWith(radius: 10)
    }
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnEditProfileAction(_ sender: Any) {
        
        let vc = UIStoryboard(name: STORYBOARDNAME.Profile, bundle: nil).instantiateViewController(withIdentifier: "EditiProfileVC") as! EditiProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func setProfileData(){
        lblUserName.text = preferenceHelper.getUserName()
        lblEmail.text = AppSingleton.shared.user?.email_id
        lblPhone.text = (AppSingleton.shared.user?.country_code ?? "") + (AppSingleton.shared.user?.mobile_no ?? "")
        lblAddress.text = AppSingleton.shared.user?.address
        imgUser.downloadedFrom(link: AppSingleton.shared.user?.profile_image ?? "")
    }
    func wsGetProfileData(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_USER_PROFILE, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelLogin.self, from: data!)
                    AppSingleton.shared.user = responseModel.data
                    preferenceHelper.setUserName((AppSingleton.shared.user?.first_name ?? "") + " " + (AppSingleton.shared.user?.last_name ?? ""))
                    preferenceHelper.setUserPic(AppSingleton.shared.user?.profile_image ?? "")
                    self.setProfileData()
                }
                catch {
                    
                }
            }
        }
    }
}
