//
//  ChangePasswordVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/22/21.
//

import UIKit

class ChangePasswordVC: UIViewController {

    
    //MARK:- Outlets
    @IBOutlet weak var lblOldPassword : UILabel!
    @IBOutlet weak var lblNewPassword : UILabel!
    @IBOutlet weak var lblConfirmNewPassword : UILabel!
    
    @IBOutlet weak var txtOldPassword : UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword : UITextField!
    
    @IBOutlet weak var viewOldPassword : UIView!
    @IBOutlet weak var viewPassword : UIView!
    @IBOutlet weak var viewConfirmPassword : UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        setData()
    }
    
    //MARK:- Set Data
    func setData(){
        
        self.lblOldPassword.isHidden =  true
        self.lblNewPassword.isHidden =  true
        self.lblConfirmNewPassword.isHidden =  true
        
        self.txtOldPassword.placeholder = "Old Password"
        self.txtPassword.placeholder = "New Password"
        self.txtConfirmPassword.placeholder = "Confirm NewPassword"
        
        txtOldPassword.delegate =  self
        txtPassword.delegate =  self
        txtConfirmPassword.delegate =  self
        
        txtPassword.rightView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 50, height: 40))
        txtPassword.rightViewMode = .always
        txtConfirmPassword.rightView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 50, height: 40))
        txtConfirmPassword.rightViewMode = .always
        txtOldPassword.rightView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 50, height: 40))
        txtOldPassword.rightViewMode = .always
        
    }
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSignupAction(_ sender: Any) {

        if checkValidation() {
            wsChangePassword()
        }
    }
    @IBAction func btnPasswordHideShowAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            txtPassword.isSecureTextEntry = false
        } else {
            txtPassword.isSecureTextEntry = true
        }
    }
    @IBAction func btnConfirmPasswordHideShowAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            txtConfirmPassword.isSecureTextEntry = false
        } else {
            txtConfirmPassword.isSecureTextEntry = true
        }
    }
    @IBAction func btnOldPasswordHideShowAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            txtOldPassword.isSecureTextEntry = false
        } else {
            txtOldPassword.isSecureTextEntry = true
        }
    }
}
//MARK:- TextField Delegate Methods
extension ChangePasswordVC : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
         if textField == txtOldPassword{
            self.lblOldPassword.isHidden =  false
            viewOldPassword.backgroundColor =  .white
            self.txtOldPassword.placeholder = ""
        }else if textField ==  txtPassword{
            self.lblNewPassword.isHidden =  false
            viewPassword.backgroundColor =  .white
            self.txtPassword.placeholder = ""
        }else if textField ==  txtConfirmPassword{
            self.lblConfirmNewPassword.isHidden =  false
            viewConfirmPassword.backgroundColor =  .white
            self.txtConfirmPassword.placeholder = ""
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtOldPassword{
            if txtOldPassword.text ==  ""{
                self.lblOldPassword.isHidden =  true
                self.txtOldPassword.placeholder = "Old Password"
                viewOldPassword.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblOldPassword.isHidden =  false
                viewOldPassword.backgroundColor =  .white

            }
            
        }else if textField ==  txtPassword{
            if txtPassword.text ==  ""{
                self.lblNewPassword.isHidden =  true
                self.txtPassword.placeholder = "New Password"
                viewPassword.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblNewPassword.isHidden =  false
                viewPassword.backgroundColor =  .white
            }
        }else if textField ==  txtConfirmPassword{
            if txtConfirmPassword.text ==  ""{
                self.lblConfirmNewPassword.isHidden =  true
                self.txtConfirmPassword.placeholder = "Confirm New Password"
                viewConfirmPassword.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblConfirmNewPassword.isHidden =  false
                viewConfirmPassword.backgroundColor =  .white
            }
        }
    }
    func checkValidation()->Bool{
        if txtOldPassword.isEmpty {
            Common().showAlert(strMsg: "Please enter old password.", view: self)
            return false
        }
       else if txtPassword.isEmpty {
            Common().showAlert(strMsg: "Please enter new password.", view: self)
            return false
        }
        else if txtConfirmPassword.isEmpty {
            Common().showAlert(strMsg: "Please confirm your password.", view: self)
            return false
        }
         else if !txtPassword.text!.isValidPassword() {
            Common().showAlert(strMsg: "Password must be at least 8 characters and one uppercase, one digit and one symbol.", view: self)
            return false
        } else if txtPassword.text != txtConfirmPassword.text{
            Common().showAlert(strMsg: "Password and confirm password must be same!", view: self)
            return false
        }
        else if txtPassword.text == txtOldPassword.text{
            Common().showAlert(strMsg: "Please enter new password.", view: self)
            return false
        }
        
        return true
        
    }
    
    func wsChangePassword(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.old_password] = txtOldPassword.text!
        dictParam[PARAMS.new_password] = txtPassword.text!
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.CHANGE_PASSWORD, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                Common.showAlertToVc_with_handler(vc: self, strMessage: (response["msg"] as? String ?? "")) {
                    self.wsLogout()
                }
            }
        }
    }
    func wsLogout(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.LOGOUT_USER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                appDel.gotoLogin()
            }
        }
    }
    
}
