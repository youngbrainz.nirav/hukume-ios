//
//  WebViewVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/24/21.
//

import UIKit
import WebKit

class WebViewVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var webView: WKWebView!
    
    //MARK:- Variables
    var strURL = ""
    var isFrom = ""
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        webView.configuration.userContentController.addUserScript(userScript)
        strURL = "<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>"
        //webView.loadHTMLString(strURL, baseURL: nil)
        wsGetAboutUs()
    }
    func wsGetAboutUs(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.page_type] = 1
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_PAGES, methodName: AlamofireHelper.GET_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response) {
                if let data = response["data"] as? [String:Any] {
                if let description = data["description"] as? String {
                    self.webView.loadHTMLString(description, baseURL: nil)
                }
                }
            }
        }
    }
    
    
    
}
