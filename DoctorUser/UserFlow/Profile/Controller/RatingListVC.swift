//
//  RatingListVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/27/21.
//

import UIKit

class RatingListVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblRating : UITableView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    
    //MARK:- Varibles
    var arrReview:[Reviews] = []
    var name = ""
    var category = ""
    var rating = ""
    var imgUrl = UIImage()
    //MARK:- View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
        // Do any additional setup after loading the view.
    
    }
    func setData(){
        lblUserName.text = name
        lblRating.text = rating
        lblCategory.text = category
        imgUser.image = imgUrl
    }
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
//MARK:- TableView Delegate Method
extension RatingListVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReview.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SPTVCell
        cell.setSPRating(data: arrReview[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}


