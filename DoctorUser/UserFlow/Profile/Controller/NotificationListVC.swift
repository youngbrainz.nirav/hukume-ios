//
//  NotificationListVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/22/21.
//

import UIKit

class NotificationListVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tblMenu : UITableView!
    
    var arrNotifications:[DataNotifications] = []
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        wsGetNotifications()
    }
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - API Calling
    func wsGetNotifications(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_NOTIFICATIONS, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            self.arrNotifications.removeAll()
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelNotifications.self, from: data!)
                    self.arrNotifications = responseModel.data ?? []
                }
                catch {
                    
                }
            }
            self.tblMenu.reloadData()
        }
    }
    func wsDeleteNotification(id:String){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.notification_id] = id
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.DELETE_NOTIFICATION, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response) {
                self.wsGetNotifications()
            }
        }
    }

}
//MARK:- TableView Delegate Method
extension NotificationListVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NotificationTVCell
        //cell.lblName.text =  "Lorem ipsum is simply dummy text Lorem ipsum is simply dummy textLorem ipsum is simply dummy text"
        cell.didTapDelete = { id in
            self.wsDeleteNotification(id: id)
        }
        cell.setData(data: arrNotifications[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let id = arrNotifications[indexPath.row].notification_type {
            switch id {
            case PUSH_NOTIFICATION.book_session:
                
                if let session_id = arrNotifications[indexPath.row].session_id {
                    appDel.gotoSessionDetail(id: session_id, isPastSession: false)
                }
                break
            case PUSH_NOTIFICATION.start_session:
                if let session_id = arrNotifications[indexPath.row].session_id {
                    appDel.gotoSessionDetail(id: session_id, isPastSession: false,isSP: false)
                }
                break
            case PUSH_NOTIFICATION.user_chat:
                if arrNotifications[indexPath.row].chat_data != nil {
                gotoChat(data: arrNotifications[indexPath.row].chat_data!, isSp: false)
                }
                break
            case PUSH_NOTIFICATION.provider_chat:
                if arrNotifications[indexPath.row].chat_data != nil {
                gotoChat(data: arrNotifications[indexPath.row].chat_data!, isSp: true)
                }
                break
            case PUSH_NOTIFICATION.admin_approve_provider_account:
                self.navigationController?.popViewController(animated: true)
                break
            case PUSH_NOTIFICATION.complete_session_provider:
                if let session_id = arrNotifications[indexPath.row].session_id {
                    appDel.gotoSessionDetail(id: session_id, isPastSession: true)
                }
                break
            case PUSH_NOTIFICATION.complete_session_user:
                if let session_id = arrNotifications[indexPath.row].session_id {
                    appDel.gotoSessionDetail(id: session_id, isPastSession: true,isSP: false)
                }
                break
            case PUSH_NOTIFICATION.cancel_session_provider:
                if let session_id = arrNotifications[indexPath.row].session_id {
                    appDel.gotoSessionDetail(id: session_id, isPastSession: true)
                }
                break
            case PUSH_NOTIFICATION.cancel_session_user:
                if let session_id = arrNotifications[indexPath.row].session_id {
                    appDel.gotoSessionDetail(id: session_id, isPastSession: true,isSP: false)
                }
                break
            case PUSH_NOTIFICATION.session_reminder_user:
                if let session_id = arrNotifications[indexPath.row].session_id {
                    appDel.gotoSessionDetail(id: session_id, isPastSession: true,isSP: false,isStartSession: true)
                }
                break
            case PUSH_NOTIFICATION.session_reminder_provider:
                if let session_id = arrNotifications[indexPath.row].session_id {
                    appDel.gotoSessionDetail(id: session_id, isPastSession: false,isStartSession: true)
                }
                break
            
            default:
                break
            }
        }
    }
    func gotoChat(data:Last_chat_list,isSp:Bool) {
        if isSp {
        let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            vc.conversationId = data.conversation_id ?? ""
            vc.recipient_id = data.user_id ?? ""
            vc.receiver_name = data.sender_name ?? ""
            self.pushToVC(vc)
        } else {
            let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            vc.conversationId = data.conversation_id ?? ""
            vc.recipient_id = data.user_id ?? ""
            vc.receiver_name = data.sender_name ?? ""
            self.pushToVC(vc)
        }

    }
}

