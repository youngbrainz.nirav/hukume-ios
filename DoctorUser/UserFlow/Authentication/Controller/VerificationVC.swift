//
//  VerificationVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/20/21.
//

import UIKit
import FirebaseAuth

class VerificationVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var txtVerifyCode: UITextField!
    @IBOutlet weak var txtVerifyCode1: UITextField!
    @IBOutlet weak var txtVerifyCode2: UITextField!
    @IBOutlet weak var txtVerifyCode3: UITextField!
    @IBOutlet weak var txtVerifyCode4: UITextField!
    @IBOutlet weak var txtVerifyCode5: UITextField!
    
    @IBOutlet weak var viewVerifyCode: UIView!
    @IBOutlet weak var viewVerifyCode1: UIView!
    @IBOutlet weak var viewVerifyCode2: UIView!
    @IBOutlet weak var viewVerifyCode3: UIView!
    @IBOutlet weak var viewVerifyCode4: UIView!
    @IBOutlet weak var viewVerifyCode5: UIView!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var btnResend:UIButton!
    var isEmailOtp = ""
    var seconds = 30
    var timer = Timer()
    var isTimerRunning = false
    var verificationID = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtVerifyCode.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        txtVerifyCode1.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        txtVerifyCode2.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        
        txtVerifyCode3.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        txtVerifyCode4.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        txtVerifyCode5.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        self.txtVerifyCode.delegate =  self
        self.txtVerifyCode1.delegate =  self
        self.txtVerifyCode2.delegate =  self
        self.txtVerifyCode3.delegate =  self
        self.txtVerifyCode4.delegate =  self
        self.txtVerifyCode5.delegate =  self
        self.txtVerifyCode.text =  "-"
        self.txtVerifyCode1.text =  "-"
        self.txtVerifyCode2.text =  "-"
        self.txtVerifyCode3.text =  "-"
        self.txtVerifyCode4.text =  "-"
        self.txtVerifyCode5.text =  "-"
        lblTimer.isHidden = false
        
        runTimer()
        
    }
    //MARK:- Timer Methods
    func runTimer() {
        seconds = 30
        btnResend.isHidden = true
         timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }

    
    @objc func updateTimer() {
        
        lblTimer.text = timeString(time: TimeInterval(seconds))
        if seconds > 0 {
            seconds -= 1
            lblTimer.isHidden = false
//            btnResend.isUserInteractionEnabled = false
//            btnResend.alpha = 0.4
//            btnResend.isHidden = true
            
        }else{
            lblTimer.isHidden = true
            btnResend.isHidden = false
            timer.invalidate()
//            btnResend.isHidden = false
//            btnResend.isUserInteractionEnabled = true
//            btnResend.alpha = 1
        }
        
    }
    func timeString(time:TimeInterval) -> String {
//        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnResendAction(_ sender: Any) {
        if isEmailOtp.isEmpty {
            self.verifyMobile(phoneNumber: RegisterSingleton.shared.country_code+RegisterSingleton.shared.mobile_no)
        } else {
            self.wsSendEmailOtp()
            
        }
    }
    @IBAction func btnVerifyAction(_ sender: Any) {
        if checkValidation(){
            let otp = txtVerifyCode.text! + txtVerifyCode1.text! + txtVerifyCode2.text! + txtVerifyCode3.text! + txtVerifyCode4.text! + txtVerifyCode5.text!
            if !isEmailOtp.isEmpty {
                if otp == isEmailOtp {
                    if preferenceHelper.getUserType() == UserDefaultKeyForValue.Customeruser {
                        self.wsRegister()
                    } else {
                        
                        let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "SPSearchVC") as! SPSearchVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                } else {
                    Common().showAlert(strMsg: "Please enter valid otp.", view: self)
                }
            } else {
                checkOtp(otp: otp)
            }
            
        }
    }
    func verifyMobile(phoneNumber: String)  {
        Common.showSpinner()
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                
            }
            else {
                print("Either denied or notDetermined")
            }
        }
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            Common.removeSpinner()
            if let error = error {
                Common().showAlert(strMsg: "Invalid mobile number", view: self)
                return
            }
            
            self.verificationID = verificationID ?? ""
            self.runTimer()
            
        }
    }
    func checkOtp(otp:String) {
        Common.showSpinner()
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID,
            verificationCode: otp)
        Auth.auth().signIn(with: credential) { (authResult, error) in
            Common.removeSpinner()
            if let error = error {
                Common().showAlert(strMsg: error.localizedDescription, view: self)
                return
            }
            if preferenceHelper.getUserType() == UserDefaultKeyForValue.Customeruser {
                self.wsRegister()
            } else {
                //wsRegisterSP()
                let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "SPSearchVC") as! SPSearchVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
    @objc func textFieldDidChange(_ textField: UITextField){
        let text = textField.text
        
        if (text?.utf16.count)! >= 1{
            switch textField{
            case txtVerifyCode:
                txtVerifyCode1.becomeFirstResponder()
            case txtVerifyCode1:
                txtVerifyCode2.becomeFirstResponder()
            case txtVerifyCode2:
                txtVerifyCode3.becomeFirstResponder()
            case txtVerifyCode3:
                txtVerifyCode4.becomeFirstResponder()
            case txtVerifyCode4:
                txtVerifyCode5.becomeFirstResponder()
            case txtVerifyCode5:
                txtVerifyCode5.resignFirstResponder()
            default:
                break
            }
        }else{
            
        }
    }
    
    func checkValidation()->Bool{
        if txtVerifyCode.isEmpty || txtVerifyCode1.isEmpty || txtVerifyCode2.isEmpty || txtVerifyCode3.isEmpty || txtVerifyCode4.isEmpty || txtVerifyCode5.isEmpty  {
            Common().showAlert(strMsg: "Please enter otp.", view: self)
            return false
        }
        return true
    }
    func wsSendEmailOtp(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.email_id] = RegisterSingleton.shared.email_id
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.user_name] = (RegisterSingleton.shared.first_name + " " + RegisterSingleton.shared.last_name)
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.SEND_OTP, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response) {
                self.isEmailOtp = response["otp"] as? String ?? ""
                self.runTimer()
            }
        }
    }
    func wsRegister(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.device_type] = "1"
        dictParam[PARAMS.device_token] = preferenceHelper.getDeviceToken()
        dictParam[PARAMS.email_id] = RegisterSingleton.shared.email_id
        dictParam[PARAMS.password] = RegisterSingleton.shared.password
        dictParam[PARAMS.mobile_no] = RegisterSingleton.shared.mobile_no
        dictParam[PARAMS.country_code] = RegisterSingleton.shared.country_code
        dictParam[PARAMS.first_name] = RegisterSingleton.shared.first_name
        dictParam[PARAMS.last_name] = RegisterSingleton.shared.last_name
        dictParam[PARAMS.address] = RegisterSingleton.shared.address
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.latitude] = RegisterSingleton.shared.latitude
        dictParam[PARAMS.longitude] = RegisterSingleton.shared.longitude
        dictParam[PARAMS.country_code_info] = RegisterSingleton.shared.country_code_info
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.REGISTER_USER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelLogin.self, from: data!)
                    preferenceHelper.setUserId(responseModel.data?._id ?? "")
                    preferenceHelper.setSessionToken(responseModel.data?.session_token ?? "")
                    AppSingleton.shared.user = responseModel.data
                    preferenceHelper.setUserName((AppSingleton.shared.user?.first_name ?? "") + " " + (AppSingleton.shared.user?.last_name ?? ""))
                    preferenceHelper.setUserPic(AppSingleton.shared.user?.profile_image ?? "")
                    appDel.gotoHome()
                }
                catch {
                    print(WebService.REGISTER_USER + " error")
                }
                
            }
        }
    }
    
}
//MARK:- TextFileld Delegate Methods
extension VerificationVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtVerifyCode{
            textField.text = ""
            viewVerifyCode.backgroundColor = UIColor(red: 243 / 255, green: 244 / 255, blue: 245 / 255, alpha: 1.0)
        }else if textField ==  txtVerifyCode1{
            textField.text = ""
            viewVerifyCode1.backgroundColor = UIColor(red: 243 / 255, green: 244 / 255, blue: 245 / 255, alpha: 1.0)
        }else if textField ==  txtVerifyCode2{
            textField.text = ""
            viewVerifyCode2.backgroundColor = UIColor(red: 243 / 255, green: 244 / 255, blue: 245 / 255, alpha: 1.0)
        }else if textField ==  txtVerifyCode3{
            textField.text = ""
            viewVerifyCode3.backgroundColor = UIColor(red: 243 / 255, green: 244 / 255, blue: 245 / 255, alpha: 1.0)
        }
        else if textField ==  txtVerifyCode4{
            textField.text = ""
            viewVerifyCode4.backgroundColor = UIColor(red: 243 / 255, green: 244 / 255, blue: 245 / 255, alpha: 1.0)
        }
        else if textField ==  txtVerifyCode5{
            textField.text = ""
            viewVerifyCode5.backgroundColor = UIColor(red: 243 / 255, green: 244 / 255, blue: 245 / 255, alpha: 1.0)
        }
        
    }
}
