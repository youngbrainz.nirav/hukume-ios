//
//  LoginVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/19/21.
//

import UIKit
import Foundation

class LoginVC: UIViewController {
    
    
    //MARK: - Outlets
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var lblPassword : UILabel!
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtPassword : UITextField!
    @IBOutlet weak var viewEmail : UIView!
    @IBOutlet weak var viewPassword : UIView!
    

    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setData()
    }
    
    //MARK: - Set Data
    func setData(){
        //self.lblEmail.isHidden =  true
        //self.lblPassword.isHidden =  true
        
        let strName =  "HubNet"
        let strTitle = "To"
        
        let strNew = "\(strTitle) \(strName)"
        let attributedWithTextColor: NSAttributedString = strNew.attributedStringWithColor([strName], color: UIColor(red: 8 / 255, green: 92 / 255, blue: 252 / 255, alpha: 1.0))
    //    lblName.attributedText =  attributedWithTextColor
        
        txtEmail.delegate =  self
        txtPassword.delegate =  self
        txtPassword.rightView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 50, height: 40))
        txtPassword.rightViewMode = .always
    }
    //MARK: - Button Action
    
    @IBAction func btnPasswordHideShowAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            txtPassword.isSecureTextEntry = false
        } else {
            txtPassword.isSecureTextEntry = true
        }
    }
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnForgotPasswordAction(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.Authentication, bundle: nil).instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @IBAction func btnSignupAction(_ sender: Any) {
        let value = preferenceHelper.getUserType()
            print(value)
            
            if value == UserDefaultKeyForValue.Customeruser{
                let vc = UIStoryboard(name: STORYBOARDNAME.Authentication, bundle: nil).instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let vc = UIStoryboard(name: STORYBOARDNAME.Authentication, bundle: nil).instantiateViewController(withIdentifier: "SPSignupVC") as! SPSignupVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        

        

    }
    @IBAction func btnLoginAction(_ sender: Any) {
        
        if checkValidation() {
            wsLogin()
        }
        

    }
    func checkValidation()->Bool{
       
        if txtEmail.isEmpty{
            
            Common().showAlert(strMsg: "Please enter email.", view: self)
            return false
        } else if txtPassword.isEmpty {
            Common().showAlert(strMsg: "Please enter password.", view: self)
            return false
        } else if !txtEmail.text!.isEmail() {
            Common().showAlert(strMsg: "Please enter valid email.", view: self)
            return false
        }
//        else if !txtPassword.text!.isValidPassword() {
//            Common().showAlert(strMsg: "Password must be at least 8 characters.", view: self)
//            return false
//        }
        
        return true
        
    }
  //MARK: - API Calling
    func wsLogin(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.device_type] = "1"
        dictParam[PARAMS.device_token] = preferenceHelper.getDeviceToken()
        dictParam[PARAMS.email_id] = txtEmail.text
        dictParam[PARAMS.password] = txtPassword.text
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.LOGIN_USER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelLogin.self, from: data!)
                    preferenceHelper.setUserId(responseModel.data?._id ?? "")
                    preferenceHelper.setSessionToken(responseModel.data?.session_token ?? "")
                    AppSingleton.shared.user = responseModel.data
                    preferenceHelper.setUserName((AppSingleton.shared.user?.first_name ?? "") + " " + (AppSingleton.shared.user?.last_name ?? ""))
                    preferenceHelper.setUserPic(AppSingleton.shared.user?.profile_image ?? "")
                    if preferenceHelper.getUserType() == UserDefaultKeyForValue.SPUser {
                        preferenceHelper.setIsSubscription(AppSingleton.shared.user?.isSubscribedUser == 1)
                    }
                    appDel.gotoHome()
                }
                catch {
                    print(WebService.LOGIN_USER + " error")
                }
                
            }
        }
    }
    
    
}
//MARK: - TextField Delegate Methods
extension LoginVC : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtEmail{
            self.lblEmail.isHidden =  false
            viewEmail.backgroundColor =  .white
            self.txtEmail.placeholder = ""
        }else if textField ==  txtPassword{
            self.lblPassword.isHidden =  false
            viewPassword.backgroundColor =  .white
            self.txtPassword.placeholder = "" 
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtEmail{
            if txtEmail.text ==  ""{
              //  self.lblEmail.isHidden =  true
                self.txtEmail.placeholder = "Email"
                viewEmail.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblEmail.isHidden =  false
                viewEmail.backgroundColor =  .white

            }
            
        }else if textField ==  txtPassword{
            if txtPassword.text ==  ""{
              //  self.lblPassword.isHidden =  true
                self.txtPassword.placeholder = "Password"
                viewPassword.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblPassword.isHidden =  false
                viewPassword.backgroundColor =  .white
            }
        }
    }
    
}
