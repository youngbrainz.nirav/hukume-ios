//
//  ForgotPasswordVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/19/21.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var viewEmail : UIView!

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        txtEmail.delegate =  self
    }
    
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSendAction(_ sender: Any) {
            //self.navigationController?.popViewController(animated: true)
      if txtEmail.isEmpty{
             
            Common().showAlert(strMsg: "Please enter email.", view: self)
            
         }
        else if !txtEmail.text!.isEmail() {
            Common().showAlert(strMsg: "Please enter valid email.", view: self)
           
        } else {
        wsForgetPassword()
        }
    }
}
//MARK:- TextField Delegate Methods
extension ForgotPasswordVC : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtEmail{
            
            viewEmail.backgroundColor =  .white
            self.txtEmail.placeholder = ""
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtEmail{
            if txtEmail.text ==  ""{
                viewEmail.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                viewEmail.backgroundColor =  .white

            }
            
        }
    }
    func wsForgetPassword(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.email_id] = txtEmail.text
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.FORGOT_PASSWORD, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response,isSuccessToast: true) {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
}
