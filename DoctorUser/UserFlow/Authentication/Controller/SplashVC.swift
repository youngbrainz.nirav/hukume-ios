//
//  SplashVC.swift
//  DoctorUser
//
//  Created by Youngbrainz Mac Air on 08/08/22.
//

import UIKit

class SplashVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            let vc = UIStoryboard(name: STORYBOARDNAME.Authentication, bundle: nil).instantiateViewController(withIdentifier: "UserSelectionVC") as! UserSelectionVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    

   

}
