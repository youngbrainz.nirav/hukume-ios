//
//  SignupVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/20/21.
//

import UIKit
import CountryPickerView
import GooglePlaces
import GooglePlacePicker
import FirebaseAuth
class SPSignupVC: UIViewController,CountryPickerViewDelegate,GMSAutocompleteViewControllerDelegate {
    
    
    
    //MARK:- Outlets
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblFirstName : UILabel!
    @IBOutlet weak var lblLastName : UILabel!
    @IBOutlet weak var lblPhone : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var lblAddress : UILabel!
    @IBOutlet weak var lblPassword : UILabel!
    @IBOutlet weak var lblConfirmPassword : UILabel!
    
    @IBOutlet weak var txtFirstName : UITextField!
    @IBOutlet weak var txtLastName : UITextField!
    @IBOutlet weak var txtPhone : UITextField!
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtAddress : UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword : UITextField!
    
    @IBOutlet weak var viewFirstName : UIView!
    @IBOutlet weak var viewLastName : UIView!
    @IBOutlet weak var viewPhone : UIView!
    @IBOutlet weak var viewEmail : UIView!
    @IBOutlet weak var viewAddress : UIView!
    @IBOutlet weak var viewPassword : UIView!
    @IBOutlet weak var viewConfirmPassword : UIView!
    @IBOutlet weak var btnCountryCode: UIButton!
    let countryPickerView_1 = CountryPickerView()

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setData()
        let currentLocale = NSLocale.current
        let countryCode = currentLocale.regionCode
        let code = countryPickerView_1.countries.filter({$0.code == (countryCode)})
        RegisterSingleton.shared.country_code_info = countryCode ?? ""
        btnCountryCode.setTitle("  \(code.first?.phoneCode ?? "+12")", for: .normal)
        btnCountryCode.setImage(code.first?.flag, for: .normal)
        
        btnCountryCode.titleLabel?.font = FontHelper.Font(size: 17, style: .SemiBold)
        
    }
    
    //MARK: - Set Data
    func setData(){
//        self.lblFirstName.isHidden =  true
//        self.lblLastName.isHidden =  true
//        self.lblPhone.isHidden =  true
//        self.lblEmail.isHidden =  true
//        self.lblAddress.isHidden =  true
//        self.lblPassword.isHidden =  true
//        self.lblConfirmPassword.isHidden =  true
        
        countryPickerView_1.delegate = self
        let strName =  "HubNet"
        let strTitle = "To"
        
        let strNew = "\(strTitle) \(strName)"
        let attributedWithTextColor: NSAttributedString = strNew.attributedStringWithColor([strName], color: UIColor(red: 8 / 255, green: 92 / 255, blue: 252 / 255, alpha: 1.0))
      //  lblTitle.attributedText =  attributedWithTextColor
        lblTitle.text = ""
        txtFirstName.delegate =  self
        txtLastName.delegate =  self
        txtPhone.delegate =  self
        txtEmail.delegate =  self
        txtAddress.delegate =  self
        txtPassword.delegate =  self
        txtConfirmPassword.delegate =  self
        txtPassword.rightView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 50, height: 40))
        txtPassword.rightViewMode = .always
        txtConfirmPassword.rightView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 50, height: 40))
        txtConfirmPassword.rightViewMode = .always
    }
    //MARK:- Button Action
    @IBAction func btnLoginAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSignupAction(_ sender: Any) {
        if checkValidation() {
            wsCheckUserExist()
        }
    }
    @IBAction func onClickBtnCountryCode(_ sender: Any) {
        countryPickerView_1.showCountriesList(from: self)
    }
    @IBAction func btnPasswordHideShowAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            txtPassword.isSecureTextEntry = false
        } else {
            txtPassword.isSecureTextEntry = true
        }
    }
    @IBAction func btnConfirmPasswordHideShowAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            txtConfirmPassword.isSecureTextEntry = false
        } else {
            txtConfirmPassword.isSecureTextEntry = true
        }
    }
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        btnCountryCode.setTitle("  "+country.phoneCode, for: .normal)
        btnCountryCode.setImage(country.flag, for: .normal)
        RegisterSingleton.shared.country_code_info = country.code
    }
}
//MARK:- TextField Delegate Methods
extension SPSignupVC : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtAddress {
            openAutoCompletePlace()
            return false
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtFirstName{
            self.lblFirstName.isHidden =  false
            viewFirstName.backgroundColor =  .white
            self.txtFirstName.placeholder = ""
        }else if textField == txtLastName{
            self.lblLastName.isHidden =  false
            viewLastName.backgroundColor =  .white
            self.txtLastName.placeholder = ""
        }else if textField == txtPhone{
            self.lblPhone.isHidden =  false
            viewPhone.backgroundColor =  .white
            self.txtPhone.placeholder = ""
        }else if textField == txtEmail{
            self.lblEmail.isHidden =  false
            viewEmail.backgroundColor =  .white
            self.txtEmail.placeholder = ""
        }else if textField == txtAddress{
            self.lblAddress.isHidden =  false
            viewAddress.backgroundColor =  .white
            self.txtAddress.placeholder = ""
        }else if textField ==  txtPassword{
            self.lblPassword.isHidden =  false
            viewPassword.backgroundColor =  .white
            self.txtPassword.placeholder = ""
        }else if textField ==  txtConfirmPassword{
            self.lblConfirmPassword.isHidden =  false
            viewConfirmPassword.backgroundColor =  .white
            self.txtConfirmPassword.placeholder = ""
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtFirstName || textField == txtLastName {
            if !string.isAllChar() || string.isEmpty {
                return true
            } else {
                return false
            }
        }
        if textField == txtPhone {
            if (string.isAllDigits()) || string.isEmpty   {
                return true
            } else {
               return false
            }
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtFirstName{
            if txtFirstName.text ==  ""{
               // self.lblFirstName.isHidden =  true
                self.txtFirstName.placeholder = "First Name"
                viewFirstName.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblFirstName.isHidden =  false
                viewFirstName.backgroundColor =  .white

            }
            
        }else if textField == txtLastName{
            if txtLastName.text ==  ""{
              //  self.lblLastName.isHidden =  true
                self.txtLastName.placeholder = "Last Name"
                viewLastName.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblLastName.isHidden =  false
                viewLastName.backgroundColor =  .white

            }
            
        }else if textField == txtPhone{
            if txtPhone.text ==  ""{
              //  self.lblPhone.isHidden =  true
                self.txtPhone.placeholder = "Phone Number"
                viewPhone.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblPhone.isHidden =  false
                viewPhone.backgroundColor =  .white

            }
            
        }else if textField == txtEmail{
            if txtEmail.text ==  ""{
               // self.lblEmail.isHidden =  true
                self.txtEmail.placeholder = "Email"
                viewEmail.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblEmail.isHidden =  false
                viewEmail.backgroundColor =  .white

            }
            
        }else if textField == txtAddress{
            if txtAddress.text ==  ""{
               // self.lblAddress.isHidden =  true
                self.txtAddress.placeholder = "Address"
                viewAddress.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblAddress.isHidden =  false
                viewAddress.backgroundColor =  .white

            }
            
        }else if textField ==  txtPassword{
            if txtPassword.text ==  ""{
               // self.lblPassword.isHidden =  true
                self.txtPassword.placeholder = "Password"
                viewPassword.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblPassword.isHidden =  false
                viewPassword.backgroundColor =  .white
            }
        }else if textField ==  txtConfirmPassword{
            if txtConfirmPassword.text ==  ""{
              //  self.lblConfirmPassword.isHidden =  true
                self.txtConfirmPassword.placeholder = "Confirm Password"
                viewConfirmPassword.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblConfirmPassword.isHidden =  false
                viewConfirmPassword.backgroundColor =  .white
            }
        }
    }
    func openAutoCompletePlace(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        txtAddress.text = place.formattedAddress
        RegisterSingleton.shared.latitude = String(place.coordinate.latitude)
        RegisterSingleton.shared.longitude = String(place.coordinate.longitude)
        print(RegisterSingleton.shared.latitude)
        print(RegisterSingleton.shared.longitude)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("cancelled")
        dismiss(animated: true, completion: nil)
    }
    func checkValidation()->Bool{
       
        if txtFirstName.isEmpty {
            Common().showAlert(strMsg: "Please enter first name.", view: self)
            return false
        }
        else if txtLastName.isEmpty{
             
            Common().showAlert(strMsg: "Please enter last name.", view: self)
             return false
         }
        else if txtPhone.isEmpty{
             
            Common().showAlert(strMsg: "Please enter phone number.", view: self)
             return false
         }
        else if txtEmail.isEmpty{
             
            Common().showAlert(strMsg: "Please enter email.", view: self)
             return false
         }
        
       else if txtAddress.isEmpty{
            
        Common().showAlert(strMsg: "Please enter address.", view: self)
            return false
        }
       else if txtPassword.isEmpty {
            Common().showAlert(strMsg: "Please enter password.", view: self)
            return false
        }
        else if txtConfirmPassword.isEmpty {
            Common().showAlert(strMsg: "Please confirm your password.", view: self)
            return false
        }
        else if !txtEmail.text!.isEmail() {
            Common().showAlert(strMsg: "Please enter valid email.", view: self)
            return false
        }
         else if !txtPassword.text!.isValidPassword() {
            Common().showAlert(strMsg: "Password must be at least 8 characters and one uppercase, one digit and one symbol.", view: self)
            return false
        } else if txtPassword.text != txtConfirmPassword.text{
            Common().showAlert(strMsg: "Password and confirm password must be same!", view: self)
            return false
        }
        else if txtPhone.text!.count < 8 {
            Common().showAlert(strMsg: "Phone number should be at least 8 digit.", view: self)
            return false
        }
        return true
        
    }
    func verifyMobile(phoneNumber: String)  {
        Common.showSpinner()
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                
            }
            else {
                print("Either denied or notDetermined")
            }
        }
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            Common.removeSpinner()
            if let error = error {
                Common().showAlert(strMsg: "Invalid mobile number", view: self)
                return
            }
            let vc = UIStoryboard(name: STORYBOARDNAME.Authentication, bundle: nil).instantiateViewController(withIdentifier: "VerificationVC") as! VerificationVC
            vc.verificationID = verificationID ?? ""
                   self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    func setSingleton() {
        RegisterSingleton.shared.first_name = txtFirstName.text!
        RegisterSingleton.shared.last_name = txtLastName.text!
        RegisterSingleton.shared.address = txtAddress.text!
        RegisterSingleton.shared.email_id = txtEmail.text!
        RegisterSingleton.shared.mobile_no = txtPhone.text!
        RegisterSingleton.shared.password = txtPassword.text!
        RegisterSingleton.shared.country_code = btnCountryCode.titleLabel?.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
    }
    func openVerificationDialog(){
        let dialog = CustomVerificationDialog.showCustomVerificationDialog()
        dialog.onClickLeftButton = {
            dialog.removeFromSuperview()
        }
        dialog.onClickRightButton = { str in
            dialog.removeFromSuperview()
            if str == "Email" {
                self.wsSendEmailOtp()
            } else {
                
                self.verifyMobile(phoneNumber: RegisterSingleton.shared.country_code+RegisterSingleton.shared.mobile_no)
            }
            
        }
    }
    
    //MARK: - API Calling
    func wsCheckUserExist(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.email_id] = txtEmail.text!
        dictParam[PARAMS.mobile_no] = txtPhone.text!
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.CHECK_USER_EXIST, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                self.setSingleton()
                self.openVerificationDialog()
                //self.verifyMobile(phoneNumber: RegisterSingleton.shared.country_code+RegisterSingleton.shared.mobile_no)
//                let vc = UIStoryboard(name: STORYBOARDNAME.Authentication, bundle: nil).instantiateViewController(withIdentifier: "VerificationVC") as! VerificationVC
//                       self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
    }
    func wsSendEmailOtp(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.email_id] = txtEmail.text!
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.user_name] = (txtFirstName.text! + " " + txtLastName.text!)
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.SEND_OTP, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response) {
                let vc = UIStoryboard(name: STORYBOARDNAME.Authentication, bundle: nil).instantiateViewController(withIdentifier: "VerificationVC") as! VerificationVC
                vc.isEmailOtp = response["otp"] as? String ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
