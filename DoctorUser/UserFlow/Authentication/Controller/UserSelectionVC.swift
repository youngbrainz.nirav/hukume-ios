//
//  UserSelectionVC.swift
//  DoctorUser
//
//  Created by VATSAL on 1/15/22.
//

import UIKit

class UserSelectionVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var lblSP : UILabel!
    @IBOutlet weak var lblCustomer : UILabel!
    @IBOutlet weak var viewSP : UIView!
    @IBOutlet weak var viewCustomer : UIView!
    
    // MARK: - Varibles
    var strValue = ""
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if !preferenceHelper.getUserId().isEmpty {
            appDel.gotoHome()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // self.lblSP.textColor =  .black
        //self.lblCustomer.textColor =  .black
        self.viewSP.backgroundColor = .white
        self.viewCustomer.backgroundColor = .white
    }

    
    // MARK: - Button Action
    @IBAction func btnCustomerAction(_ sender: Any) {
        
        strValue =  UserDefaultKeyForValue.Customeruser
        self.lblSP.textColor =  .black
        self.lblCustomer.textColor = UIColor(red: 8 / 255, green: 92 / 255, blue: 252 / 255, alpha: 1.0)
        self.viewCustomer.backgroundColor = .white
        self.viewSP.backgroundColor = UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
       // UserDefaults.standard.setValue(UserDefaultKeyForValue.Customeruser, forKey: UserDefaultKey.userType)
        preferenceHelper.setUserType(value: UserDefaultKeyForValue.Customeruser)
        let vc = UIStoryboard(name: STORYBOARDNAME.Authentication, bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnSPAction(_ sender: Any) {
        strValue = UserDefaultKeyForValue.SPUser
        self.lblCustomer.textColor =  .black
        self.lblSP.textColor = UIColor(red: 8 / 255, green: 92 / 255, blue: 252 / 255, alpha: 1.0)
        self.viewSP.backgroundColor = .white
        self.viewCustomer.backgroundColor = UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
        
       // UserDefaults.standard.setValue(UserDefaultKeyForValue.SPUser, forKey: UserDefaultKey.userType)
        preferenceHelper.setUserType(value: UserDefaultKeyForValue.SPUser)
        let vc = UIStoryboard(name: STORYBOARDNAME.Authentication, bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

    @IBAction func btnNextAction(_ sender: Any) {
        
        if strValue ==  UserDefaultKeyForValue.Customeruser{
            //UserDefaults.standard.setValue(UserDefaultKeyForValue.Customeruser, forKey: UserDefaultKey.userType)
            preferenceHelper.setUserType(value: UserDefaultKeyForValue.Customeruser)
            let vc = UIStoryboard(name: STORYBOARDNAME.Authentication, bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if strValue == UserDefaultKeyForValue.SPUser{
            //UserDefaults.standard.setValue(UserDefaultKeyForValue.SPUser, forKey: UserDefaultKey.userType)
            preferenceHelper.setUserType(value: UserDefaultKeyForValue.SPUser)
            let vc = UIStoryboard(name: STORYBOARDNAME.Authentication, bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if strValue == ""{
            let okAction = UIAlertAction(title: "Ok", style: .default) { action in
                print("ok")
            }
            showAlert(title: "Information", msg: "Please select your profession", type: .alert, actions: [okAction])
            
            
        }
        
    }

}
