

import Foundation
struct ModelLogin : Codable {
	let data : DataUser?
	let status : Bool?
	let msg : String?

	enum CodingKeys: String, CodingKey {

		case data = "data"
		case status = "status"
		case msg = "msg"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		data = try values.decodeIfPresent(DataUser.self, forKey: .data)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		msg = try values.decodeIfPresent(String.self, forKey: .msg)
	}

}
struct DataUser : Codable {
    let year_of_experience : String?
    let profile_image : String?
    let month_name : String?
    let modified_date : String?
    let category_name : String?
    let month : String?
    let created_date : String?
    let _id : String?
    let email_id : String?
    let last_name : String?
    let session_token : String?
    let year : String?
    let email_token : String?
    let device_token : String?
    let mobile_no : String?
    let user_type : String?
    let sub_category_ids : String?
    let user_status : String?
    let sub_category_name : String?
    let about_you : String?
    let password : String?
    let category_id : String?
    let device_type : String?
    let address : String?
    let first_name : String?
    let user_id : String?
    let business_name: String?
    let degree_certificate:String?
    let country_code:String?
    var latitude:String?
    var longitude:String?
    let country_code_info:String?
    let sub_category_data:[Categories]?
    let avg_rating: String?
    let isSubscribedUser: Int?
    var isSubscribedUserAutoPlan:Int?
    let admin_approval:String?
    let degree_certificate_name:String?
    enum CodingKeys: String, CodingKey {

        case year_of_experience = "year_of_experience"
        case profile_image = "profile_image"
        case month_name = "month_name"
        case modified_date = "modified_date"
        case category_name = "category_name"
        case month = "month"
        case created_date = "created_date"
        case _id = "_id"
        case email_id = "email_id"
        case last_name = "last_name"
        case session_token = "session_token"
        case year = "year"
        case email_token = "email_token"
        case device_token = "device_token"
        case mobile_no = "mobile_no"
        case user_type = "user_type"
        case sub_category_ids = "sub_category_ids"
        case user_status = "user_status"
        case sub_category_name = "sub_category_name"
        case about_you = "about_you"
        case password = "password"
        case category_id = "category_id"
        case device_type = "device_type"
        case address = "address"
        case first_name = "first_name"
        case user_id = "user_id"
        case business_name = "business_name"
        case degree_certificate = "degree_certificate"
        case country_code = "country_code"
        case latitude = "latitude"
        case longitude = "longitude"
        case country_code_info = "country_code_info"
        case sub_category_data = "sub_category_data"
        case avg_rating = "avg_rating"
        case isSubscribedUser = "isSubscribedUser"
        case isSubscribedUserAutoPlan = "isSubscribedUserAutoPlan"
        case admin_approval = "admin_approval"
        case degree_certificate_name = "degree_certificate_name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        year_of_experience = try values.decodeIfPresent(String.self, forKey: .year_of_experience)
        profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
        month_name = try values.decodeIfPresent(String.self, forKey: .month_name)
        modified_date = try values.decodeIfPresent(String.self, forKey: .modified_date)
        category_name = try values.decodeIfPresent(String.self, forKey: .category_name)
        month = try values.decodeIfPresent(String.self, forKey: .month)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        created_date = try values.decodeIfPresent(String.self, forKey: .created_date)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        email_id = try values.decodeIfPresent(String.self, forKey: .email_id)
        last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
        session_token = try values.decodeIfPresent(String.self, forKey: .session_token)
        year = try values.decodeIfPresent(String.self, forKey: .year)
        email_token = try values.decodeIfPresent(String.self, forKey: .email_token)
        device_token = try values.decodeIfPresent(String.self, forKey: .device_token)
        mobile_no = try values.decodeIfPresent(String.self, forKey: .mobile_no)
        user_type = try values.decodeIfPresent(String.self, forKey: .user_type)
        sub_category_ids = try values.decodeIfPresent(String.self, forKey: .sub_category_ids)
        user_status = try values.decodeIfPresent(String.self, forKey: .user_status)
        sub_category_name = try values.decodeIfPresent(String.self, forKey: .sub_category_name)
        about_you = try values.decodeIfPresent(String.self, forKey: .about_you)
        password = try values.decodeIfPresent(String.self, forKey: .password)
        category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
        device_type = try values.decodeIfPresent(String.self, forKey: .device_type)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
        business_name = try values.decodeIfPresent(String.self, forKey: .business_name)
        degree_certificate = try values.decodeIfPresent(String.self, forKey: .degree_certificate)
        country_code = try values.decodeIfPresent(String.self, forKey: .country_code)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        country_code_info = try values.decodeIfPresent(String.self, forKey: .country_code_info)
        sub_category_data = try values.decodeIfPresent([Categories].self, forKey: .sub_category_data)
        avg_rating = try values.decodeIfPresent(String.self, forKey: .avg_rating)
        isSubscribedUser = try values.decodeIfPresent(Int.self, forKey: .isSubscribedUser)
        isSubscribedUserAutoPlan = try values.decodeIfPresent(Int.self, forKey: .isSubscribedUserAutoPlan)
        admin_approval = try values.decodeIfPresent(String.self, forKey: .admin_approval)
        degree_certificate_name = try values.decodeIfPresent(String.self, forKey: .degree_certificate_name)
    }

}
