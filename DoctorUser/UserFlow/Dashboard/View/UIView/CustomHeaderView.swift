//
//  CustomHeaderView.swift
//  StretchableTableViewHeader
//
//  Created by Admin on 15/12/21.
//

import UIKit

class CustomHeaderView: UIView {
    
    //MARK:- Outlet
    @IBOutlet weak var btnAction: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var heightConstimgProfile: NSLayoutConstraint!
    
    //MARK:- Class Function
    class func instanceFromNib() -> CustomHeaderView {
        return UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
    }
    
}

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
