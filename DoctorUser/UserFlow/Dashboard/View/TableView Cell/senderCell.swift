//
//  senderCell.swift
//
//  Created by VATSAL on 12/27/21.
//

import UIKit

class senderCell: UITableViewCell {

    @IBOutlet weak var viewForChat: UIView!
    
    @IBOutlet var txtMsg: UITextView!
    @IBOutlet var LblTime: UILabel!
    @IBOutlet var lblTimeHeight: NSLayoutConstraint!
    @IBOutlet var LblMsg: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
       // self.txtMsg.textContainerInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(data:ModelChat) {
        LblMsg.text = data.message
        LblTime.text = getDateFromTimeStamp(timeStamp: Double(data.timeStamp) ??  0)
        LblTime.translatesAutoresizingMaskIntoConstraints = false
        LblTime.centerXAnchor.constraint(equalTo: viewForChat.leftAnchor).isActive = true
        LblTime.topAnchor.constraint(equalTo: viewForChat.bottomAnchor, constant: 10).isActive = true
    }
    
}
func getDateFromTimeStamp(timeStamp : Double) -> String {
    
    let date = NSDate(timeIntervalSince1970: timeStamp/1000)
    
    let dayTimePeriodFormatter = DateFormatter()
    dayTimePeriodFormatter.dateFormat = "dd/MM/yyyy, hh:mm a"
    // UnComment below to get only time
    //  dayTimePeriodFormatter.dateFormat = "hh:mm a"
    
    
    let isCheckToday = Calendar.current.isDateInToday(date as Date)
    let dateString = dayTimePeriodFormatter.string(from: date as Date)
    return dateString.lowercased()
//    if isCheckToday == true
//    {
//        dayTimePeriodFormatter.dateFormat = "hh:mm a"
//        
//        var dateString = dayTimePeriodFormatter.string(from: date as Date)
//        
//        dateString = "\(dateString)"
//        
//        return dateString
//    }
//    else
//    {
//        let dateString = dayTimePeriodFormatter.string(from: date as Date)
//        return dateString
//    }
    
    
}
