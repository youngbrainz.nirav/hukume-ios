//
//  ReceiverCell.swift
//  WhoseThere
//
//  Created by VATSAL on 12/27/21.
//

import UIKit


class ChatTVCell: UITableViewCell {

    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var viewCount: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var btnChat: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //viewMain.addShadowOnRect()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

class ReceiverCell: UITableViewCell {

    @IBOutlet weak var viewForChat: UIView!
    @IBOutlet var lblTimeHeight: NSLayoutConstraint!
    @IBOutlet var txtMsg: UITextView!
    @IBOutlet var LblTime: UILabel!
    @IBOutlet var LblMsg: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
      //  self.txtMsg.textContainerInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(data:ModelChat) {
        LblMsg.text = data.message
        LblTime.text = getDateFromTimeStamp(timeStamp: Double(data.timeStamp) ??  0)
        LblTime.translatesAutoresizingMaskIntoConstraints = false
        LblTime.centerXAnchor.constraint(equalTo: viewForChat.rightAnchor).isActive = true
        LblTime.topAnchor.constraint(equalTo: viewForChat.bottomAnchor, constant: 10).isActive = true
    }
}
