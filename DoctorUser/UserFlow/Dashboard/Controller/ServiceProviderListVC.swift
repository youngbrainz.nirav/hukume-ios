//
//  ServiceProviderListVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/22/21.
//

import UIKit

class ServiceProviderListVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblList : UITableView!
    
    // MARK: - Variables
    var arrSPList:[DataSPList] = []
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        AppSingleton.shared.filterObject.isFilter = true
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if AppSingleton.shared.filterObject.isFilter {
        wsGetSPList()
        }
    }
    
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    //MARK:- Button Action
    @IBAction func btnFilterAction(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    //MARK: - API Calling
    func wsGetSPList(){
        var dictParam : [String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        if AppSingleton.shared.filterObject.isFilter {
            if !AppSingleton.shared.filterObject.category_id.isEmpty {
                dictParam[PARAMS.category_id] = AppSingleton.shared.filterObject.category_id
            }
            if !AppSingleton.shared.filterObject.sub_category_id.isEmpty {
                dictParam[PARAMS.sub_category_id] = AppSingleton.shared.filterObject.sub_category_id
            }
            if !AppSingleton.shared.filterObject.rating.isEmpty {
                dictParam[PARAMS.rating] = AppSingleton.shared.filterObject.rating
            }
            if !AppSingleton.shared.filterObject.latitude.isEmpty {
                dictParam[PARAMS.latitude] = AppSingleton.shared.filterObject.latitude
                dictParam[PARAMS.longitude] = AppSingleton.shared.filterObject.longitude
            }
            if !AppSingleton.shared.filterObject.search_keyword.isEmpty {
                dictParam[PARAMS.search_keyword] = AppSingleton.shared.filterObject.search_keyword
            }
        }
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_SERVICE_PROVIDER_LIST, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            self.arrSPList.removeAll()
            AppSingleton.shared.filterObject.resetFilter()
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelSPList.self, from: data!)
                    self.arrSPList = responseModel.data ?? []
                }
                catch {
                    
                }
            }
            self.tblList.reloadData()
        }
    }
    
}
//MARK:- TableView Delegate Method
extension ServiceProviderListVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSPList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SPTVCell
        cell.setSPData(data: arrSPList[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "SPProfileVC") as! SPProfileVC
        vc.user_id = arrSPList[indexPath.row]._id ?? ""
        self.pushToVC(vc)
    
}
    
}

