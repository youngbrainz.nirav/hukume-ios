//
//  ChatVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/31/21.
//

import UIKit
import FirebaseDatabase
import FirebaseStorage
import Firebase

class ChatVC: UIViewController {
    
    // MARK:- Outlets
    @IBOutlet var TblChat: UITableView!
    @IBOutlet var txtMsg: UITextView!
    @IBOutlet var viewSend: UIView!
    @IBOutlet var viewContainerr: UIView!
    @IBOutlet var TextMsgHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var scrollHeight: NSLayoutConstraint!
    @IBOutlet var scroll: UIScrollView!
    
    
    var conversationId = "1"
    let STR_Typeamessage = "Type a message"
    var arrChats:[ModelChat] = []
    var recipient_id = ""
    var receiver_name = ""
    var isFromPush = false
    var isFromPayment = false
    var isFirstTime = true
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        txtMsg.text = STR_Typeamessage
        
        txtMsg.delegate = self
        txtMsg.textColor = UIColor.lightGray
        lblTitle.text = receiver_name
        getMessages()
//        DispatchQueue.main.asyncAfter(deadline: .now()+3) {
//            self.getNewMessages()
//        }
        
        
        wsReadMsg()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        wsIsUserSubscribe()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        appDel.ref.child(conversationId).removeAllObservers()
        let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(didPressPartButton))
        singleTapGesture.numberOfTapsRequired = 1
        view.addGestureRecognizer(singleTapGesture)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                
                UIView.animate(withDuration: 0.3, animations: {
                    print(keyboardSize.height , "keyboard show")
                    
                    let screenSize = UIScreen.main.bounds
                    if screenSize.height >= 812{
                        self.scrollHeight.constant = keyboardSize.height - 34
                    }else{
                        self.scrollHeight.constant = keyboardSize.height
                    }
                    self.scroll.layoutIfNeeded()
                })
            }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
       
            self.scrollHeight.constant = 0
            UIView.animate(withDuration: 0.2, animations: {
            })
        
    }
    @objc func didPressPartButton() {
        self.view.endEditing(true)
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func getMessages(){
        appDel.ref.child(conversationId).observe(.childAdded) { snapshot in
            
                if let data = snapshot.value as? [String:Any] {
                let id = data["senderId"] as? String ?? ""
                let message = data["message"] as? String ?? ""
//                let name = data["senderName"] as? String ?? ""
//                let timeStamp = data["timeStamp"] as? String ?? ""
                    if !message.isEmpty {
//
                        if !self.isFirstTime {
                            if id == self.recipient_id {
                                self.wsReadMsg()
                            }
                        }
                    }
                }
        }
        appDel.ref.child(conversationId).observe(.value) { snapshot in
            let snap = snapshot.children
            self.arrChats.removeAll()
            for child in snap {
                let snapshot = child as? DataSnapshot
                if let data = snapshot?.value as? [String:Any] {
                let id = data["senderId"] as? String ?? ""
                let message = data["message"] as? String ?? ""
                let name = data["senderName"] as? String ?? ""
                let timeStamp = data["timeStamp"] as? String ?? ""
                    let chat = ModelChat.init(id, name, timeStamp, message)
                    if !message.isEmpty {
                        self.arrChats.append(chat)
                    }
              //  print(data)
                }
            }
            self.TblChat.reloadData()
            if self.arrChats.count > 0 {
                self.TblChat.scrollToRow(at: IndexPath.init(row: self.arrChats.count-1, section: 0), at: .bottom, animated: !self.isFirstTime)
                self.isFirstTime = false
                
            }
            
        }
    }
        func getNewMessages() {
            appDel.ref.child(conversationId).observe(.childAdded) { snapshot in
                
                    if let data = snapshot.value as? [String:Any] {
                    let id = data["senderId"] as? String ?? ""
                    let message = data["message"] as? String ?? ""
                    let name = data["senderName"] as? String ?? ""
                    let timeStamp = data["timeStamp"] as? String ?? ""
                        let chat = ModelChat.init(id, name, timeStamp, message)
                        if !message.isEmpty {
                            let arr = self.arrChats.filter({$0.timeStamp == chat.timeStamp})
                            if arr.count == 0 {
                                if id == self.recipient_id {
                                self.wsReadMsg()
                                }
                            }
                        }
                    }
            }
        
            
    }
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        if isFromPush {
            appDel.gotoHome()
        }
        else if isFromPayment {
            if let vcs = self.navigationController?.viewControllers {
                for vc in vcs {
                   if vc is SPProfileVC {
                        self.navigationController?.popToViewController(vc, animated: true)
                    }
                }
            }
        }
        else {
        self.navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func btnSendAction(_ sender: UIButton) {
        
        let date = Int((NSDate().timeIntervalSince1970) * 1000)

        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd/MM/YY, hh:mm a"
        var dateString = dayTimePeriodFormatter.string(from: Date())

        
        let sessionItem = ModelChat(preferenceHelper.getUserId(), preferenceHelper.getUserName(), "\(date)", txtMsg.text!)
        
        
        
        
        
        let uid = appDel.ref.childByAutoId().key ?? "";
        
        let sessionItemRef = appDel.ref.child(self.conversationId).child(uid)

        
        sessionItemRef.setValue(sessionItem.toAnyObject())
        
      //  self.CallLastReadMsg(strMsg: txtMsg.text!)
        wsLastMessage()
        
        txtMsg.text = ""
        
    }
    
    //MARK: - API Calling
    func wsLastMessage(){
        
            var dictParam : [String:Any] = [:]
            dictParam[PARAMS.sender_id] = preferenceHelper.getUserId()
            dictParam[PARAMS.sender_type] = preferenceHelper.getUserType()
            dictParam[PARAMS.recipient_id] = recipient_id
            dictParam[PARAMS.conversation_id] = conversationId
            dictParam[PARAMS.lastMsg] = txtMsg.text!
            dictParam[PARAMS.timezone] = TimeZone.current.identifier
            let alamofire = AlamofireHelper.init()
            alamofire.getResponseFromURL(url: WebService.LAST_CONVERSATION, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam, isShowLoading: false) { response, data, error in
                if Parser.isSuccess(response: response) {
                    
                }
            }
        
    }
    func wsIsUserSubscribe(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.IS_CUSTOMER_SUBSCRIBED, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response) {
                if let isSubscribe = response["isSubscribedUser"] as? Bool {
                    if isSubscribe {
                       
                    } else {
                        let vc = UIStoryboard(name: STORYBOARDNAME.Payment, bundle: nil).instantiateViewController(withIdentifier: "CardListVC") as! CardListVC
                        vc.isFromChat = true
                        self.pushToVC(vc)
                    }
                }
            }
        }
    }
    func wsReadMsg(){
        var dictParam : [String:Any] = [:]
        dictParam[PARAMS.sender_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.recipient_id] = recipient_id
        dictParam[PARAMS.conversation_id] = conversationId
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.READ_MSG, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam, isShowLoading: false) { response, data, error in
            if Parser.isSuccess(response: response) {
                
            }
        }
    }
}
//MARK:- TableView Delegate Method
extension ChatVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrChats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if arrChats[indexPath.row].senderId != preferenceHelper.getUserId() {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverCell", for: indexPath) as! ReceiverCell
            cell.setData(data: arrChats[indexPath.row])
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "senderCell", for: indexPath) as! senderCell
            cell.setData(data: arrChats[indexPath.row])
            return cell
        }
        
    }
    
    
}

extension ChatVC : UITextViewDelegate{
    // MARK:- TextView Delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        
            
            
            
            if textView.text == STR_Typeamessage
            {
                textView.text = ""
                textView.textColor = UIColor.black
            }
            
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
       
            if textView.text == ""
            {
                textView.text = STR_Typeamessage
                textView.textColor = UIColor.lightGray
            }
            
            
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if txtMsg.isFirstResponder
        {
            // if a timer is already active, prevent it from firing
            
            
//            let fixedWidth = txtMsg.frame.size.width
//                let newSize = txtMsg.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
//                if newSize.height > 100 {
//                    txtMsg.isScrollEnabled = true
//                }
//                else {
//                    txtMsg.isScrollEnabled = false
//                    TextMsgHeight.constant = newSize.height
//                }
            if txtMsg.contentSize.height > 75
            {
                txtMsg.isScrollEnabled = true
                
            }

            if txtMsg.contentSize.height < 75
            {
                txtMsg.isScrollEnabled = false
                if txtMsg.contentSize.height > 33 {
                    txtMsg.sizeToFit()
                TextMsgHeight.constant = txtMsg.contentSize.height
                }
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        
        
        return true
    }
    
}
struct ModelChat {
   
    var senderId:String = ""
    var senderName:String = ""
    var timeStamp:String = ""
    var message:String = ""
    //var datetime:String = ""
    
    init(_ senderId:String, _ senderName:String, _ timeStamp:String, _ message:String/*, _ senderDateTime:String*/) {
        
        self.senderId = senderId
        self.senderName = senderName
        self.timeStamp = timeStamp
        self.message = message
       // self.datetime = senderDateTime
      
    }
    
    func toAnyObject() -> Any {
        return [
            
            "senderId": senderId,
            "senderName": senderName,
            "timeStamp": timeStamp,
            "message": message
//            ,
//            "datetime": datetime

        ]
    }
}
