//
//  ThankYouVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/28/21.
//

import UIKit

class ThankYouVC: UIViewController {
    
    
    //MARK:- Outlets
    
    
    //MARK:- Varibles
    
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        let homeViewController = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let nav = UINavigationController(rootViewController: homeViewController)
        appDel.window?.rootViewController = nav
        nav.isNavigationBarHidden = true
        appDel.window?.makeKeyAndVisible()
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
        let homeViewController = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let nav = UINavigationController(rootViewController: homeViewController)
        appDel.window?.rootViewController = nav
        nav.isNavigationBarHidden = true
        appDel.window?.makeKeyAndVisible()
    }

}
