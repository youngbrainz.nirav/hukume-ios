//
//  AddRatingVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/27/21.
//

import UIKit
import Cosmos

class AddRatingVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var viewRating : CosmosView!
    @IBOutlet weak var txtDescription : UITextView!
    
    
    //MARK:- Varibles
    var strPlaceHolder = "How's your experience with Dr."
    var objSession:DataSessionList?
    var sessionId = ""
    var servive_provider_id = ""
    //MARK:- View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtDescription.delegate = self
        self.viewRating.rating =  0.0
        txtDescription.text = strPlaceHolder
        txtDescription.textColor = .lightGray
        viewRating.settings.fillMode = .half
    }
    
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnsubmitAction(_ sender: Any) {
        if(viewRating.rating == 0.0)
        {
            Common().showAlert(strMsg: "Plase give at least one star.", view: self)
        }
        else
        {
            if txtDescription.text == strPlaceHolder {
                Common().showAlert(strMsg: "Please add your experience with Dr.", view: self)
            } else {
                wsAddReview()
            }
            
        }
    }
//MARK: - API Calling
    func wsAddReview(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.booking_id] = objSession?._id ?? sessionId
        dictParam[PARAMS.user_id] = objSession?.service_provider_id ?? servive_provider_id
        dictParam[PARAMS.rating] = String(viewRating.rating)
        dictParam[PARAMS.description] = txtDescription.text
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.ADD_REVIEW, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            
            if Parser.isSuccess(response: response,isSuccessToast: true) {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }

    
}

//MARK:- Textview Delegate Methods
extension AddRatingVC : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == strPlaceHolder {
            txtDescription.textColor = .black
            textView.text = nil
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = strPlaceHolder
            txtDescription.textColor = .lightGray
        }
    }

}
