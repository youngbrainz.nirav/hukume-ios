//
//  SPProfileVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/27/21.
//

import UIKit

class SPProfileVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tblRating : UITableView!
    @IBOutlet weak var heightConstTblRating : NSLayoutConstraint!
    @IBOutlet weak var lblPayTitle : UILabel!
    @IBOutlet weak var viewPayPopup : UIView!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    
    @IBOutlet weak var lblDescriptionValue: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblSubCategory: UILabel!
    @IBOutlet weak var viewForReview: UIView!
    //MARK:- Varibles
    var user_id = ""
    var arrReview:[Reviews] = []
    var isSubscribed = false
    var amount = ""
    var category_id = ""
    var arrSubcategory:[Categories] = []
    //MARK:- View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblRating.estimatedRowHeight = 150
        tblRating.rowHeight =  UITableView.automaticDimension
        lblPrice.isHidden = true
        
        let strName =  "For continue with chat pay"
        let strTitle = "$14.00"
        
        let strNew = "\(strName) \(strTitle)"
        let attributedWithTextColor: NSAttributedString = strNew.attributedStringWithColor([strTitle], color: UIColor(red: 8 / 255, green: 92 / 255, blue: 252 / 255, alpha: 1.0))
        lblPayTitle.attributedText =  attributedWithTextColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewPayPopup.isHidden = true
        wsGetSPDetails()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        heightConstTblRating.constant =  tblRating.contentSize.height
    }
    
    //MARK: - Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnTempAction(_ sender: Any) {
        
    }

    @IBAction func btnBookAction(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "SelectCategoryVC") as! SelectCategoryVC
        vc.category_name = lblCategory.text!
        vc.category_id = category_id
        vc.arrSubCategory = arrSubcategory
        AppSingleton.shared.bookingObject.service_provider_id = user_id
        self.pushToVC(vc)
//        AppSingleton.shared.bookingObject.service_provider_id = user_id
//        let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "AddIssueVC") as! AddIssueVC
//        self.pushToVC(vc)
    }
    
    @IBAction func btnViewAllAction(_ sender: Any) {
        print("View All")
        let vc = UIStoryboard(name: STORYBOARDNAME.Profile, bundle: nil).instantiateViewController(withIdentifier: "RatingListVC") as! RatingListVC
        vc.arrReview = arrReview
        vc.name = lblUserName.text!
        vc.rating = lblRating.text!
        vc.category = lblCategory.text!
        vc.imgUrl = imgUser.image!
        self.pushToVC(vc)
    }

    @IBAction func btnChatAction(_ sender: Any) {
        
            //Cancel Popup
        if isSubscribed {
            wsStartConversation()
        } else {
            let strName =  "For continue with chat pay"
            let strTitle = amount
            
            let strNew = "\(strName) \(strTitle)"
            let attributedWithTextColor: NSAttributedString = strNew.attributedStringWithColor([strTitle], color: UIColor(red: 8 / 255, green: 92 / 255, blue: 252 / 255, alpha: 1.0))
            lblPayTitle.attributedText = attributedWithTextColor
            self.viewPayPopup.isHidden = false
        }
        
        
    }
    @IBAction func btnCloseViewAction(_ sender: Any) {
        self.viewPayPopup.isHidden = true
    }
    
    @IBAction func btnPayAction(_ sender: Any) {
        self.viewPayPopup.isHidden = true
//        let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
//        self.pushToVC(vc)
        let vc = UIStoryboard(name: STORYBOARDNAME.Payment, bundle: nil).instantiateViewController(withIdentifier: "CardListVC") as! CardListVC
        vc.isForChat = true
        vc.user_id = user_id
        vc.user_name = lblUserName.text!
        self.pushToVC(vc)
        //wsStartConversation()
    }
    func setSPDetail(data:DataSPDetail) {
        lblUserName.text = (data.first_name ?? "") + " " + (data.last_name ?? "")
        lblLocation.text = data.address
        lblCategory.text = data.category_name ?? " "
        lblRating.text = data.avg_rating
        lblDescriptionValue.text = data.about_you
        lblDate.text = data.availability_date ?? " "
        lblTime.text = data.start_time ?? " "
        imgUser.downloadedFrom(link: data.profile_image ?? "")
        lblSubCategory.text = " "//data.sub_category_name ?? " "
        isSubscribed = (data.isSubscribedUser ?? 0) == 1
        amount = data.subscription_amount ?? ""
        category_id = data.category_id ?? ""
        arrSubcategory = data.sub_category_data ?? []
        //AppSingleton.shared.bookingObject.available_dates = data.available_dates ?? []
        
    }
  //MARK: - API Calling
    func wsGetSPDetails(){
        var dictParam : [String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.user_id] = user_id
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_SP_DETAIL, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelSPDetail.self, from: data!)
                    self.arrReview = responseModel.data?.reviews ?? []
                    self.viewForReview.isHidden = self.arrReview.isEmpty
                    self.tblRating.reloadData()
                    self.heightConstTblRating.constant =  self.tblRating.contentSize.height
                    DispatchQueue.main.async {
                        self.heightConstTblRating.constant =  self.tblRating.contentSize.height
                    }
                    self.setSPDetail(data: responseModel.data!)
                }
                catch {
                    
                }
            }
        }
    }
    func wsStartConversation(){
        var dictParam : [String:Any] = [:]
        dictParam[PARAMS.sender_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.sender_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.recipient_id] = user_id
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.START_CONVERSATION, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response) {
                let data = response["data"] as! [String:Any]
                var conversationID = ""
                if let conv_id = data["conversation_id"] as? Int {
                    conversationID = "\(conv_id)"
                } else {
                    conversationID = data["conversation_id"] as? String ?? ""
                }
                let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                vc.conversationId = conversationID
                vc.recipient_id = self.user_id
                vc.receiver_name = self.lblUserName.text!
                self.pushToVC(vc)
            }
        }
    }
    
}
//MARK:- TableView Delegate Method
extension SPProfileVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReview.count > 2 ? 2:arrReview.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SPTVCell
        //cell.lblAvailable.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
        cell.setSPRating(data: arrReview[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

