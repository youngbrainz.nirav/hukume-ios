//
//  SearchVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/23/21.
//

import UIKit
import DropDown
import Cosmos
import GooglePlaces
import GooglePlacePicker

class SearchVC: UIViewController,GMSAutocompleteViewControllerDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var lblCategory : UILabel!
    @IBOutlet weak var lblSubCategory : UILabel!
    @IBOutlet weak var lblLanguage : UILabel!
    @IBOutlet weak var lblLocation : UILabel!
    @IBOutlet weak var imgCategoryDown : UIImageView!
    @IBOutlet weak var imgSubCategoryDown : UIImageView!
    @IBOutlet weak var imgLanguageDown : UIImageView!
    
    @IBOutlet weak var viewCategory : UIView!
    @IBOutlet weak var viewSubCategory : UIView!
    @IBOutlet weak var viewLanguage : UIView!
    @IBOutlet weak var viewLocation : UIView!
    
    @IBOutlet weak var btnCategory : UIButton!
    @IBOutlet weak var btnSubCategory : UIButton!
    @IBOutlet weak var btnLanguage : UIButton!
    @IBOutlet weak var btnLocation : UIButton!
    
    @IBOutlet weak var viewRating : CosmosView!
    @IBOutlet weak var sliderPrice : UISlider!
    
    
    //MARK:- Varibles
    let ddCategory = DropDown()
    let ddSubCategory = DropDown()
    let ddLanguage = DropDown()
    var arrCategoryString:[String] = []
    var arrSubCategoryString:[String] = []
    var arrCategory:[Categories] = []
    var arrSubCategory:[Categories] = []
    var arrLanguage = ["Gujarati","Hindi","English"]
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewRating.settings.fillMode = .half
        // Do any additional setup after loading the view.
        setDropDowns()
        wsGetCategorySubCategories()
    }

    
    //MARK:- Set DropDowns
    func setDropDowns(){
        
        //Language
        self.ddLanguage.anchorView = viewLanguage
        self.ddLanguage.dataSource = arrLanguage
        self.ddLanguage.bottomOffset = CGPoint(x: 0, y:(self.ddLanguage.anchorView?.plainView.bounds.height)!)
        self.ddSubCategory.topOffset = CGPoint(x: 0, y:-(self.ddLanguage.anchorView?.plainView.bounds.height)!)
        ddLanguage.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblLanguage.text = self.arrLanguage[index]
        }
    }
    
    
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDropDownAction(_ sender: UIButton) {
        
        
        if sender.tag ==  1{
            print("category")
            self.lblCategory.textColor = .black
            viewCategory.backgroundColor =  .white
            imgCategoryDown.image =  UIImage(named: "ic_down_black")
//            viewFirstName.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            ddCategory.show()
            
        }else if sender.tag == 2{
            print("sub category")
            self.lblSubCategory.textColor = .black
            viewSubCategory.backgroundColor =  .white
            imgSubCategoryDown.image =  UIImage(named: "ic_down_black")
            ddSubCategory.show()
        }else if sender.tag == 3{
            print("Language")
            self.lblLanguage.textColor = .black
            viewLanguage.backgroundColor =  .white
            imgLanguageDown.image =  UIImage(named: "ic_down_black")
            ddLanguage.show()
        }else if sender.tag == 4{
            print("Location")
            openAutoCompletePlace()
        }
        
    }
    @IBAction func btnSearchAction(_ sender: Any) {
        if viewRating.rating != 0 {
        AppSingleton.shared.filterObject.rating = "\(viewRating.rating)"
        }
        AppSingleton.shared.filterObject.isFilter = true
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnClearAction(_ sender: Any) {
        AppSingleton.shared.filterObject.resetFilter()
        lblCategory.text = "Select Category"
        lblSubCategory.text = "Select Sub Category"
        viewRating.rating = 0
        lblLocation.text = "Select Location"
       // self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSliderAction(_ sender: UISlider) {
        
    }
    
    func openAutoCompletePlace(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        lblLocation.text = place.formattedAddress
        lblLocation.textColor = .black
        AppSingleton.shared.filterObject.latitude = String(place.coordinate.latitude)
        AppSingleton.shared.filterObject.longitude = String(place.coordinate.longitude)
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    func setCategory(){
        arrCategoryString.removeAll()
        arrSubCategoryString.removeAll()
        
        
        for cat in arrCategory {
            arrCategoryString.append(cat.name!)
        }
        
        //Category
        self.ddCategory.anchorView = btnCategory
        self.ddCategory.dataSource = arrCategoryString
        self.ddCategory.bottomOffset = CGPoint(x: 0, y:(self.ddCategory.anchorView?.plainView.bounds.height)!)
        self.ddCategory.topOffset = CGPoint(x: 0, y:-(self.ddCategory.anchorView?.plainView.bounds.height)!)
        ddCategory.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.lblCategory.text = self.arrCategoryString[index]
            AppSingleton.shared.filterObject.sub_category_id = ""
            AppSingleton.shared.filterObject.category_id = self.arrCategory[index]._id!
            self.wsGetCategorySubCategories(id: self.arrCategory[index]._id!)
        }
        
        
        
        
    }
    func setSubCategory(){
        arrSubCategoryString.removeAll()
        for sub in arrSubCategory {
            
            arrSubCategoryString.append(sub.name!)
        }
        
        //SubCategory
        self.ddSubCategory.anchorView = viewSubCategory
        self.ddSubCategory.dataSource = arrSubCategoryString
        self.ddSubCategory.bottomOffset = CGPoint(x: 0, y:(self.ddSubCategory.anchorView?.plainView.bounds.height)!)
        self.ddSubCategory.topOffset = CGPoint(x: 0, y:-(self.ddSubCategory.anchorView?.plainView.bounds.height)!)
        ddSubCategory.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblSubCategory.text = self.arrSubCategoryString[index]
            AppSingleton.shared.filterObject.sub_category_id = self.arrSubCategory[index]._id!
        }
    }
//MARK: - API Calling
    func wsGetCategorySubCategories(id:String=""){
        let dictParam:[String:Any] = id.isEmpty ? [:]:[PARAMS.category_id:id]
       
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_SP_CATEGORIES_SUB_CATEGORIES, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelCategories.self, from: data!)
                    
                    
                    if id.isEmpty {
                        self.arrCategory = responseModel.data?.categories ?? []
                        self.setCategory()
                    } else {
                        self.arrSubCategory = responseModel.data?.sub_categories ?? []
                        self.setSubCategory()
                    }
                    
                } catch {
                    
                }
               
            }
        }
    }

}
