//
//  HomeVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/21/21.
//

import UIKit
import BSStackView

class HomeVC: UIViewController {
    
    @IBOutlet var stackView: BSStackView!
    @IBOutlet var viewUpcoming: UIView!
    @IBOutlet var stackView2: BSStackView!
    @IBOutlet var viewUpcoming2: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var imgPastUser: UIImageView!
    
    @IBOutlet weak var lblPastSPName: UILabel!
    @IBOutlet weak var lblPastCategory: UILabel!
    
    @IBOutlet weak var lblPastSessionName: UILabel!
    @IBOutlet weak var lblPastDate: UILabel!
    @IBOutlet weak var lblPastTime: UILabel!
    
    @IBOutlet weak var imgUpcomingUser: UIImageView!
    
    @IBOutlet weak var lblUpcomingSPName: UILabel!
    @IBOutlet weak var lblUpcomingCategory: UILabel!
    
    @IBOutlet weak var lblUpcomingDate: UILabel!
    @IBOutlet weak var lblUpcomingTime: UILabel!
    @IBOutlet weak var lblUpcomingSessionName: UILabel!
    @IBOutlet var cvBanner: UICollectionView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewForUpcomingMain: UIView!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var viewForPastMain: UIView!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblChatCount: UILabel!
    //MARK: - Variable
//    var viewOfHeader = CustomHeaderView.instanceFromNib()
    var views =  [UIView]()
    var views2 =  [UIView]()
    var arrCategory:[Categories] = []
    var arrPastSession:[Past_sessions] = []
    var arrUpcomingSession:[Upcoming_sessions] = []
    //12B9AE
    //18 185 174
    override func viewDidLoad() {
        super.viewDidLoad()
        lblCount.isHidden = true
        txtSearch.delegate = self
        viewUpcoming.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(handleTapUpcoming)))
        viewUpcoming.isUserInteractionEnabled = true
        viewUpcoming2.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(handleTapPast)))
        viewUpcoming2.isUserInteractionEnabled = true
        for i in 0..<3 {
            views.append(viewUpcoming)
            views2.append(viewUpcoming2)
        }
        self.stackView.contraintsConfigurator.top = 5
        self.stackView.contraintsConfigurator.bottom = 0
        self.stackView.contraintsConfigurator.leading = 0
        self.stackView.contraintsConfigurator.trailing = 0
//        self.stackView.backgroundColor = UIColor(red: 22 / 255, green: 100 / 255, blue: 252 / 255 , alpha: 1.0)
        // Do any additional setup after loading the view.
//        stackView.swipeDirections = [.right]//[.left, .right, .up, .down]
//        stackView.forwardDirections = [.right, .up]
//        stackView.backwardDirections = [.left, .down]
        stackView.changeAlphaOnSendAnimation = true
        
        stackView.configure(with: views)
        stackView.delegate = self
        
        self.stackView2.contraintsConfigurator.top = 5
        self.stackView2.contraintsConfigurator.bottom = 0
        self.stackView2.contraintsConfigurator.leading = 0
        self.stackView2.contraintsConfigurator.trailing = 0
//        self.viewUpcoming2.backgroundColor = UIColor(red: 18 / 255, green: 185 / 255, blue: 174 / 255 , alpha: 1.0)
        // Do any additional setup after loading the view.
//        stackView.swipeDirections = [.right]//[.left, .right, .up, .down]
//        stackView.forwardDirections = [.right, .up]
//        stackView.backwardDirections = [.left, .down]
        stackView2.changeAlphaOnSendAnimation = true//true
        
        stackView2.configure(withViews2: views2)
        stackView2.delegate = self
        btnChat.setImage(UIImage.init(named: "ic_chat")?.sd_tintedImage(with: .themeColor), for: .normal)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        wsGetProfileData()
        wsUserHomeDashboard()
        txtSearch.text = ""
        lblUserName.text = preferenceHelper.getUserName().components(separatedBy: " ").first
        imgUser.downloadedFrom(link: preferenceHelper.getUserPic())
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imgUser.applyCornerRadiusWith(radius: 10)
    }
    @objc func handleTapUpcoming(){
        let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "SessionDetailsVC") as! SessionDetailsVC
        vc.isPastSession = false
        vc.sessionId = arrUpcomingSession.first?._id ?? ""
        self.pushToVC(vc)
    }
    @objc func handleTapPast(){
        let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "SessionDetailsVC") as! SessionDetailsVC
        vc.isPastSession = true
        vc.sessionId = arrPastSession.first?._id ?? ""
        self.pushToVC(vc)
    }
    func view(withLabel index: Int) -> UIView? {
        let view = UIView(frame: self.view.bounds)
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 100.0, height: 100.0))
        label.text = String(format: "%ld", index)
        label.textColor = UIColor.blue
        label.font = UIFont.systemFont(ofSize: 25.0)
        label.textAlignment = .center
        view.addSubview(label)

        return view
    }
    func viewMain(withLabel index: Int) -> UIView? {
        let view = UIView(frame: self.view.bounds)
//        view.addSubview(self.stackView)
        viewUpcoming.backgroundColor = UIColor(red: 22 / 255, green: 100 / 255, blue: 252 / 255 , alpha: 1.0)
        return viewUpcoming
    }
    func setHomeData(){
       // imgCategory.downloadedFrom(link: arrCategory.first?.icon ?? "")
        //lblCategoryName.text = "    " + (arrCategory.first?.name ?? "") + "    "
        viewForUpcomingMain.isHidden = arrUpcomingSession.isEmpty
        viewForPastMain.isHidden = arrPastSession.isEmpty
        
        lblUpcomingCategory.text = arrUpcomingSession.first?.category_name
        lblUpcomingSessionName.text = arrUpcomingSession.first?.session_name
        lblUpcomingSPName.text = arrUpcomingSession.first?.service_provider_name
        lblUpcomingDate.text = arrUpcomingSession.first?.booking_date
        lblUpcomingTime.text = arrUpcomingSession.first?.start_time
        imgUpcomingUser.downloadedFrom(link: arrUpcomingSession.first?.service_provider_image ?? "")
        
        lblPastCategory.text = arrPastSession.first?.category_name
        lblPastSessionName.text = arrPastSession.first?.session_name
        lblPastSPName.text = arrPastSession.first?.service_provider_name
        lblPastDate.text = arrPastSession.first?.booking_date
        lblPastTime.text = arrPastSession.first?.start_time
        imgPastUser.downloadedFrom(link: arrPastSession.first?.service_provider_image ?? "")
        
        
    }
    //MARK:- Button Action
    @IBAction func btnProfileAction(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.Profile, bundle: nil).instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func btnViewAllCategory(_ sender: UIButton) {
        if sender.tag == 0 {
        let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "ServiceProviderListVC") as! ServiceProviderListVC
        self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "SessionListVC") as! SessionListVC
            vc.isPastSession = sender.tag == 2
            self.pushToVC(vc)
        }
    }
    @IBAction func btnNotificationAction(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.Profile, bundle: nil).instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @IBAction func btnSearchAction(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "ServiceProviderListVC") as! ServiceProviderListVC
        self.navigationController?.pushViewController(vc, animated: true)
//        let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func btnChatAction(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "ChatListVC") as! ChatListVC
        self.pushToVC(vc)
    }
    
    func translate(_ direction: BSStackViewItemDirection) -> String? {
        switch direction {
        case BSStackViewItemDirection.front:
            return "front"
        case BSStackViewItemDirection.back:
            return "back"
        default:
            break
        }
        return ""
    }
    func wsGetProfileData(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_USER_PROFILE, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam,isShowLoading: false) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelLogin.self, from: data!)
                    AppSingleton.shared.user = responseModel.data
                    preferenceHelper.setUserName((AppSingleton.shared.user?.first_name ?? "") + " " + (AppSingleton.shared.user?.last_name ?? ""))
                    preferenceHelper.setUserPic(AppSingleton.shared.user?.profile_image ?? "")
                    self.lblUserName.text = preferenceHelper.getUserName().components(separatedBy: " ").first
                    self.imgUser.downloadedFrom(link: preferenceHelper.getUserPic())
                }
                catch {
                    
                }
            }
        }
    }
    func wsUserHomeDashboard(){
        var dictParam : [String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.USER_HOME_DASHBOARD, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelUserHome.self, from: data!)
                    self.arrCategory = responseModel.data?.categories ?? []
                    self.arrPastSession = responseModel.data?.past_sessions ?? []
                    self.arrUpcomingSession = responseModel.data?.upcoming_sessions ?? []
                    self.setHomeData()
                    self.lblCount.text = responseModel.notification_count
                    self.lblChatCount.text = responseModel.totalUnreadCount
                    if responseModel.notification_count == "0" {
                        self.lblCount.isHidden = true
                    } else {
                        self.lblCount.isHidden = false
                    }
                    if responseModel.totalUnreadCount == "0" {
                        self.lblChatCount.isHidden = true
                    } else {
                        self.lblChatCount.isHidden = false
                    }
                    self.cvBanner.reloadData()
                }
                catch {
                    
                }
            }
        }
    }
}
//MARK: - Textfield delegate
extension HomeVC:UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        AppSingleton.shared.filterObject.search_keyword = txtSearch.text!
        AppSingleton.shared.filterObject.isFilter = true
        self.view.endEditing(true)
        btnSearchAction(self)
        
        return true
    }
}

//MARK: - StackView Protocol
extension HomeVC : BSStackViewProtocol{
    func stackView(_ stackView: BSStackView?, willSendItem item: UIView?, direction: BSStackViewItemDirection) {
        print(String(format: "stackView willSendItem %ld direction %@", item?.tag ?? 0, translate(direction)!))
    }
    func stackView(_ stackView: BSStackView?, didSendItem item: UIView?, direction: BSStackViewItemDirection) {
            print(String(format: "stackView didSendItem %ld direction %@", item?.tag ?? 0, translate(direction)!))
    }
}
//MARK: - CollectionView Method
extension HomeVC : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControl.numberOfPages = arrCategory.count
        pageControl.isHidden = true//!(arrCategory.count > 1)
        return self.arrCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ChatCVCell
        cell.setUserHomeData(data: arrCategory[indexPath.row])
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        AppSingleton.shared.filterObject.category_id = arrCategory[indexPath.item]._id ?? ""
        AppSingleton.shared.filterObject.isFilter = true
        let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "ServiceProviderListVC") as! ServiceProviderListVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width-10-40)/2 , height: collectionView.frame.size.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("Did scrollViewDidEndDecelerating :- \(Int(scrollView.contentOffset.x) / Int(scrollView.frame.width))")
        pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
     
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        print("Did End :- \(Int(scrollView.contentOffset.x) / Int(scrollView.frame.width))")
        pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
}
