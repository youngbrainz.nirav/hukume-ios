//
//  SelectCategoryVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/27/21.
//

import UIKit

class SelectCategoryVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var cvCategory : UICollectionView!
    @IBOutlet weak var heightConstcvCategory : NSLayoutConstraint!
    @IBOutlet weak var cvSubCategory : UICollectionView!
    @IBOutlet weak var heightConstcvSubCategory : NSLayoutConstraint!
    
    @IBOutlet weak var lblCategory: UILabel!
    //MARK:- Varibles
    var arrCategory:[Categories] = []
    var arrSubCategory : [Categories] = []
    var arrSelectedCategory = [IndexPath]()
    var arrSelectedSubCategory = [IndexPath]()
    var category_id = ""
    var category_name = ""
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //wsGetCategorySubCategories(id: category_id)
        AppSingleton.shared.bookingObject.sub_category_id = ""
        AppSingleton.shared.bookingObject.category_id = category_id
        lblCategory.text = "Category: \(category_name)"
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cvCategory.layoutIfNeeded()
        cvCategory.reloadData()
        cvSubCategory.layoutIfNeeded()
        cvSubCategory.reloadData()
        let  height1 = self.cvSubCategory.collectionViewLayout.collectionViewContentSize.height
        self.heightConstcvSubCategory.constant = height1
    }
    override func viewDidLayoutSubviews() {
        
        self.view.setNeedsLayout() //Or self.view.layoutIfNeeded()
    }
    
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
        if AppSingleton.shared.bookingObject.category_id.isEmpty {
         
            Common().showAlert(strMsg: "Please select category.", view: self)
        } else if arrSelectedSubCategory.isEmpty {
            Common().showAlert(strMsg: "Please select sub category.", view: self)
        } else {
//            for sub in arrSelectedSubCategory {
//            AppSingleton.shared.bookingObject.sub_category_id = AppSingleton.shared.bookingObject.sub_category_id.isEmpty ? (self.arrSubCategory[sub.item]._id ?? ""):(AppSingleton.shared.bookingObject.sub_category_id+","+(self.arrSubCategory[sub.item]._id ?? ""))
//            }
            AppSingleton.shared.bookingObject.sub_category_id = self.arrSubCategory[arrSelectedSubCategory[0].item]._id ?? ""
        let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "AddIssueVC") as! AddIssueVC
        self.pushToVC(vc)
        }
    }
}
//MARK:- CollectionView Method
extension SelectCategoryVC : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.cvCategory{
            return arrCategory.count
        }else{
            return arrSubCategory.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCVCell
        if collectionView == self.cvCategory{
            let item = arrCategory[indexPath.row]
            cell.lblName.text =  item.name
            if arrSelectedCategory.contains(indexPath){
                cell.viewMain.backgroundColor =  UIColor(red: 22 / 255, green: 100 / 255, blue: 252 / 255 , alpha: 1.0)
                cell.lblName.textColor = .white
            }else{
                cell.viewMain.backgroundColor =  .white
                cell.lblName.textColor = .darkGray
            }
        }else{
            let item = arrSubCategory[indexPath.row]
            cell.lblName.text =  item.name
            if arrSelectedSubCategory.contains(indexPath){
                cell.viewMain.backgroundColor =  UIColor(red: 22 / 255, green: 100 / 255, blue: 252 / 255 , alpha: 1.0)
                cell.lblName.textColor = .white
            }else{
                cell.viewMain.backgroundColor =  .white
                cell.lblName.textColor = .darkGray
            }
            
        }
        cell.viewMain.borderWidth = 1.0
        cell.viewMain.borderColor = .lightGray
        cell.viewMain.cornerRadius = 5
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.cvCategory{
//            self.arrSelectedCategory = self.arrSelectedCategory.filter($0.indexPath)
//            if self.arrSelectedCategory.contains(indexPath){
//                self.arrSelectedCategory = self.arrSelectedCategory.filter{$0 != indexPath}
//
//            }else{
//                self.arrSelectedCategory.append(indexPath)
//
//            }
            if !self.arrSelectedCategory.contains(indexPath){
            self.arrSelectedCategory = [indexPath]
                AppSingleton.shared.bookingObject.sub_category_id = ""
                self.arrSelectedSubCategory.removeAll()
                wsGetCategorySubCategories(id: arrCategory[indexPath.item]._id ?? "")
                AppSingleton.shared.bookingObject.category_id = arrCategory[indexPath.item]._id ?? ""
                cvCategory.reloadData()
                cvSubCategory.reloadData()
            }
            
            
        }else{

//            if self.arrSelectedSubCategory.contains(indexPath){
//                self.arrSelectedSubCategory = self.arrSelectedSubCategory.filter{$0 != indexPath}
//            }else{
//                self.arrSelectedSubCategory.append(indexPath)
//
//            }
            arrSelectedSubCategory = [indexPath]
            cvSubCategory.reloadData()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let noOfCellsInRow = 3
            
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

            let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))

            return CGSize(width: size, height: 60)
       // return CGSize(width: (UIScreen.main.bounds.width-40-60) / 3 , height: 60)
    }
    func wsGetCategorySubCategories(id:String=""){
        let dictParam:[String:Any] = id.isEmpty ? [:]:[PARAMS.category_id:id]
       
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_SP_CATEGORIES_SUB_CATEGORIES, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelCategories.self, from: data!)
                    
                    
                    if id.isEmpty {
                        self.arrCategory = responseModel.data?.categories ?? []
                        //self.setCategory()
                        self.cvCategory.reloadData()
                    } else {
                        self.arrSubCategory = responseModel.data?.sub_categories ?? []
                      //  self.setSubCategory()
                        self.cvSubCategory.reloadData()
                    }
//                    let  height = self.cvCategory.collectionViewLayout.collectionViewContentSize.height
//                    self.heightConstcvCategory.constant = height
                    
                    let  height1 = self.cvSubCategory.collectionViewLayout.collectionViewContentSize.height
                    self.heightConstcvSubCategory.constant = height1
                    
                } catch {
                    
                }
               
            }
        }
    }
}

