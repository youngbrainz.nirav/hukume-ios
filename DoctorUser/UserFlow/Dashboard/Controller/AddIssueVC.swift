//
//  AddIssueVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/27/21.
//

import UIKit

class AddIssueVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var txtDescription : UITextView!
    
    //MARK:- Varibles
    var strPlaceHolder = "Write some words to describe your issue"
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtDescription.delegate = self
        txtDescription.text = strPlaceHolder
        txtDescription.textColor = .lightGray
    }
    
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
        if txtDescription.text == strPlaceHolder || txtDescription.text.isEmpty  {
            Common().showAlert(strMsg: "Please enter some words to describe your issue", view: self)
               
        } else {
            AppSingleton.shared.bookingObject.description = txtDescription.text
            let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
            self.pushToVC(vc)
        }
            
        
    }

}
//MARK:- Textview Delegate Methods
extension AddIssueVC : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == strPlaceHolder {
            txtDescription.textColor = .black
            textView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = strPlaceHolder
            txtDescription.textColor = .lightGray
        }
    }

}
