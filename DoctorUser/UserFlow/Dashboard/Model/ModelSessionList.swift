

import Foundation
struct ModelSessionList : Codable {
	let msg : String?
	let data : [DataSessionList]?
	let status : Bool?

	enum CodingKeys: String, CodingKey {

		case msg = "msg"
		case data = "data"
		case status = "status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		msg = try values.decodeIfPresent(String.self, forKey: .msg)
		data = try values.decodeIfPresent([DataSessionList].self, forKey: .data)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
	}

}

struct DataSessionList : Codable {
    let user_name : String?
    let profile_image : String?
    let month_name : String?
    let status : String?
    let modified_date : String?
    let user_id : String?
    let duration : String?
    let cancelled_by : String?
    let booking_date : String?
    let created_date : String?
    let _id : String?
    let sub_category_id : String?
    let month : String?
    let category_name : String?
    let cancelled_by_user_type : String?
    let year : String?
    let session_date_time : String?
    let avg_rating : String?
    let date : String?
    let service_provider_id : String?
    let cancellation_date : String?
    let start_time : String?
    let end_time : String?
    let category_id : String?
    let availability_id : String?
    let description : String?

    enum CodingKeys: String, CodingKey {

        case user_name = "user_name"
        case profile_image = "profile_image"
        case month_name = "month_name"
        case status = "status"
        case modified_date = "modified_date"
        case user_id = "user_id"
        case duration = "duration"
        case cancelled_by = "cancelled_by"
        case booking_date = "booking_date"
        case created_date = "created_date"
        case _id = "_id"
        case sub_category_id = "sub_category_id"
        case month = "month"
        case category_name = "category_name"
        case cancelled_by_user_type = "cancelled_by_user_type"
        case year = "year"
        case session_date_time = "session_date_time"
        case avg_rating = "avg_rating"
        case date = "date"
        case service_provider_id = "service_provider_id"
        case cancellation_date = "cancellation_date"
        case start_time = "start_time"
        case end_time = "end_time"
        case category_id = "category_id"
        case availability_id = "availability_id"
        case description = "description"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
        profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
        month_name = try values.decodeIfPresent(String.self, forKey: .month_name)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        modified_date = try values.decodeIfPresent(String.self, forKey: .modified_date)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        duration = try values.decodeIfPresent(String.self, forKey: .duration)
        cancelled_by = try values.decodeIfPresent(String.self, forKey: .cancelled_by)
        booking_date = try values.decodeIfPresent(String.self, forKey: .booking_date)
        created_date = try values.decodeIfPresent(String.self, forKey: .created_date)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        sub_category_id = try values.decodeIfPresent(String.self, forKey: .sub_category_id)
        month = try values.decodeIfPresent(String.self, forKey: .month)
        category_name = try values.decodeIfPresent(String.self, forKey: .category_name)
        cancelled_by_user_type = try values.decodeIfPresent(String.self, forKey: .cancelled_by_user_type)
        year = try values.decodeIfPresent(String.self, forKey: .year)
        session_date_time = try values.decodeIfPresent(String.self, forKey: .session_date_time)
        avg_rating = try values.decodeIfPresent(String.self, forKey: .avg_rating)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        service_provider_id = try values.decodeIfPresent(String.self, forKey: .service_provider_id)
        cancellation_date = try values.decodeIfPresent(String.self, forKey: .cancellation_date)
        start_time = try values.decodeIfPresent(String.self, forKey: .start_time)
        end_time = try values.decodeIfPresent(String.self, forKey: .end_time)
        category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
        availability_id = try values.decodeIfPresent(String.self, forKey: .availability_id)
        description = try values.decodeIfPresent(String.self, forKey: .description)
    }

}
