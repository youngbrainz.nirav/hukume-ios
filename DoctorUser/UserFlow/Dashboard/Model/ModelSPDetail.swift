

import Foundation
struct ModelSPDetail : Codable {
	let msg : String?
	let status : Bool?
	let data : DataSPDetail?

	enum CodingKeys: String, CodingKey {

		case msg = "msg"
		case status = "status"
		case data = "data"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		msg = try values.decodeIfPresent(String.self, forKey: .msg)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		data = try values.decodeIfPresent(DataSPDetail.self, forKey: .data)
	}

}
struct DataSPDetail : Codable {
    let user_type : String?
    let _id : String?
    let last_name : String?
    let session_token : String?
    let created_date : String?
    let month_name : String?
    let email_id : String?
    let rejection_detail : String?
    let modified_date : String?
    let address : String?
    let sub_category_ids : String?
    let degree_certificate : String?
    let year : String?
    let device_token : String?
    let mobile_no : String?
    let is_visible : String?
    let user_status : String?
    let email_token : String?
    let category_name : String?
    let about_you : String?
    let avg_rating : String?
    let longitude : String?
    let reviews : [Reviews]?
    let first_name : String?
    let device_type : String?
    let profile_image : String?
    let country_code : String?
    let month : String?
    let start_time : String?
    let admin_approval : String?
    let country_code_info : String?
    let category_id : String?
    let password : String?
    let business_name : String?
    let year_of_experience : String?
    let availability_date : String?
    let latitude : String?
    let sub_category_name: String?
    let subscription_amount: String?
    let isSubscribedUser: Int?
    let sub_category_data:[Categories]?
    let available_dates: [String]?
    enum CodingKeys: String, CodingKey {

        case user_type = "user_type"
        case _id = "_id"
        case last_name = "last_name"
        case session_token = "session_token"
        case created_date = "created_date"
        case month_name = "month_name"
        case email_id = "email_id"
        case rejection_detail = "rejection_detail"
        case modified_date = "modified_date"
        case address = "address"
        case sub_category_ids = "sub_category_ids"
        case degree_certificate = "degree_certificate"
        case year = "year"
        case device_token = "device_token"
        case mobile_no = "mobile_no"
        case is_visible = "is_visible"
        case user_status = "user_status"
        case email_token = "email_token"
        case category_name = "category_name"
        case about_you = "about_you"
        case avg_rating = "avg_rating"
        case longitude = "longitude"
        case reviews = "reviews"
        case first_name = "first_name"
        case device_type = "device_type"
        case profile_image = "profile_image"
        case country_code = "country_code"
        case month = "month"
        case start_time = "start_time"
        case admin_approval = "admin_approval"
        case country_code_info = "country_code_info"
        case category_id = "category_id"
        case password = "password"
        case business_name = "business_name"
        case year_of_experience = "year_of_experience"
        case availability_date = "availability_date"
        case latitude = "latitude"
        case sub_category_name = "sub_category_name"
        case subscription_amount = "subscription_amount"
        case isSubscribedUser = "isSubscribedUser"
        case sub_category_data = "sub_category_data"
        case available_dates = "available_dates"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_type = try values.decodeIfPresent(String.self, forKey: .user_type)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
        session_token = try values.decodeIfPresent(String.self, forKey: .session_token)
        created_date = try values.decodeIfPresent(String.self, forKey: .created_date)
        month_name = try values.decodeIfPresent(String.self, forKey: .month_name)
        email_id = try values.decodeIfPresent(String.self, forKey: .email_id)
        rejection_detail = try values.decodeIfPresent(String.self, forKey: .rejection_detail)
        modified_date = try values.decodeIfPresent(String.self, forKey: .modified_date)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        sub_category_ids = try values.decodeIfPresent(String.self, forKey: .sub_category_ids)
        degree_certificate = try values.decodeIfPresent(String.self, forKey: .degree_certificate)
        year = try values.decodeIfPresent(String.self, forKey: .year)
        device_token = try values.decodeIfPresent(String.self, forKey: .device_token)
        mobile_no = try values.decodeIfPresent(String.self, forKey: .mobile_no)
        is_visible = try values.decodeIfPresent(String.self, forKey: .is_visible)
        user_status = try values.decodeIfPresent(String.self, forKey: .user_status)
        email_token = try values.decodeIfPresent(String.self, forKey: .email_token)
        category_name = try values.decodeIfPresent(String.self, forKey: .category_name)
        about_you = try values.decodeIfPresent(String.self, forKey: .about_you)
        avg_rating = try values.decodeIfPresent(String.self, forKey: .avg_rating)
        longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
        reviews = try values.decodeIfPresent([Reviews].self, forKey: .reviews)
        first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
        device_type = try values.decodeIfPresent(String.self, forKey: .device_type)
        profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
        country_code = try values.decodeIfPresent(String.self, forKey: .country_code)
        month = try values.decodeIfPresent(String.self, forKey: .month)
        start_time = try values.decodeIfPresent(String.self, forKey: .start_time)
        admin_approval = try values.decodeIfPresent(String.self, forKey: .admin_approval)
        country_code_info = try values.decodeIfPresent(String.self, forKey: .country_code_info)
        category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
        password = try values.decodeIfPresent(String.self, forKey: .password)
        business_name = try values.decodeIfPresent(String.self, forKey: .business_name)
        year_of_experience = try values.decodeIfPresent(String.self, forKey: .year_of_experience)
        availability_date = try values.decodeIfPresent(String.self, forKey: .availability_date)
        latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
        sub_category_name = try values.decodeIfPresent(String.self, forKey: .sub_category_name)
        subscription_amount = try values.decodeIfPresent(String.self, forKey: .subscription_amount)
        isSubscribedUser = try values.decodeIfPresent(Int.self, forKey: .isSubscribedUser)
        sub_category_data = try values.decodeIfPresent([Categories].self, forKey: .sub_category_data)
        available_dates = try values.decodeIfPresent([String].self, forKey: .available_dates)
    }

}
struct Reviews : Codable {
    let rating : String?
    let profile_image : String?
    let description : String?
    let user_name : String?

    enum CodingKeys: String, CodingKey {

        case rating = "rating"
        case profile_image = "profile_image"
        case description = "description"
        case user_name = "user_name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        rating = try values.decodeIfPresent(String.self, forKey: .rating)
        profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
    }

}
