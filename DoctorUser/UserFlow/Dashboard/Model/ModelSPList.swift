

import Foundation
struct ModelSPList : Codable {
	let data : [DataSPList]?
	let status : Bool?
	let msg : String?

	enum CodingKeys: String, CodingKey {

		case data = "data"
		case status = "status"
		case msg = "msg"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		data = try values.decodeIfPresent([DataSPList].self, forKey: .data)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		msg = try values.decodeIfPresent(String.self, forKey: .msg)
	}

}

struct DataSPList : Codable {
    let rating : String?
    let sub_category_id : String?
    let _id : String?
    let profile_image : String?
    let category_id : String?
    let user_name : String?

    enum CodingKeys: String, CodingKey {

        case rating = "rating"
        case sub_category_id = "sub_category_id"
        case _id = "_id"
        case profile_image = "profile_image"
        case category_id = "category_id"
        case user_name = "user_name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        rating = try values.decodeIfPresent(String.self, forKey: .rating)
        sub_category_id = try values.decodeIfPresent(String.self, forKey: .sub_category_id)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
        category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
    }

}
