

import Foundation
struct ModelUserHome : Codable {
	let status : Bool?
	let data : DataUserHome?
	let msg : String?
    let notification_count:String?
    let totalUnreadCount: String?
	enum CodingKeys: String, CodingKey {

		case status = "status"
		case data = "data"
		case msg = "msg"
        case notification_count = "notification_count"
        case totalUnreadCount = "totalUnreadCount"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		data = try values.decodeIfPresent(DataUserHome.self, forKey: .data)
		msg = try values.decodeIfPresent(String.self, forKey: .msg)
        notification_count = try values.decodeIfPresent(String.self, forKey: .notification_count)
        totalUnreadCount = try values.decodeIfPresent(String.self, forKey: .totalUnreadCount)
	}

}

struct DataUserHome : Codable {
    let categories : [Categories]?
    let upcoming_sessions : [Upcoming_sessions]?
    let past_sessions : [Past_sessions]?

    enum CodingKeys: String, CodingKey {

        case categories = "categories"
        case upcoming_sessions = "upcoming_sessions"
        case past_sessions = "past_sessions"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        categories = try values.decodeIfPresent([Categories].self, forKey: .categories)
        upcoming_sessions = try values.decodeIfPresent([Upcoming_sessions].self, forKey: .upcoming_sessions)
        past_sessions = try values.decodeIfPresent([Past_sessions].self, forKey: .past_sessions)
    }

}
struct Upcoming_sessions : Codable {
    let _id: String?
    let session_name : String?
    let current_date : String?
    let current_time : String?
    let category_name : String?
    let service_provider_name : String?
    let service_provider_image : String?
    let booking_date : String?
    let start_time : String?

    enum CodingKeys: String, CodingKey {
        case _id = "_id"
        case session_name = "session_name"
        case current_date = "current_date"
        case current_time = "current_time"
        case category_name = "category_name"
        case service_provider_name = "service_provider_name"
        case service_provider_image = "service_provider_image"
        case booking_date = "booking_date"
        case start_time = "start_time"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        session_name = try values.decodeIfPresent(String.self, forKey: .session_name)
        current_date = try values.decodeIfPresent(String.self, forKey: .current_date)
        current_time = try values.decodeIfPresent(String.self, forKey: .current_time)
        category_name = try values.decodeIfPresent(String.self, forKey: .category_name)
        service_provider_name = try values.decodeIfPresent(String.self, forKey: .service_provider_name)
        service_provider_image = try values.decodeIfPresent(String.self, forKey: .service_provider_image)
        booking_date = try values.decodeIfPresent(String.self, forKey: .booking_date)
        start_time = try values.decodeIfPresent(String.self, forKey: .start_time)
    }

}
struct Past_sessions : Codable {
    let _id: String?
    let session_name : String?
    let current_date : String?
    let current_time : String?
    let category_name : String?
    let service_provider_name : String?
    let service_provider_image : String?
    let booking_date : String?
    let start_time : String?

    enum CodingKeys: String, CodingKey {
        case _id = "_id"
        case session_name = "session_name"
        case current_date = "current_date"
        case current_time = "current_time"
        case category_name = "category_name"
        case service_provider_name = "service_provider_name"
        case service_provider_image = "service_provider_image"
        case booking_date = "booking_date"
        case start_time = "start_time"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        session_name = try values.decodeIfPresent(String.self, forKey: .session_name)
        current_date = try values.decodeIfPresent(String.self, forKey: .current_date)
        current_time = try values.decodeIfPresent(String.self, forKey: .current_time)
        category_name = try values.decodeIfPresent(String.self, forKey: .category_name)
        service_provider_name = try values.decodeIfPresent(String.self, forKey: .service_provider_name)
        service_provider_image = try values.decodeIfPresent(String.self, forKey: .service_provider_image)
        booking_date = try values.decodeIfPresent(String.self, forKey: .booking_date)
        start_time = try values.decodeIfPresent(String.self, forKey: .start_time)
    }

}
