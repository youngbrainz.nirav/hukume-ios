

import Foundation
struct ModelSubscription : Codable {
	let data : [DataSubscription]?
	let status : Bool?
	let msg : String?

	enum CodingKeys: String, CodingKey {

		case data = "data"
		case status = "status"
		case msg = "msg"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		data = try values.decodeIfPresent([DataSubscription].self, forKey: .data)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		msg = try values.decodeIfPresent(String.self, forKey: .msg)
	}

}
struct DataSubscription : Codable {
    let _id : String?
    let status : String?
    let created_date : String?
    let month_name : String?
    let price : String?
    let modified_date : String?
    let month : String?
    let year : String?
    let description : String?
    let month_duration : String?
    let name : String?
    let benefits:[String]?
    let is_active_plan:String?
    let expiry_date:String?
    
    enum CodingKeys: String, CodingKey {

        case _id = "_id"
        case status = "status"
        case created_date = "created_date"
        case month_name = "month_name"
        case price = "price"
        case modified_date = "modified_date"
        case month = "month"
        case year = "year"
        case description = "description"
        case month_duration = "month_duration"
        case name = "name"
        case benefits = "benefits"
        case is_active_plan = "is_active_plan"
        case expiry_date = "expiry_date"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_date = try values.decodeIfPresent(String.self, forKey: .created_date)
        month_name = try values.decodeIfPresent(String.self, forKey: .month_name)
        price = try values.decodeIfPresent(String.self, forKey: .price)
        modified_date = try values.decodeIfPresent(String.self, forKey: .modified_date)
        month = try values.decodeIfPresent(String.self, forKey: .month)
        year = try values.decodeIfPresent(String.self, forKey: .year)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        month_duration = try values.decodeIfPresent(String.self, forKey: .month_duration)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        benefits = try values.decodeIfPresent([String].self, forKey: .benefits)
        is_active_plan = try values.decodeIfPresent(String.self, forKey: .is_active_plan)
        expiry_date = try values.decodeIfPresent(String.self, forKey: .expiry_date)
    }

}
