//
//  SubscriptionVC.swift
//  DoctorUser
//
//  Created by VATSAL on 1/31/22.
//

import UIKit

class SubscriptionVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var tblBenifit : UITableView!
    @IBOutlet weak var heightConstTblBenifit : NSLayoutConstraint!
    @IBOutlet weak var btnPlan1 : UIButton!
    @IBOutlet weak var btnPlan2 : UIButton!
    @IBOutlet weak var btnPlan3 : UIButton!
    
    @IBOutlet weak var btnPay: UIButton!
    @IBOutlet weak var stkPlans: UIStackView!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblSep1: UILabel!
    @IBOutlet weak var lblSep2: UILabel!
    @IBOutlet weak var lblSep3: UILabel!
    @IBOutlet weak var viewBorder: UIView!
    @IBOutlet weak var lblPlanName: UILabel!
    @IBOutlet weak var lblPlanDescription: UILabel!
    @IBOutlet weak var lblExpDate: UILabel!
    // MARK: - Varibles
    var arrBenifit = ["Benifit - 1","Benifit - 2","Benifit - 3","Benifit - 4"]
    var arrSubscription : [DataSubscription] = []
    var subscription_plan_id = ""
    var isFromMenu = false
    let color1 = UIColor.init(red: 226/255, green: 237/255, blue: 234/255, alpha: 1)
    let color2 = UIColor.init(red: 241/255, green: 229/255, blue: 172/255, alpha: 1)
    let color3 = UIColor.init(red: 174/255, green: 247/255, blue: 227/255, alpha: 1)
    var amount = ""
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        wsGetsubscription()
        initSetup()
//        if isFromMenu {
//            btnBack.isHidden = false
//        } else {
//            btnBack.isHidden = true
//        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        btnPlan1.setRound(withBorderColor: .themeColor, andCornerRadious: 0.01, borderWidth: 1)
        btnPlan2.setRound(withBorderColor: .themeColor, andCornerRadious: 0.01, borderWidth: 1)
        btnPlan3.setRound(withBorderColor: .themeColor, andCornerRadious: 0.01, borderWidth: 1)
        viewBorder.setRound(withBorderColor: .themeColor, andCornerRadious: 0.01, borderWidth: 1)
    }
    func initSetup(){
        btnPlan1.titleLabel?.numberOfLines = 2
        btnPlan2.titleLabel?.numberOfLines = 2
        btnPlan3.titleLabel?.numberOfLines = 2
//        btnPlan1.backgroundColor = .white
//        btnPlan2.backgroundColor = .white
//        btnPlan3.backgroundColor = .white
        btnPlan1.setBackgroundColor(color: .white, forState: .normal)
        btnPlan1.setBackgroundColor(color: color1, forState: .selected)
        btnPlan2.setBackgroundColor(color: .white, forState: .normal)
        btnPlan2.setBackgroundColor(color: color2, forState: .selected)
        btnPlan3.setBackgroundColor(color: .white, forState: .normal)
        btnPlan3.setBackgroundColor(color: color3, forState: .selected)
        btnPlan1.setTitleColor(.themeColor, for: .selected)
        btnPlan1.setTitleColor(.black, for: .normal)
        btnPlan2.setTitleColor(.themeColor, for: .selected)
        btnPlan2.setTitleColor(.black, for: .normal)
        btnPlan3.setTitleColor(.themeColor, for: .selected)
        btnPlan3.setTitleColor(.black, for: .normal)
        btnPlan1.isSelected = true
        let contentInset = UIEdgeInsets.init(top: 0, left: 10, bottom: 0, right: 10)
        btnPlan1.contentEdgeInsets = contentInset
        btnPlan2.contentEdgeInsets = contentInset
        btnPlan3.contentEdgeInsets = contentInset
        lblSep2.isHidden = true
        lblSep3.isHidden = true
    }
    
    // MARK: - Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        if isFromMenu {
        self.navigationController?.popViewController(animated: true)
        } else {
            alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "Are you sure you want to logout?") { (status) in
                if status {
                    self.wsLogout()
                }
            }
        }
    }

    @IBAction func btnPlan1Action(_ sender: Any) {
        if arrSubscription.count < 3 {return}
        btnPlan1.isSelected = true
        btnPlan2.isSelected = false
        btnPlan3.isSelected = false
        lblSep1.isHidden = false
        lblSep2.isHidden = true
        lblSep3.isHidden = true
        setActivePlan(tag: 1)
        viewBorder.backgroundColor = color1
        lblSep1.backgroundColor = color1
        amount = arrSubscription[0].price ?? ""
        lblPlanName.text = arrSubscription[0].name ?? ""
        lblPlanDescription.text = arrSubscription[0].description ?? ""
        subscription_plan_id = arrSubscription[0]._id  ?? ""
        arrBenifit = arrSubscription[0].benefits ?? []
        tblBenifit.reloadData()
        heightConstTblBenifit.constant = tblBenifit.contentSize.height
        DispatchQueue.main.async {
            self.heightConstTblBenifit.constant = self.tblBenifit.contentSize.height
        }
//        btnPlan1.backgroundColor = .themeColor
//        btnPlan2.backgroundColor = .white
//        btnPlan3.backgroundColor = .white
    }
    
    @IBAction func btnPlan2Action(_ sender: Any) {
//
        if arrSubscription.count < 3 {return}
        btnPlan2.isSelected = true
        btnPlan1.isSelected = false
        btnPlan3.isSelected = false
        lblSep2.isHidden = false
        lblSep1.isHidden = true
        lblSep3.isHidden = true
        setActivePlan(tag: 2)
        amount = arrSubscription[1].price ?? ""
        lblSep2.backgroundColor = color2
        viewBorder.backgroundColor = color2
        lblPlanName.text = arrSubscription[1].name ?? ""
        lblPlanDescription.text = arrSubscription[1].description ?? ""
        subscription_plan_id = arrSubscription[1]._id  ?? ""
        arrBenifit = arrSubscription[1].benefits ?? []
        tblBenifit.reloadData()
        heightConstTblBenifit.constant = tblBenifit.contentSize.height
        DispatchQueue.main.async {
            self.heightConstTblBenifit.constant = self.tblBenifit.contentSize.height
        }
//        btnPlan2.backgroundColor = .themeColor
//        btnPlan1.backgroundColor = .white
//        btnPlan3.backgroundColor = .white
    }
    @IBAction func btnPlan3Action(_ sender: Any) {
        if arrSubscription.count < 3 {return}
        btnPlan3.isSelected = true
        btnPlan1.isSelected = false
        btnPlan2.isSelected = false
        lblSep3.isHidden = false
        lblSep2.isHidden = true
        lblSep1.isHidden = true
        setActivePlan(tag: 3)
        amount = arrSubscription[2].price ?? ""
        lblSep3.backgroundColor = color3
        viewBorder.backgroundColor = color3
        lblPlanName.text = arrSubscription[2].name ?? ""
        lblPlanDescription.text = arrSubscription[2].description ?? ""
        subscription_plan_id = arrSubscription[2]._id  ?? ""
        arrBenifit = arrSubscription[2].benefits ?? []
        tblBenifit.reloadData()
        heightConstTblBenifit.constant = tblBenifit.contentSize.height
        DispatchQueue.main.async {
            self.heightConstTblBenifit.constant = self.tblBenifit.contentSize.height
        }
//        btnPlan3.backgroundColor = .themeColor
//        btnPlan2.backgroundColor = .white
//        btnPlan1.backgroundColor = .white
    }
    
    @IBAction func btnPayAction(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.Payment, bundle: nil).instantiateViewController(withIdentifier: "CardListVC") as! CardListVC
        vc.subscription_plan_id = subscription_plan_id
        vc.amount = amount
        self.pushToVC(vc)
               
    }
    func setActivePlan(tag:Int){
        btnPay.isHidden = false
        lblExpDate.isHidden = true
        if arrSubscription[0].is_active_plan == "1" {
            switch tag {
            case 1:
               // btnPay.setTitle("Exp. date - \(arrSubscription[0].expiry_date ?? "")", for: .normal)
                btnPay.isHidden = true
                lblExpDate.text = "Exp. date - \(arrSubscription[0].expiry_date ?? "")"
                lblExpDate.isHidden = false
                break
            case 2:
                btnPay.setTitle("Upgrade", for: .normal)
                break
            case 3:
                btnPay.setTitle("Upgrade", for: .normal)
                break
            default:
                break
            }
            
        } else if arrSubscription[1].is_active_plan == "1" {
            switch tag {
            case 1:
                btnPay.setTitle("Downgrade", for: .normal)
                break
            case 2:
              //  btnPay.setTitle("Exp. date - \(arrSubscription[1].expiry_date ?? "")", for: .normal)
                btnPay.isHidden = true
                lblExpDate.text = "Exp. date - \(arrSubscription[1].expiry_date ?? "")"
                lblExpDate.isHidden = false
                break
            case 3:
                btnPay.setTitle("Upgrade", for: .normal)
                break
            default:
                break
            }
            
        } else if arrSubscription[2].is_active_plan == "1" {
            switch tag {
            case 1:
                btnPay.setTitle("Downgrade", for: .normal)
                
                break
            case 2:
                btnPay.setTitle("Downgrade", for: .normal)
                break
            case 3:
             //   btnPay.setTitle("Exp. date - \(arrSubscription[2].expiry_date ?? "")", for: .normal)
                btnPay.isHidden = true
                lblExpDate.text = "Exp. date - \(arrSubscription[2].expiry_date ?? "")"
                lblExpDate.isHidden = false
                break
            default:
                break
            }
        }
    }
    //MARK: - API Calling
    func wsGetsubscription(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_SUBSCRIPTION_PLAN, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelSubscription.self, from: data!)
                    self.arrSubscription = responseModel.data ?? []
                    if self.arrSubscription.count >= 3 {
                        self.btnPlan1.setTitle((self.arrSubscription[0].price ?? "")+"/ Month", for: .normal)
                        self.btnPlan2.setTitle((self.arrSubscription[1].price ?? "")+"/ \(self.arrSubscription[1].month_duration ?? "") Month", for: .normal)
                        self.btnPlan3.setTitle((self.arrSubscription[2].price ?? "")+"/ \(self.arrSubscription[2].month_duration ?? "") Month", for: .normal)
                        self.subscription_plan_id = self.arrSubscription[0]._id ?? ""
                        self.btnPlan1Action(self)
                    }
                }
                catch {
                    
                }
                
            }
        }
    }
    func wsLogout(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.LOGOUT_USER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                appDel.gotoLogin()
            }
        }
    }


}
//MARK: - TableView Delegate Method
extension SubscriptionVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrBenifit.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CardTVCell
        cell.lblName.text =  self.arrBenifit[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

