//
//  SPPaymentHistoryListVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/24/21.
//

import UIKit

class SPPaymentHistoryListVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tblView : UITableView!
    
    //MARK:- Varibles
    
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

//MARK:- TableView Delegate Method
extension SPPaymentHistoryListVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 15
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PaymentTVCell
        return cell
    }
  
}

