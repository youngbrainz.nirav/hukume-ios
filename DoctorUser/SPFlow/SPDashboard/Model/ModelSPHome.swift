

import Foundation
struct ModelSPHome : Codable {
	let status : Bool?
	let data : DataSPHome?
	let msg : String?
    let approval_text: String?
    let notification_count:String?
	enum CodingKeys: String, CodingKey {

		case status = "status"
		case data = "data"
		case msg = "msg"
        case approval_text = "approval_text"
        case notification_count = "notification_count"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		data = try values.decodeIfPresent(DataSPHome.self, forKey: .data)
		msg = try values.decodeIfPresent(String.self, forKey: .msg)
        approval_text = try values.decodeIfPresent(String.self, forKey: .approval_text)
        notification_count = try values.decodeIfPresent(String.self, forKey: .notification_count)
	}

}
struct DataSPHome : Codable {
    let advertisement : [Advertisement]?
    let last_chat_list : [Last_chat_list]?
    let reviews : [Reviews]?

    enum CodingKeys: String, CodingKey {

        case advertisement = "advertisement"
        case last_chat_list = "last_chat_list"
        case reviews = "reviews"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        advertisement = try values.decodeIfPresent([Advertisement].self, forKey: .advertisement)
        last_chat_list = try values.decodeIfPresent([Last_chat_list].self, forKey: .last_chat_list)
        reviews = try values.decodeIfPresent([Reviews].self, forKey: .reviews)
    }

}
struct Last_chat_list : Codable {
    let id : String?
    let session_id : String?
    let sender : String?
    let recipient : String?
    let sender_id : String?
    let recipient_id : String?
    let sender_unread : String?
    let receiver_unread : String?
    let isdeleted : String?
    let lastConversationDate : String?
    let lastMsg : String?
    let createdAt : String?
    let updatedAt : String?
    let sender_name : String?
    let sender_profile_image : String?
    let conversation_id : String?
    let user_id: String?
    let unread_count:String?
    enum CodingKeys: String, CodingKey {

        case id = "id"
        case session_id = "session_id"
        case sender = "sender"
        case recipient = "recipient"
        case sender_id = "sender_id"
        case recipient_id = "recipient_id"
        case sender_unread = "sender_unread"
        case receiver_unread = "receiver_unread"
        case isdeleted = "isdeleted"
        case lastConversationDate = "lastConversationDate"
        case lastMsg = "lastMsg"
        case createdAt = "createdAt"
        case updatedAt = "updatedAt"
        case sender_name = "sender_name"
        case sender_profile_image = "sender_profile_image"
        case conversation_id = "conversation_id"
        case user_id = "user_id"
        case unread_count = "unread_count"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        session_id = try values.decodeIfPresent(String.self, forKey: .session_id)
        sender = try values.decodeIfPresent(String.self, forKey: .sender)
        recipient = try values.decodeIfPresent(String.self, forKey: .recipient)
        sender_id = try values.decodeIfPresent(String.self, forKey: .sender_id)
        recipient_id = try values.decodeIfPresent(String.self, forKey: .recipient_id)
        sender_unread = try values.decodeIfPresent(String.self, forKey: .sender_unread)
        receiver_unread = try values.decodeIfPresent(String.self, forKey: .receiver_unread)
        isdeleted = try values.decodeIfPresent(String.self, forKey: .isdeleted)
        lastConversationDate = try values.decodeIfPresent(String.self, forKey: .lastConversationDate)
        lastMsg = try values.decodeIfPresent(String.self, forKey: .lastMsg) ?? ""
        createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
        updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt)
        sender_name = try values.decodeIfPresent(String.self, forKey: .sender_name)
        sender_profile_image = try values.decodeIfPresent(String.self, forKey: .sender_profile_image)
        conversation_id = try values.decodeIfPresent(String.self, forKey: .conversation_id)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        unread_count = try values.decodeIfPresent(String.self, forKey: .unread_count)
    }

}
struct Advertisement : Codable {
    let _id : String?
    let status : String?
    let created_date : String?
    let month_name : String?
    let modified_date : String?
    let image : String?
    let date : String?
    let month : String?
    let year : String?
    let sender_profile_image : String?
    let name : String?
    let file_type: String?
    enum CodingKeys: String, CodingKey {

        case _id = "_id"
        case status = "status"
        case created_date = "created_date"
        case month_name = "month_name"
        case modified_date = "modified_date"
        case image = "image"
        case date = "date"
        case month = "month"
        case year = "year"
        case sender_profile_image = "sender_profile_image"
        case name = "name"
        case file_type = "file_type"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        created_date = try values.decodeIfPresent(String.self, forKey: .created_date)
        month_name = try values.decodeIfPresent(String.self, forKey: .month_name)
        modified_date = try values.decodeIfPresent(String.self, forKey: .modified_date)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        month = try values.decodeIfPresent(String.self, forKey: .month)
        year = try values.decodeIfPresent(String.self, forKey: .year)
        sender_profile_image = try values.decodeIfPresent(String.self, forKey: .sender_profile_image)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        file_type = try values.decodeIfPresent(String.self, forKey: .file_type)
    }

}
