

import Foundation
struct ModelCategories : Codable {
	let status : Bool?
	let data : DataCategory?
	let msg : String?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case data = "data"
		case msg = "msg"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		data = try values.decodeIfPresent(DataCategory.self, forKey: .data)
		msg = try values.decodeIfPresent(String.self, forKey: .msg)
	}

}
struct DataCategory : Codable {
    let categories : [Categories]?
    let sub_categories : [Categories]?

    enum CodingKeys: String, CodingKey {

        case categories = "categories"
        case sub_categories = "sub_categories"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        categories = try values.decodeIfPresent([Categories].self, forKey: .categories)
        sub_categories = try values.decodeIfPresent([Categories].self, forKey: .sub_categories)
    }

}
struct Categories : Codable {
    let _id : String?
    let name : String?
    let description : String?
    let search_keyword : String?
    let icon : String?
    let is_show_home_screen : String?
    let status : String?
    let date : String?
    let created_date : String?
    let modified_date : String?
    let month : String?
    let month_name : String?
    let year : String?

    enum CodingKeys: String, CodingKey {

        case _id = "_id"
        case name = "name"
        case description = "description"
        case search_keyword = "search_keyword"
        case icon = "icon"
        case is_show_home_screen = "is_show_home_screen"
        case status = "status"
        case date = "date"
        case created_date = "created_date"
        case modified_date = "modified_date"
        case month = "month"
        case month_name = "month_name"
        case year = "year"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        search_keyword = try values.decodeIfPresent(String.self, forKey: .search_keyword)
        icon = try values.decodeIfPresent(String.self, forKey: .icon)
        is_show_home_screen = try values.decodeIfPresent(String.self, forKey: .is_show_home_screen)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        created_date = try values.decodeIfPresent(String.self, forKey: .created_date)
        modified_date = try values.decodeIfPresent(String.self, forKey: .modified_date)
        month = try values.decodeIfPresent(String.self, forKey: .month)
        month_name = try values.decodeIfPresent(String.self, forKey: .month_name)
        year = try values.decodeIfPresent(String.self, forKey: .year)
    }

}
