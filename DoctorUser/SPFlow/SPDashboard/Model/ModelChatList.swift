

import Foundation
struct ModelChatList : Codable {
	let status : Bool?
	let data : [Last_chat_list]?
	let msg : String?
	let totalUnreadCount : String?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case data = "data"
		case msg = "msg"
		case totalUnreadCount = "totalUnreadCount"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		data = try values.decodeIfPresent([Last_chat_list].self, forKey: .data)
		msg = try values.decodeIfPresent(String.self, forKey: .msg)
        do {
		totalUnreadCount = try values.decodeIfPresent(String.self, forKey: .totalUnreadCount)
        } catch {
            let count = try values.decodeIfPresent(Int.self, forKey: .totalUnreadCount)
            totalUnreadCount = "\(count ?? 0)"
        }
	}

}
struct DataChatList : Codable {
    let id : Int?
    let session_id : String?
    let sender : Int?
    let recipient : Int?
    let sender_id : Int?
    let recipient_id : Int?
    let sender_unread : Int?
    let receiver_unread : Int?
    let isdeleted : Int?
    let lastConversationDate : String?
    let lastMsg : String?
    let createdAt : String?
    let updatedAt : String?
    let senderID : Int?
    let senderName : String?
    let recipientID : Int?
    let recipientName : String?
    let senderProfileImage : String?
    let recipientProfileImage : String?
    let dateInfo : String?
    let sender_profile_image_url : String?
    let recipient_profile_image_url : String?
    let user_profile_image_url : String?
    let user_id : String?
    let to_user_id : Int?
    let user_name : String?
    let unread_count : Int?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case session_id = "session_id"
        case sender = "sender"
        case recipient = "recipient"
        case sender_id = "sender_id"
        case recipient_id = "recipient_id"
        case sender_unread = "sender_unread"
        case receiver_unread = "receiver_unread"
        case isdeleted = "isdeleted"
        case lastConversationDate = "lastConversationDate"
        case lastMsg = "lastMsg"
        case createdAt = "createdAt"
        case updatedAt = "updatedAt"
        case senderID = "senderID"
        case senderName = "senderName"
        case recipientID = "recipientID"
        case recipientName = "recipientName"
        case senderProfileImage = "senderProfileImage"
        case recipientProfileImage = "recipientProfileImage"
        case dateInfo = "dateInfo"
        case sender_profile_image_url = "sender_profile_image_url"
        case recipient_profile_image_url = "recipient_profile_image_url"
        case user_profile_image_url = "user_profile_image_url"
        case user_id = "user_id"
        case to_user_id = "to_user_id"
        case user_name = "user_name"
        case unread_count = "unread_count"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        session_id = try values.decodeIfPresent(String.self, forKey: .session_id)
        sender = try values.decodeIfPresent(Int.self, forKey: .sender)
        recipient = try values.decodeIfPresent(Int.self, forKey: .recipient)
        sender_id = try values.decodeIfPresent(Int.self, forKey: .sender_id)
        recipient_id = try values.decodeIfPresent(Int.self, forKey: .recipient_id)
        sender_unread = try values.decodeIfPresent(Int.self, forKey: .sender_unread)
        receiver_unread = try values.decodeIfPresent(Int.self, forKey: .receiver_unread)
        isdeleted = try values.decodeIfPresent(Int.self, forKey: .isdeleted)
        lastConversationDate = try values.decodeIfPresent(String.self, forKey: .lastConversationDate)
        lastMsg = try values.decodeIfPresent(String.self, forKey: .lastMsg)
        createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
        updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt)
        senderID = try values.decodeIfPresent(Int.self, forKey: .senderID)
        senderName = try values.decodeIfPresent(String.self, forKey: .senderName)
        recipientID = try values.decodeIfPresent(Int.self, forKey: .recipientID)
        recipientName = try values.decodeIfPresent(String.self, forKey: .recipientName)
        senderProfileImage = try values.decodeIfPresent(String.self, forKey: .senderProfileImage)
        recipientProfileImage = try values.decodeIfPresent(String.self, forKey: .recipientProfileImage)
        dateInfo = try values.decodeIfPresent(String.self, forKey: .dateInfo)
        sender_profile_image_url = try values.decodeIfPresent(String.self, forKey: .sender_profile_image_url)
        recipient_profile_image_url = try values.decodeIfPresent(String.self, forKey: .recipient_profile_image_url)
        user_profile_image_url = try values.decodeIfPresent(String.self, forKey: .user_profile_image_url)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        to_user_id = try values.decodeIfPresent(Int.self, forKey: .to_user_id)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
        unread_count = try values.decodeIfPresent(Int.self, forKey: .unread_count)
    }

}
