/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct ModelAvailability : Codable {
	let status : Bool?
	let data : [DataAvailability]?
	let msg : String?
    let available_dates: [String]?
	enum CodingKeys: String, CodingKey {

		case status = "status"
		case data = "data"
		case msg = "msg"
        case available_dates = "available_dates"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		data = try values.decodeIfPresent([DataAvailability].self, forKey: .data)
		msg = try values.decodeIfPresent(String.self, forKey: .msg)
        available_dates = try values.decodeIfPresent([String].self, forKey: .available_dates)
	}

}
struct DataAvailability : Codable {
    let modified_date : String?
    let category_id : String?
    let _id : String?
    let user_id : String?
    let sub_category_id : String?
    let availability_from_time : String?
    let total_availability_min : String?
    let date : String?
    let created_date : String?
    let month_name : String?
    let availability_date : String?
    let month : String?
    let year : String?
    let availability_to_time : String?
    let break_time : String?
    
    enum CodingKeys: String, CodingKey {

        case modified_date = "modified_date"
        case category_id = "category_id"
        case _id = "_id"
        case user_id = "user_id"
        case sub_category_id = "sub_category_id"
        case availability_from_time = "availability_from_time"
        case total_availability_min = "total_availability_min"
        case date = "date"
        case created_date = "created_date"
        case month_name = "month_name"
        case availability_date = "availability_date"
        case month = "month"
        case year = "year"
        case availability_to_time = "availability_to_time"
        case break_time = "break_time"
        
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        modified_date = try values.decodeIfPresent(String.self, forKey: .modified_date)
        category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
        _id = try values.decodeIfPresent(String.self, forKey: ._id)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        sub_category_id = try values.decodeIfPresent(String.self, forKey: .sub_category_id)
        availability_from_time = try values.decodeIfPresent(String.self, forKey: .availability_from_time)
        total_availability_min = try values.decodeIfPresent(String.self, forKey: .total_availability_min)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        created_date = try values.decodeIfPresent(String.self, forKey: .created_date)
        month_name = try values.decodeIfPresent(String.self, forKey: .month_name)
        availability_date = try values.decodeIfPresent(String.self, forKey: .availability_date)
        month = try values.decodeIfPresent(String.self, forKey: .month)
        year = try values.decodeIfPresent(String.self, forKey: .year)
        availability_to_time = try values.decodeIfPresent(String.self, forKey: .availability_to_time)
        break_time = try values.decodeIfPresent(String.self, forKey: .break_time)
        
    }

}
