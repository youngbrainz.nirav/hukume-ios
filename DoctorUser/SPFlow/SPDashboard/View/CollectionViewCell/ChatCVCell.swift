//
//  ChatCVCell.swift
//  DoctorUser
//
//  Created by VATSAL on 2/1/22.
//

import UIKit

class ChatCVCell: UICollectionViewCell {
 
    
    @IBOutlet weak var imgView : UIImageView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var viewMain : UIView!
 
    func setUserHomeData(data:Categories) {
        lblName.text = "\(data.name ?? "")"
        imgView.downloadedFrom(link: data.icon ?? "")
        viewMain.addShadowOnBottom()
    }
    func setChatData(data:Last_chat_list) {
        imgView.downloadedFrom(link: data.sender_profile_image ?? "")
        lblName.text = data.sender_name
        lblDescription.text = data.lastMsg
    }
}

class SPHomeCVCell:UICollectionViewCell {
    @IBOutlet weak var imgView : UIImageView!
    
}
