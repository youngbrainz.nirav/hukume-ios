//
//  EditAvailabilityVC.swift
//  DoctorSP
//
//  Created by VATSAL on 1/8/22.
//

import UIKit
import DropDown

class EditAvailabilityVC: UIViewController {
    
    
    
    //MARK: - Outlets
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var lblBreakTime : UILabel!
    @IBOutlet weak var imgTimeDown : UIImageView!
    @IBOutlet weak var imgBreakTimeDown : UIImageView!
    
    @IBOutlet weak var viewTime : UIView!
    @IBOutlet weak var viewBreakTime : UIView!
    
    @IBOutlet weak var btnTime : UIButton!
    @IBOutlet weak var btnBreakTime : UIButton!
    
    @IBOutlet weak var txtTime : UITextField!
    
    //MARK: - Varibles
    let ddTime = DropDown()
    let ddBreakTime = DropDown()
    
    var arrTime = ["15 min","30 min","45 min","60 min", "90 min", "120 min"]
    var arrBreakTime = ["5 min","10 min","20 min"]
    var objAvailability:DataAvailability?
    var selectedDate:Date = Date()
    var passSelectedDate:((_ date:Date)->())?
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTime.text = "XX min"
        // Do any additional setup after loading the view.
        setDropDowns()
    }
    
    //MARK: - Set DropDowns
    func setDropDowns(){
        //Category
//        ddCategory.direction = .bottom
        txtTime.delegate =  self
        //Time
        self.ddTime.anchorView = viewTime
        self.ddTime.dataSource = arrTime
        self.ddTime.bottomOffset = CGPoint(x: 0, y:(self.ddTime.anchorView?.plainView.bounds.height)!)
        self.ddTime.topOffset = CGPoint(x: 0, y:-(self.ddTime.anchorView?.plainView.bounds.height)!)
        ddTime.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblTime.text = self.arrTime[index]
        }
        
        self.ddBreakTime.anchorView = viewBreakTime
        self.ddBreakTime.dataSource = arrBreakTime
        self.ddBreakTime.bottomOffset = CGPoint(x: 0, y:(self.ddBreakTime.anchorView?.plainView.bounds.height)!)
        self.ddBreakTime.topOffset = CGPoint(x: 0, y:-(self.ddBreakTime.anchorView?.plainView.bounds.height)!)
        ddBreakTime.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblBreakTime.text = self.arrBreakTime[index]
        }
        txtTime.text = objAvailability?.availability_from_time
        lblTime.text = objAvailability?.total_availability_min
        self.lblTime.textColor = .black
        viewTime.backgroundColor =  .white
        imgTimeDown.image =  UIImage(named: "ic_down_black")
    }
    //MARK: - Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnRemoveAvailabilityAction(_ sender: Any) {
        wsRemoveAvailability()
    }
    @IBAction func btnDropDownAction(_ sender: UIButton) {
         if sender.tag == 3{
            print("time")
            self.lblTime.textColor = .black
            viewTime.backgroundColor =  .white
            imgTimeDown.image =  UIImage(named: "ic_down_black")
            ddTime.show()
        }else if sender.tag == 4{
            print("break Time")
            self.lblBreakTime.textColor = .black
            viewBreakTime.backgroundColor =  .white
            imgBreakTimeDown.image =  UIImage(named: "ic_down_black")
            ddBreakTime.show()
        }
        
        
    }
    @IBAction func btnDateAction(_ sender: Any) {
        
    }
    
    @IBAction func btnAddAction(_ sender: Any) {

        wsAddAvailability()
    }

    func wsAddAvailability(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.category_id] = objAvailability?.category_id
        dictParam[PARAMS.sub_category_id] = objAvailability?.sub_category_id
        dictParam[PARAMS.availability_date] = objAvailability?.availability_date
        dictParam[PARAMS.availability_from_time] = Utils.converStringtoDate(txtTime.text!, "hh:mm a", toFormate: "HH:mm")
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        if lblBreakTime.text?.lowercased() != "select break time" {
        dictParam[PARAMS.break_time] =  lblBreakTime.text?.replacingOccurrences(of: " min", with: "")
        }
        dictParam[PARAMS.total_availability_min] = lblTime.text?.replacingOccurrences(of: " min", with: "")
        dictParam[PARAMS.availability_id] = objAvailability?._id
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.ADD_NEW_AVAILABILITY, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                self.passSelectedDate!(self.selectedDate)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func wsRemoveAvailability(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.availability_id] = objAvailability?._id
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.REMOVE_AVAILABILITY, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func openDatePicker(){
        let dialog = CustomDatePickerDialog.showCustomDatePickerDialog(title: "Date and Time", titleLeftButton: "Cancel", titleRightButton: "Okay")
        dialog.setDatePickerForFutureTrip(mode: .time)
        let isCurrentDate = Calendar.current.isDateInToday(selectedDate)
        dialog.setDate(date: isCurrentDate ? Date():selectedDate)
        dialog.onClickLeftButton = {
            dialog.removeFromSuperview()
        }
        dialog.onClickRightButton = {(selectedDate) in
            dialog.removeFromSuperview()
            self.txtTime.text = Utils.convertDatetoString(selectedDate, "dd-MM-yyyy HH:mm", toFormate: "hh:mm a")
        }
    }
}
//MARK: - UITextFieldDelegate Method
extension EditAvailabilityVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let currentText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) else { return true }
         if textField == txtTime {
            if (currentText.utf16.count) <= 5{
                textField.setText(to: currentText.grouping(every: 2, with: ":"), preservingCursor: true)
                return false
            }
        }else {
            
        }
        return false
    
    }
    func checkValidation()->Bool {
        if txtTime.isEmpty {
            Common().showAlert(strMsg: "Please enter time.", view: self)
            return false
        }
        else if lblTime.text == "XX min" {
            Common().showAlert(strMsg: "Please select time.", view: self)
            return false
        }
//        else if lblBreakTime.text?.lowercased() == "select break time" {
//            Common().showAlert(strMsg: "Please select break time.", view: self)
//            return false
//        }
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtTime {
            openDatePicker()
            return false
        }
        return true
    }
}
