//
//  AddAvailabilityVC.swift
//  DoctorSP
//
//  Created by VATSAL on 1/8/22.
//

import UIKit
import DropDown

class AddAvailabilityVC: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var lblCategory : UILabel!
    @IBOutlet weak var lblSubCategory : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var lblBreakTime : UILabel!
    @IBOutlet weak var imgCategoryDown : UIImageView!
    @IBOutlet weak var imgSubCategoryDown : UIImageView!
    @IBOutlet weak var imgTimeDown : UIImageView!
    @IBOutlet weak var imgBreakTimeDown : UIImageView!
    
    @IBOutlet weak var viewCategory : UIView!
    @IBOutlet weak var viewSubCategory : UIView!
    @IBOutlet weak var viewTime : UIView!
    @IBOutlet weak var viewBreakTime : UIView!
    
    
    @IBOutlet weak var btnCategory : UIButton!
    @IBOutlet weak var btnSubCategory : UIButton!
    @IBOutlet weak var btnTime : UIButton!
    @IBOutlet weak var btnBreakTime : UIButton!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var txtTime : UITextField!
    
    //MARK: - Varibles
    let ddCategory = DropDown()
    let ddSubCategory = DropDown()
    let ddTime = DropDown()
    let ddBreakTime = DropDown()
    
    var arrCategoryString:[String] = []
    var arrSubCategoryString:[String] = []
    var arrCategory:[Categories] = []
    var arrSubCategory:[Categories] = AppSingleton.shared.user?.sub_category_data ?? []
    var arrTime = ["15 min","30 min","45 min","60 min", "90 min", "120 min"]
    var arrBreakTime = ["5 min","10 min","20 min"]
    var selectedDate = Date()
    var strDate = ""
    var passSelectedDate:((_ date:Date)->())?
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //wsGetCategorySubCategories(id:AppSingleton.shared.user?.category_id ?? "")
        // Do any additional setup after loading the view.
        setDropDowns()
        setSubCategory()
        txtTime.font = FontHelper.Font()
        lblTime.text = "XX min"
        lblDate.text = Utils.converStringtoDate(strDate, "yyyy-MM-dd", toFormate: "dd-MM-yyyy")
        self.lblDate.textColor = .black
    }
    
    //MARK: - Set DropDowns
    func setDropDowns(){
        //Category
//        ddCategory.direction = .bottom
        
        txtTime.delegate =  self
        imgCategoryDown.isHidden = true
        self.lblCategory.textColor = .black
        viewCategory.backgroundColor =  .white
        lblCategory.text = AppSingleton.shared.user?.category_name
        //Time
        self.ddTime.anchorView = viewTime
        self.ddTime.dataSource = arrTime
        self.ddTime.bottomOffset = CGPoint(x: 0, y:(self.ddTime.anchorView?.plainView.bounds.height)!)
        self.ddTime.topOffset = CGPoint(x: 0, y:-(self.ddTime.anchorView?.plainView.bounds.height)!)
        ddTime.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblTime.text = self.arrTime[index]
        }
        
        self.ddBreakTime.anchorView = viewBreakTime
        self.ddBreakTime.dataSource = arrBreakTime
        self.ddBreakTime.bottomOffset = CGPoint(x: 0, y:(self.ddBreakTime.anchorView?.plainView.bounds.height)!)
        self.ddBreakTime.topOffset = CGPoint(x: 0, y:-(self.ddBreakTime.anchorView?.plainView.bounds.height)!)
        ddBreakTime.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblBreakTime.text = self.arrBreakTime[index]
        }
        
    }
    //MARK: - Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDropDownAction(_ sender: UIButton) {
        
        
        if sender.tag ==  1{
            print("category")
//            self.lblCategory.textColor = .black
//            viewCategory.backgroundColor =  .white
          //  imgCategoryDown.image =  UIImage(named: "ic_down_black")
//            viewFirstName.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
         //   ddCategory.show()
            
        }else if sender.tag == 2{
            print("sub category")
            self.lblSubCategory.textColor = .black
            viewSubCategory.backgroundColor =  .white
            imgSubCategoryDown.image =  UIImage(named: "ic_down_black")
            ddSubCategory.show()
        }else if sender.tag == 3{
            print("time")
            self.lblTime.textColor = .black
            viewTime.backgroundColor =  .white
            imgTimeDown.image =  UIImage(named: "ic_down_black")
            ddTime.show()
        }else if sender.tag == 4{
            print("break Time")
            self.lblBreakTime.textColor = .black
            viewBreakTime.backgroundColor =  .white
            imgBreakTimeDown.image =  UIImage(named: "ic_down_black")
            ddBreakTime.show()
        }
        
        
    }
    @IBAction func btnDateAction(_ sender: Any) {
        openDatePicker()
    }
    
    @IBAction func btnAddAction(_ sender: Any) {
//        if !txtTime.isEmpty{
//            let mainStr = txtTime.text?.split(separator: ":")
//            let strHour = "\(mainStr![0])"
//            let strMinutes = "\(mainStr![1])"
//            print("Hours : \(strHour), Minutes : \(strMinutes)")
//        }
        //self.navigationController?.popViewController(animated: true)
        if checkValidation() {
            wsAddAvailability()
        }
    }
    func openDatePicker(){
        let dialog = CustomDatePickerDialog.showCustomDatePickerDialog(title: "Date and Time", titleLeftButton: "Cancel", titleRightButton: "Okay")
        dialog.setDatePickerForFutureTrip()
        let isCurrentDate = Calendar.current.isDateInToday(selectedDate)
        dialog.setDate(date: isCurrentDate ? Date():selectedDate)
        dialog.onClickLeftButton = {
            dialog.removeFromSuperview()
        }
        dialog.onClickRightButton = {(selectedDate) in
            dialog.removeFromSuperview()
            self.selectedDate = selectedDate
            self.lblDate.text = Utils.convertDatetoString(selectedDate, "dd-MM-yyyy HH:mm", toFormate: "dd-MM-yyyy")
            self.lblDate.textColor = .black
            self.txtTime.text = Utils.convertDatetoString(selectedDate, "dd-MM-yyyy HH:mm", toFormate: "hh:mm a")
        }
    }
    func setCategory(){
        arrCategoryString.removeAll()
        arrSubCategoryString.removeAll()
        for cat in arrCategory {
            arrCategoryString.append(cat.name!)
        }
        
        //Category
        self.ddCategory.anchorView = btnCategory
        self.ddCategory.dataSource = arrCategoryString
        self.ddCategory.bottomOffset = CGPoint(x: 0, y:(self.ddCategory.anchorView?.plainView.bounds.height)!)
        self.ddCategory.topOffset = CGPoint(x: 0, y:-(self.ddCategory.anchorView?.plainView.bounds.height)!)
        ddCategory.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblCategory.text = self.arrCategoryString[index]
            RegisterSingleton.shared.category_id = self.arrCategory[index]._id!
            self.wsGetCategorySubCategories(id: self.arrCategory[index]._id!)
        }
        
        
        
        
    }
    func setSubCategory(){
        arrSubCategoryString.removeAll()
        for sub in arrSubCategory {
            
            arrSubCategoryString.append(sub.name!)
        }
        //SubCategory
        self.ddSubCategory.anchorView = viewSubCategory
        self.ddSubCategory.dataSource = arrSubCategoryString
        self.ddSubCategory.bottomOffset = CGPoint(x: 0, y:(self.ddSubCategory.anchorView?.plainView.bounds.height)!)
        self.ddSubCategory.topOffset = CGPoint(x: 0, y:-(self.ddSubCategory.anchorView?.plainView.bounds.height)!)
        ddSubCategory.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblSubCategory.text = self.arrSubCategoryString[index]
            RegisterSingleton.shared.sub_category_ids = self.arrSubCategory[index]._id!
        }
    }
    
    
    //MARK:- API Calling
    func wsAddAvailability(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.category_id] = AppSingleton.shared.user?.category_id
        dictParam[PARAMS.sub_category_id] = RegisterSingleton.shared.sub_category_ids
        dictParam[PARAMS.availability_date] = Utils.converStringtoDate(lblDate.text!, "dd-MM-yyyy", toFormate: "yyyy-MM-dd")
        dictParam[PARAMS.availability_from_time] = Utils.converStringtoDate(txtTime.text!, "hh:mm a", toFormate: "HH:mm")
        if lblBreakTime.text?.lowercased() != "select break time" {
        dictParam[PARAMS.break_time] =  lblBreakTime.text?.replacingOccurrences(of: " min", with: "")
        }
        dictParam[PARAMS.total_availability_min] = lblTime.text?.replacingOccurrences(of: " min", with: "")
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.ADD_NEW_AVAILABILITY, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                self.passSelectedDate?(self.selectedDate)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    func wsGetCategorySubCategories(id:String=""){
        let dictParam:[String:Any] = id.isEmpty ? [:]:[PARAMS.category_id:id]
       
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_SP_CATEGORIES_SUB_CATEGORIES, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelCategories.self, from: data!)
                    
                    
                    if id.isEmpty {
                        self.arrCategory = responseModel.data?.categories ?? []
                        self.setCategory()
                    } else {
                        self.arrSubCategory = responseModel.data?.sub_categories ?? []
                        self.setSubCategory()
                    }
                    
                } catch {
                    
                }
               
            }
        }
    }
}

//MARK: - UITextFieldDelegate Method
extension AddAvailabilityVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let currentText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) else { return true }
         if textField == txtTime {
            if (currentText.utf16.count) <= 5{
                textField.setText(to: currentText.grouping(every: 2, with: ":"), preservingCursor: true)
                return false
            }
        }else {
            
        }
        return false
    
    }
    func checkValidation()->Bool {
         if lblCategory.text!.lowercased() == "select category" {
           Common().showAlert(strMsg: "Please select category.", view: self)
           return false
       } else if lblSubCategory.text!.lowercased() == "select sub category" {
           Common().showAlert(strMsg: "Please select sub category.", view: self)
           return false
       }
        
        else if lblDate.text == "DD-MM-YYYY" {
            Common().showAlert(strMsg: "Please select date.", view: self)
            return false
        }
       else if txtTime.isEmpty {
            Common().showAlert(strMsg: "Please enter time.", view: self)
            return false
        }
        else if lblTime.text == "XX min" {
            Common().showAlert(strMsg: "Please select time.", view: self)
            return false
        }
//        else if lblBreakTime.text?.lowercased() == "select break time" {
//            Common().showAlert(strMsg: "Please select break time.", view: self)
//            return false
//        }
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtTime {
            openDatePicker()
            return false
        }
        return true
    }
}
