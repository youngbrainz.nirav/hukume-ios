//
//  SPSearchVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/23/21.
//

import UIKit
import DropDown
import Cosmos
import MobileCoreServices


class SPSearchVC: UIViewController, UINavigationControllerDelegate {
    
    //MARK: - Outlets
    @IBOutlet weak var lblBusinessName : UILabel!
    @IBOutlet weak var lblCategory : UILabel!
    @IBOutlet weak var lblSubCategory : UILabel!
    @IBOutlet weak var imgCategoryDown : UIImageView!
    @IBOutlet weak var imgSubCategoryDown : UIImageView!
    
    @IBOutlet weak var txtBusinessName : UITextField!
    @IBOutlet weak var viewCategory : UIView!
    @IBOutlet weak var viewSubCategory : UIView!
    @IBOutlet weak var viewBusinessName : UIView!
    
    
    @IBOutlet weak var btnCategory : UIButton!
    @IBOutlet weak var btnSubCategory : UIButton!
    
    @IBOutlet weak var viewButton : UIView!
    @IBOutlet weak var tblCategory : UITableView!
    @IBOutlet weak var heightConstTblCategory : NSLayoutConstraint!

    @IBOutlet weak var lblFileName: UILabel!
    //MARK: - Varibles
    let ddCategory = DropDown()
    let ddSubCategory = DropDown()
    let ddLanguage = DropDown()
    
    var arrCategoryString:[String] = []
    var selectedIndexPath:IndexPath?
    var arrSubCategoryString:[String] = []
    var arrCategory:[Categories] = []
    var arrSubCategory:[Categories] = []
    var arrSelectedSubCategory = [IndexPath]()
    var arrLanguage = ["Gujarati","Hindi","English"]
    var imagePicker = UIImagePickerController()
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      //  tblCategory.tableFooterView = viewButton
        txtBusinessName.delegate = self
        self.lblBusinessName.isHidden =  true
        wsGetCategorySubCategories()
        tblCategory.estimatedRowHeight = 0
        tblCategory.rowHeight = UITableView.automaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.heightConstTblCategory.constant =  tblCategory.contentSize.height
    }
    
    
    
    //MARK: - Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickBtnAddCategory(_ sender: Any) {
        if checkValidation() {
        let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "AddProfessionalDetailsVC") as! AddProfessionalDetailsVC
            RegisterSingleton.shared.business_name = txtBusinessName.text!
            RegisterSingleton.shared.sub_category_ids = ""
            for sub in arrSelectedSubCategory {
                RegisterSingleton.shared.sub_category_ids = RegisterSingleton.shared.sub_category_ids.isEmpty ? (self.arrSubCategory[sub.row]._id ?? ""):(RegisterSingleton.shared.sub_category_ids+","+(self.arrSubCategory[sub.row]._id ?? ""))
            }
            print("Subcategories: ",RegisterSingleton.shared.sub_category_ids)
        self.pushToVC(vc)
        }
    }
    
    @IBAction func btnDropDownAction(_ sender: UIButton) {
        
        
        if sender.tag ==  1{
            print("category")
            self.lblCategory.textColor = .black
            viewCategory.backgroundColor =  .white
            imgCategoryDown.image =  UIImage(named: "ic_down_black")
//            viewFirstName.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            ddCategory.show()
            
        }else if sender.tag == 2{
            print("sub category")
            self.lblSubCategory.textColor = .black
            viewSubCategory.backgroundColor =  .white
            imgSubCategoryDown.image =  UIImage(named: "ic_down_black")
            ddSubCategory.show()
        }
        
    }
    @IBAction func btnSearchAction(_ sender: Any) {
        if checkValidation() {
        let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "AddProfessionalDetailsVC") as! AddProfessionalDetailsVC
            RegisterSingleton.shared.business_name = txtBusinessName.text!
        self.pushToVC(vc)
        }
    }
    @IBAction func btnClearAction(_ sender: Any) {
        
    }
    @IBAction func btnSliderAction(_ sender: UISlider) {
        
    }
    func checkValidation()->Bool{
       
//        if txtBusinessName.isEmpty{
//
//            Common().showAlert(strMsg: "Please enter business name.", view: self)
//            return false
//        } else
        if lblCategory.text!.lowercased() == "select category" {
            Common().showAlert(strMsg: "Please select category.", view: self)
            return false
        } else if arrSelectedSubCategory.isEmpty {
            Common().showAlert(strMsg: "Please select sub category.", view: self)
            return false
        }
//        else if lblFileName.text?.lowercased() == "Attach Document".lowercased() {
//                Common().showAlert(strMsg: "Please attach degree certificate.", view: self)
//                return false
//            
//        }
        
        return true
        
    }
    @IBAction func btnAttachDocument(_ sender: Any) {
        alertForDocument()
    }
    func setCategory(){
        arrCategoryString.removeAll()
        arrSubCategoryString.removeAll()
        arrSelectedSubCategory.removeAll()
        for cat in arrCategory {
            arrCategoryString.append(cat.name!)
        }
        
        //Category
        self.ddCategory.anchorView = btnCategory
        self.ddCategory.dataSource = arrCategoryString
        self.ddCategory.bottomOffset = CGPoint(x: 0, y:(self.ddCategory.anchorView?.plainView.bounds.height)!)
        self.ddCategory.topOffset = CGPoint(x: 0, y:-(self.ddCategory.anchorView?.plainView.bounds.height)!)
        ddCategory.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lblCategory.text = self.arrCategoryString[index]
            RegisterSingleton.shared.category_id = self.arrCategory[index]._id!
            self.wsGetCategorySubCategories(id: self.arrCategory[index]._id!)
        }
        
        
        
        
    }
    func setSubCategory(){
        arrSubCategoryString.removeAll()
        for sub in arrSubCategory {
            
            arrSubCategoryString.append(sub.name!)
        }
        //SubCategory
//        self.ddSubCategory.anchorView = viewSubCategory
//        self.ddSubCategory.dataSource = arrSubCategoryString
//        self.ddSubCategory.bottomOffset = CGPoint(x: 0, y:(self.ddSubCategory.anchorView?.plainView.bounds.height)!)
//        self.ddSubCategory.topOffset = CGPoint(x: 0, y:-(self.ddSubCategory.anchorView?.plainView.bounds.height)!)
//        ddSubCategory.selectionAction = { [unowned self] (index: Int, item: String) in
//            print("Selected item: \(item) at index: \(index)")
//            self.lblSubCategory.text = self.arrSubCategoryString[index]
//            RegisterSingleton.shared.sub_category_ids = self.arrSubCategory[index]._id!
//        }
        tblCategory.reloadData()
        
        heightConstTblCategory.constant = tblCategory.contentSize.height
//        DispatchQueue.main.async {
//           // self.heightConstTblCategory.constant = self.tblCategory.contentSize.height
//        }
    }
    
    func wsGetCategorySubCategories(id:String=""){
        let dictParam:[String:Any] = id.isEmpty ? [:]:[PARAMS.category_id:id]
       
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_SP_CATEGORIES_SUB_CATEGORIES, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelCategories.self, from: data!)
                    
                    
                    if id.isEmpty {
                        self.arrCategory = responseModel.data?.categories ?? []
                        self.setCategory()
                    } else {
                        self.arrSubCategory = responseModel.data?.sub_categories ?? []
                        self.setSubCategory()
                    }
                    
                } catch {
                    
                }
               
            }
        }
    }

}
//MARK: - TableView Delegate Method
extension SPSearchVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSubCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CategoryTVCell
        cell.lblSubcategory.text = arrSubCategory[indexPath.row].name
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! CategoryTVCell
        cell.btnCheck.isSelected = !cell.btnCheck.isSelected
        if self.arrSelectedSubCategory.contains(indexPath){
            self.arrSelectedSubCategory = self.arrSelectedSubCategory.filter{$0 != indexPath}
        }else{
            self.arrSelectedSubCategory.append(indexPath)
            
        }
        
    }
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 60
//    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
//MARK: - TextField Delegate Methods
extension SPSearchVC : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtBusinessName{
            self.lblBusinessName.isHidden =  false
            viewBusinessName.backgroundColor =  .white
            self.txtBusinessName.placeholder = ""
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtBusinessName{
            if txtBusinessName.text ==  ""{
                self.lblBusinessName.isHidden =  true
                self.txtBusinessName.placeholder = "Business Name"
                viewBusinessName.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblBusinessName.isHidden =  false
                viewBusinessName.backgroundColor =  .white

            }
            
        }
    }
    
}
extension SPSearchVC:UIDocumentPickerDelegate,UIImagePickerControllerDelegate {
    func alertForDocument(){
        let alert = UIAlertController(title: "Choose Document", message: nil, preferredStyle: .actionSheet)
//        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
//            self.openCamera()
//        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        alert.addAction(UIAlertAction(title: "Document", style: .default, handler: { _ in
            self.openDocumnetPicker()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera()
        {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                    imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    imagePicker.allowsEditing = false
                DispatchQueue.main.async {
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
                }else
            {
                let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }

        func openGallary()
        {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                    self.present(imagePicker, animated: true, completion: nil)
                }
        }
    func openDocumnetPicker(){
        let documentPicker = UIDocumentPickerViewController.init(documentTypes: [String.init(kUTTypePDF), String.init(kUTTypePNG), String.init(kUTTypeJPEG),"com.microsoft.word.doc", "org.openxmlformats.wordprocessingml.document"], in: .import)
        documentPicker.delegate = self
        documentPicker.allowsMultipleSelection = false
        
        self.present(documentPicker, animated: true, completion: nil)
    }
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print(urls.first)
        let fileSize = urls.first?.fileSize()
        print(fileSize)
        if fileSize! > 5 {
            Common().showAlert(strMsg: "File size must be 5 MB or less.", view: self)
            return
        }
        lblFileName.text = urls.first?.lastPathComponent
        if urls.first?.lastPathComponent.hasSuffix(".pdf") ?? false {
            RegisterSingleton.shared.isPdf = true
        }
        lblFileName.layoutIfNeeded()
        
        RegisterSingleton.shared.degree_certificate = lblFileName.text!
        
        RegisterSingleton.shared.documentData = try! Data.init(contentsOf: urls.first!)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }
        let imageSize = image.getSizeIn(.megabyte)
        print(imageSize)
        if imageSize > 5 {
            Common().showAlert(strMsg: "File size must be 5 MB or less.", view: self)
            return
        }
        if let imageUrl = info[.imageURL] as? URL {
            lblFileName.text = imageUrl.lastPathComponent
            lblFileName.layoutIfNeeded()
            
            
                }
        RegisterSingleton.shared.degree_certificate = lblFileName.text!
        RegisterSingleton.shared.documentData = image.jpegData(compressionQuality: 0.5)
        RegisterSingleton.shared.isPdf = false
        // print out the image size as a test
        print(image.size)
    }
}
