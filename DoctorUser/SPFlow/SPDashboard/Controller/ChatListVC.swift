//
//  ChatListVC.swift
//  Hukumi
//
//  Created by hiren  mistry on 03/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit

class ChatListVC: UIViewController {

    @IBOutlet weak var tblChat: UITableView!
    
    @IBOutlet weak var lblNoChat: UILabel!
    
     var arrChatList : [Last_chat_list] = []
   
    var timerCallChatApi = Timer()

    var isFirstTimeAPiCall = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        wsGetChatList()

        isFirstTimeAPiCall = true
        //self.startApiCounter()
        
       // self.CallCheckSubscription()
        
    }
//
//
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
      //  self.stopApiCounter()
    }
    
    func initSetup() {
        
        tblChat.dataSource = self
        tblChat.delegate = self
        tblChat.reloadData()
        
        
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - API Calling
    func wsGetChatList(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.user_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.timezone] = TimeZone.current.identifier
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_CONVERSATION_LIST, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam){ response, data, error in
            self.arrChatList.removeAll()
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelChatList.self, from: data!)
                   // self.arrChatList = responseModel.data ?? []
                    for chat in responseModel.data! {
                        if !chat.lastMsg!.isEmpty {
                            self.arrChatList.append(chat)
                        }
                    }
                }
                catch {
                    
                }
            }
            self.tblChat.reloadData()
            self.lblNoChat.isHidden = !self.arrChatList.isEmpty
        }
    }
    
}


extension ChatListVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        arrChatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let aCell = tableView.dequeueReusableCell(withIdentifier: "ChatlistCell") as! ChatlistCell
//

        aCell.setData(data: arrChatList[indexPath.row])
        
        return aCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}

extension ChatListVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//      
        let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        vc.conversationId = arrChatList[indexPath.row].conversation_id ?? ""
        vc.recipient_id = arrChatList[indexPath.row].user_id ?? ""
        vc.receiver_name = arrChatList[indexPath.row].sender_name ?? ""
        self.pushToVC(vc)
       // }
    }
}

class ChatListModel {
    
    var userName = ""
    var lastMessage = ""
    var messageCount = 0
    var imgUser: UIImage?
    
    init(dic: [String: Any]) {
        
        imgUser =  UIImage(named: dic["imgUser"] as? String ?? "")
        userName =  dic["userName"] as? String ?? ""
        lastMessage =  dic["lastMessage"] as? String ?? ""
        messageCount =  dic["messageCount"] as? Int ?? 0
    }
}


class ChatlistCell: UITableViewCell {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var userOnlineStatus: UIImageView!
    
    @IBOutlet weak var lblMassageCount: UILabel!
    @IBOutlet weak var lblLastMessage: UILabel!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    
    
    override func awakeFromNib() {
        lblMassageCount.backgroundColor = .themeColor
    }
    func setData(data:Last_chat_list) {
        lblUserName.text = data.sender_name
        lblLastMessage.text = data.lastMsg
        lblMassageCount.text = data.unread_count ?? "0"
        lblDate.text = Utils.converStringtoDate(data.lastConversationDate!, "yyyy-MM-dd HH:mm:ss", toFormate: "dd-MM-yyyy hh:mm a",isToday: true)
        lblMassageCount.isHidden = data.unread_count == "0"
        imgUser.downloadedFrom(link: data.sender_profile_image ?? "")
    }
}
