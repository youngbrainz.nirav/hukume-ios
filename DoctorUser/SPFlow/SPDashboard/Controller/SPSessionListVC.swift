//
//  SPSessionListVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/24/21.
//

import UIKit

class SPSessionListVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tblView : UITableView!
    @IBOutlet weak var viewActive : UIView!
    @IBOutlet weak var viewPast : UIView!
    @IBOutlet weak var lblEmpty: UILabel!
    
    
    //MARK:- Varibles
    var isPastSession = false
    var arrSessionList:[DataSessionList] = []
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.estimatedRowHeight = 130
        tblView.rowHeight = UITableView.automaticDimension
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setData()
        wsGetSessionList()
    }
    
    //MARK:- Set Data
    func setData(){
        if self.isPastSession{
            self.viewPast.backgroundColor = .white
            self.viewPast.borderColor = UIColor(red: 22 / 255, green: 100 / 255, blue: 252 / 255 , alpha: 1.0)
            self.viewPast.borderWidth = 1
            
            self.viewActive.backgroundColor = UIColor(red: 242 / 255, green: 242 / 255, blue: 242 / 255 , alpha: 1.0)
            self.viewActive.borderColor = .clear
            self.viewActive.borderWidth = 0
        }else{
            self.viewPast.backgroundColor = UIColor(red: 242 / 255, green: 242 / 255, blue: 242 / 255 , alpha: 1.0)
            self.viewPast.borderColor = .clear
            self.viewPast.borderWidth = 1
            
            self.viewActive.backgroundColor = .white
            self.viewActive.borderColor = UIColor(red: 22 / 255, green: 100 / 255, blue: 252 / 255 , alpha: 1.0)
            self.viewActive.borderWidth = 1
        }
        
    }

    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
       
            self.navigationController?.popViewController(animated: true)
       
    }
    
    @IBAction func btnMenuSelectionAction(_ sender: UIButton) {
        if sender.tag == 1{
            //active
            isPastSession = false
        }else if sender.tag == 2{
            // passt
            isPastSession = true
        }
        setData()
        wsGetSessionList()
        
    }

    //MARK: - API Calling
    func wsGetSessionList(){
        self.tblView.isHidden = true
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.session_status] = isPastSession ? "2" : "1"
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_USER_SESSION_LIST, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            self.arrSessionList.removeAll()
            if Parser.isSuccess(response: response,isErrorToast: false) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelSessionList.self, from: data!)
                    self.arrSessionList = responseModel.data ?? []
                    
                }
                catch {
                    
                }
            }
            self.tblView.isHidden = self.arrSessionList.isEmpty
            self.lblEmpty.isHidden = !self.arrSessionList.isEmpty
            self.lblEmpty.text = response["msg"] as? String ?? "No session available."
            DispatchQueue.main.async {
                self.tblView.reloadData()
            }
            
        }
    }
    
    
}
//MARK:- TableView Delegate Method
extension SPSessionListVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSessionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SPTVCell
        if isPastSession{
            cell.lblRating.isHidden = false
            cell.imgRating.isHidden = false
            if arrSessionList[indexPath.row].status == "4" {
            cell.lblAvailable.text = "Completed"
            cell.viewColor.backgroundColor = UIColor(red: 254 / 255, green: 201 / 255, blue: 58 / 255 , alpha: 1.0)
        
            } else {
                cell.lblAvailable.text = "Cancelled"
                cell.viewColor.backgroundColor = .systemRed
                cell.lblRating.isHidden = true
                cell.imgRating.isHidden = true
            }
        }else{
            if arrSessionList[indexPath.row].status == "2" {
            cell.lblRating.isHidden = true
            cell.imgRating.isHidden = true
            cell.lblAvailable.text = "Active"
            cell.viewColor.backgroundColor = UIColor(red: 92 / 255, green: 222 / 255, blue: 131 / 255 , alpha: 1.0)
            } else {
                cell.lblRating.isHidden = true
                cell.imgRating.isHidden = true
                cell.lblAvailable.text = "Upcoming"
                cell.viewColor.backgroundColor = UIColor(red: 92 / 255, green: 222 / 255, blue: 131 / 255 , alpha: 1.0)
               
            }
        }
        cell.setData(data: arrSessionList[indexPath.row])
        cell.layoutIfNeeded()
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "SPSessionDetailsVC") as! SPSessionDetailsVC
        vc.isPastSession = self.isPastSession
        vc.objSession = arrSessionList[indexPath.row]
        self.pushToVC(vc)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}

