//
//  AddProfessionalDetailsVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/27/21.
//

import UIKit

class AddProfessionalDetailsVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var txtDescription : UITextView!
    @IBOutlet weak var txtExperience : UITextField!
    
    //MARK:- Varibles
    var strPlaceHolder = "Write some words about you"
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtDescription.delegate = self
        txtDescription.text = strPlaceHolder
        txtDescription.textColor = .lightGray
        txtExperience.font = FontHelper.Font(size: 17, style: .SemiBold)
        txtDescription.font = FontHelper.Font(size: 17, style: .SemiBold)
    }
    
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
           
           
        if checkValidation() {
            setSingleton()
        //let vc = UIStoryboard(name: STORYBOARDNAME.Authentication, bundle: nil).instantiateViewController(withIdentifier: "VerificationVC") as! VerificationVC
      //  self.pushToVC(vc)
            wsRegisterSP()
        }
    }
    func checkValidation()->Bool{
        if txtExperience.isEmpty {
            Common().showAlert(strMsg: "Please enter years of experience.", view: self)
           return false
        }
        else if txtDescription.text == strPlaceHolder {
            Common().showAlert(strMsg: "Please enter some words about you.", view: self)
            return false
        }
        return true
    }
    
    // MARK: - API Calling
    func wsRegisterSP(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.device_type] = "1"
        dictParam[PARAMS.device_token] = preferenceHelper.getDeviceToken()
        dictParam[PARAMS.email_id] = RegisterSingleton.shared.email_id
        dictParam[PARAMS.password] = RegisterSingleton.shared.password
        dictParam[PARAMS.mobile_no] = RegisterSingleton.shared.mobile_no
        dictParam[PARAMS.country_code] = RegisterSingleton.shared.country_code
        dictParam[PARAMS.first_name] = RegisterSingleton.shared.first_name
        dictParam[PARAMS.last_name] = RegisterSingleton.shared.last_name
        dictParam[PARAMS.address] = RegisterSingleton.shared.address
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.category_id] = RegisterSingleton.shared.category_id
        dictParam[PARAMS.sub_category_ids] = RegisterSingleton.shared.sub_category_ids
        dictParam[PARAMS.year_of_experience] = RegisterSingleton.shared.year_of_experience
        dictParam[PARAMS.about_you] = RegisterSingleton.shared.about_you
        dictParam[PARAMS.latitude] = RegisterSingleton.shared.latitude
        dictParam[PARAMS.longitude] = RegisterSingleton.shared.longitude
        dictParam[PARAMS.country_code_info] = RegisterSingleton.shared.country_code_info
      //  dictParam[PARAMS.business_name] = RegisterSingleton.shared.business_name
        let alamofire = AlamofireHelper.init()
        if RegisterSingleton.shared.documentData != nil {
        alamofire.getResponseFromURL(url: WebService.REGISTER_USER, paramData: dictParam, pdf: RegisterSingleton.shared.documentData!, isPdf: RegisterSingleton.shared.isPdf) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelLogin.self, from: data!)
                    preferenceHelper.setUserId(responseModel.data?._id ?? "")
                    preferenceHelper.setSessionToken(responseModel.data?.session_token ?? "")
                    AppSingleton.shared.user = responseModel.data
                    preferenceHelper.setUserName((AppSingleton.shared.user?.first_name ?? "") + " " + (AppSingleton.shared.user?.last_name ?? ""))
                    preferenceHelper.setUserPic(AppSingleton.shared.user?.profile_image ?? "")
                    preferenceHelper.setIsSubscription(false)
                    appDel.gotoHome()
                }
                catch {
                    print(WebService.REGISTER_USER + " error")
                }

            }
        }
        } else {
            alamofire.getResponseFromURL(url: WebService.REGISTER_USER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
                if Parser.isSuccess(response: response) {
                    let jsonDecoder = JSONDecoder()
                    do {
                        let responseModel = try jsonDecoder.decode(ModelLogin.self, from: data!)
                        preferenceHelper.setUserId(responseModel.data?._id ?? "")
                        preferenceHelper.setSessionToken(responseModel.data?.session_token ?? "")
                        AppSingleton.shared.user = responseModel.data
                        preferenceHelper.setUserName((AppSingleton.shared.user?.first_name ?? "") + " " + (AppSingleton.shared.user?.last_name ?? ""))
                        preferenceHelper.setUserPic(AppSingleton.shared.user?.profile_image ?? "")
                        preferenceHelper.setIsSubscription(false)
                        appDel.gotoHome()
                    }
                    catch {
                        print(WebService.REGISTER_USER + " error")
                    }

                }
            }
        }
//
       
    }
}
//MARK:- Textview Delegate Methods
extension AddProfessionalDetailsVC : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == strPlaceHolder {
            txtDescription.textColor = .black
            textView.text = nil
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = strPlaceHolder
            txtDescription.textColor = .lightGray
        }
    }
    func setSingleton(){
        RegisterSingleton.shared.year_of_experience = txtExperience.text!
        RegisterSingleton.shared.about_you = txtDescription.text!
    }
    
}
