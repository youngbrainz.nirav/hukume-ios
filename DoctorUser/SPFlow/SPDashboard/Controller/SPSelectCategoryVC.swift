//
//  SPSelectCategoryVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/27/21.
//

import UIKit

class SPSelectCategoryVC: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var cvCategory : UICollectionView!
    @IBOutlet weak var heightConstcvCategory : NSLayoutConstraint!
    @IBOutlet weak var tblPrice : UITableView!
    @IBOutlet weak var heightConstTblPrice : NSLayoutConstraint!
    
    //MARK: - Varibles
    var arrCategory = ["Category 1","Category 2","Category 3","Category 4","Category 5","Category 1","Category 2","Category 3","Category 4","Category 5","Category 1","Category 2","Category 3","Category 4","Category 5"]
    var arrSubCategory = ["Sub Category 1","Sub Category 2","Sub Category 3","Sub Category 4","Sub Category 5","Sub Category 1","Sub Category 2","Sub Category 3","Sub Category 4","Sub Category 5","Sub Category 1","Sub Category 2","Sub Category 3","Sub Category 4","Sub Category 5"]
    var arrSelectedCategory = [IndexPath]()
    var arrSelectedSubCategory = [IndexPath]()
    var selectedPriceIndex =  -1
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        let  height = cvCategory.collectionViewLayout.collectionViewContentSize.height
        heightConstcvCategory.constant = height
        
        
        heightConstTblPrice.constant = tblPrice.contentSize.height
        self.view.setNeedsLayout() //Or self.view.layoutIfNeeded()
    }
    
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
//        let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "AddIssueVC") as! AddIssueVC
//        self.pushToVC(vc)
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK:- CollectionView Method
extension SPSelectCategoryVC : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return arrCategory.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCVCell
    
            let item = arrCategory[indexPath.row]
            cell.lblName.text =  item
            if arrSelectedCategory.contains(indexPath){
                cell.viewMain.backgroundColor =  UIColor(red: 22 / 255, green: 100 / 255, blue: 252 / 255 , alpha: 1.0)
                cell.lblName.textColor = .white
            }else{
                cell.viewMain.backgroundColor =  .white
                cell.lblName.textColor = .black
            }
       
        cell.viewMain.borderWidth = 1.0
        cell.viewMain.borderColor = .lightGray
        cell.viewMain.cornerRadius = 5
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//            self.arrSelectedCategory = self.arrSelectedCategory.filter($0.indexPath)
            if self.arrSelectedCategory.contains(indexPath){
                self.arrSelectedCategory = self.arrSelectedCategory.filter{$0 != indexPath}
                
            }else{
                self.arrSelectedCategory.append(indexPath)
            }
            cvCategory.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 3 - 10 , height: 100)
    }
}

//MARK: - TableView Delegate Method
extension SPSelectCategoryVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CardTVCell
        if indexPath.row == selectedPriceIndex{
            cell.btnCheck.setImage(UIImage(named: "ic_checkmark_color"), for: .normal)
        }else{
            cell.btnCheck.setImage(UIImage(named: "ic_uncheckmark"), for: .normal)
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedPriceIndex =  indexPath.row
        tblPrice.reloadData()
    }
    
}

