//
//  CalendarVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/27/21.
//

import UIKit
import VACalendar
import FSCalendar
import DropDown





class AvailabilitiesVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var vw_Cal : UIView!
    @IBOutlet weak var tblView : UITableView!
    @IBOutlet weak var heightConstTblView : NSLayoutConstraint!
    
    @IBOutlet weak var lblEmpty: UILabel!
    
    //MARK: - Varibles
    var selectedDateCalender = Date()
    fileprivate let gregorian: Calendar = Calendar(identifier: .gregorian)
    fileprivate lazy var dateFormatter1: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    var calendarView: VACalendarView!
    
    var allDate:[Date] = []
    var month = ""
    var yr = ""
    let borderDefaultColors = ["2020/06/15": UIColor.green]
    var arrAvailability:[DataAvailability] = []
    var strDate = Utils.convertDatetoString(Date(), "dd-MM-yyyy", toFormate: "yyyy-MM-dd")
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let calendar = VACalendar(calendar: defaultCalendar)
        calendarView = VACalendarView(frame: .zero, calendar: calendar)
        calendarView.showDaysOut = true
        calendarView.selectionStyle = .single
        calendarView.monthDelegate = monthHeaderView
        calendarView.dayViewAppearanceDelegate = self
        calendarView.monthViewAppearanceDelegate = self
        calendarView.calendarDelegate = self
        calendarView.scrollDirection = .horizontal
        calendarView.startDate = Date()
        tblView.estimatedRowHeight = 150
        tblView.rowHeight = UITableView.automaticDimension
        let todayItem = UIBarButtonItem(title: "TODAY", style: .plain, target: self, action: #selector(self.todayItemClicked(sender:)))
             vw_Cal.addSubview(calendarView)
        print(todayItem,"TOday item")
        wsGetAvailability()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tblView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        calendarView.startDate = Date()
       // calendarView.selectDates([selectedDateCalender])
       // calendarView.setup()
        
        
        let endFormatter = DateFormatter()
        endFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dtTime = endFormatter.string(from: Date())
        let dt = Utils.converStringtoDate1(dtTime, "yyyy-MM-dd HH:mm:ss", toFormate: "dd-MM-yyyy")
       let displayDT = Utils.converStringtoDate1(dt, "dd-MM-yyyy", toFormate: "dd MMMM yyyy")
       
       let splitDt = dt.split(separator:"-")
         month = String(splitDt[1])
         yr = String(splitDt[2])
        
       
              
        
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if(keyPath == "contentSize"){

                if let newvalue = change?[.newKey]{
                    let newsize  = newvalue as! CGSize
                    if self.heightConstTblView.constant != newsize.height {
                        if arrAvailability.isEmpty {
                            self.heightConstTblView.constant = 100
                            return
                        }
                    self.heightConstTblView.constant = newsize.height
                    }
                }
            }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
      
        if calendarView.frame == .zero {
            calendarView.frame = CGRect(
                x: 0,
                y: 100,
                width: view.frame.width,
                height: 350
                //height: view.frame.height * 0.6
            )
            calendarView.setup()
            
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tblView.removeObserver(self, forKeyPath: "contentSize")
    }
    @IBOutlet weak var monthHeaderView: VAMonthHeaderView! {
        didSet {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "LLLL yyyy"
            
            let appereance = VAMonthHeaderViewAppearance(
                previousButtonImage: #imageLiteral(resourceName: "ic_back"),
                nextButtonImage: #imageLiteral(resourceName: "ic_Back_Right"),
                dateFormatter: dateFormatter
            )
            monthHeaderView.delegate = self
            monthHeaderView.appearance = appereance
            monthHeaderView.backgroundColor = .white
            monthHeaderView.tintColor = UIColor.white
            
        }
    }
    
    @IBOutlet weak var weekDaysView: VAWeekDaysView! {
        didSet {
            let appereance = VAWeekDaysViewAppearance(symbolsType: .veryShort, calendar: defaultCalendar)
            weekDaysView.appearance = appereance
//            weekDaysView.appearance.weekDayTextColor = .white
        }
    }
    
    let defaultCalendar: Calendar = {
        var calendar = Calendar.current
        calendar.firstWeekday = 1
        calendar.timeZone = TimeZone.current//TimeZone(identifier: kCurrentUser.timezone_title!)!
        return calendar
    }()
    
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderDefaultColorFor date: Date) -> UIColor? {
        let key = self.dateFormatter1.string(from: date)
        if let color = self.borderDefaultColors[key] {
            return color
        }
        return appearance.borderDefaultColor
    }
    
    
    //MARK: - Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "AddAvailabilityVC") as! AddAvailabilityVC
        vc.strDate = strDate
        vc.selectedDate = selectedDateCalender
        vc.passSelectedDate = { date in
            self.selectedDateCalender = date
            self.strDate = Utils.convertDatetoString(date, "dd-MM-yyyy HH:mm", toFormate: "yyyy-MM-dd")
            self.selectedDate(date)
        }
        self.pushToVC(vc)
    }
    @objc
    func todayItemClicked(sender: AnyObject) {
        // self.vw_Cal.setCurrentPage(Date(), animated: false)
    }
    @IBAction func btnEditAvailabilityAction(_ sender: UIButton) {
        let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "EditAvailabilityVC") as! EditAvailabilityVC
        vc.objAvailability = arrAvailability[sender.tag]
        vc.selectedDate = selectedDateCalender
        vc.passSelectedDate = { date in
            self.selectedDateCalender = date
            self.strDate = Utils.convertDatetoString(date, "dd-MM-yyyy HH:mm", toFormate: "yyyy-MM-dd")
            self.selectedDate(date)
        }
        self.pushToVC(vc)
    }
    @objc func didTapDelete(sender:UIButton) {
        wsRemoveAvailability(id: arrAvailability[sender.tag]._id ?? "")
    }
    //MARK:- API Calling
    func wsGetAvailability(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.availability_date] = strDate
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_AVAILABILITY_LIST, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            self.arrAvailability.removeAll()
            
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelAvailability.self, from: data!)
                    self.arrAvailability = responseModel.data ?? []
                    self.tblView.reloadData()
                    self.allDate.removeAll()
                    if responseModel.available_dates == nil {return}
                    for date in responseModel.available_dates! {
                        if let d = date.toDate(withFormat: "yyyy-MM-dd") {
                        self.allDate.append(d)
                        }
                    }
                    if self.allDate.count > 0 {
                               for i in 0..<self.allDate.count {
                                   let dateObje = self.allDate[i]
                                   self.calendarView.setSupplementaries([(dateObje, [VADaySupplementary.bottomDots([UIColor.black])])])
                              }
                           }
                    
                   
                    
                    
                }
                catch {
                    
                }
            if Parser.isSuccess(response: response,isErrorToast: false) {self.lblEmpty.isHidden = true
                
            } else {
                self.calendarView.setSupplementaries([(self.selectedDateCalender, [VADaySupplementary.bottomDots([UIColor.white])])])
                
                self.lblEmpty.text = response["msg"] as? String
                self.lblEmpty.isHidden = false
            }
           
        }
    }
    func wsRemoveAvailability(id:String){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.availability_id] = id
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.REMOVE_AVAILABILITY, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                self.wsGetAvailability()
            }
        }
    }

    
}


//MARK: - TableView Delegate Method
extension AvailabilitiesVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAvailability.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CardTVCell
        
        let strName =  "\(arrAvailability[indexPath.row].break_time ?? "0") min"
        let strTitle = "Break Time :"
        
        let strNew = "\(strTitle) \(strName)"
        let attributedWithTextColor: NSAttributedString = strNew.attributedStringWithColor([strName], color: UIColor(red: 93 / 255, green: 220 / 255, blue: 93 / 255, alpha: 1.0))
        //cell.lblTime.attributedText =  attributedWithTextColor
        cell.lblName.text = (arrAvailability[indexPath.row].availability_from_time ?? "") + " to " + (arrAvailability[indexPath.row].availability_to_time ?? "")
        cell.lblMonth.text = (arrAvailability[indexPath.row].total_availability_min ?? "")
        cell.btnCheck.tag =  indexPath.row
         cell.btnCheck.addTarget(self, action: #selector(btnEditAvailabilityAction(_:)), for: .touchUpInside)
        cell.btnDeleteCard.setImage(UIImage.init(named: "ic_delete")?.sd_tintedImage(with: .darkGray), for: .normal)
        cell.btnDeleteCard.tag =  indexPath.row
         cell.btnDeleteCard.addTarget(self, action: #selector(didTapDelete(sender:)), for: .touchUpInside)
        cell.layoutIfNeeded()
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
}


extension AvailabilitiesVC: VAMonthHeaderViewDelegate {
    
    func didTapNextMonth() {
        calendarView.nextMonth()
        calendarView.monthDelegate = monthHeaderView
        
        var mn = Int(month)!
               if mn == 12 {
                   mn = 1
                let y = Int(yr)! + 1
                   yr = String(y)
               }else{
                   mn = mn + 1
               }
               
               month = String(mn)
               
               
    }
    
    func didTapPreviousMonth() {
        calendarView.previousMonth()
        calendarView.monthDelegate = monthHeaderView
        
        var mn = Int(month)!
        if mn == 1 {
            mn = 12
            let y = Int(yr)! - 1
            yr = String(y)
        }else{
            mn = mn - 1
        }
        month = String(mn)
        
       
        
    
    }
    
}

extension AvailabilitiesVC: VAMonthViewAppearanceDelegate {
    
    func leftInset() -> CGFloat {
        return 10.0
    }
    
    func rightInset() -> CGFloat {
        return 10.0
    }
    
    func verticalMonthTitleFont() -> UIFont {
        return UIFont.systemFont(ofSize: 16, weight: .semibold)
    }
    
    func verticalMonthTitleColor() -> UIColor {
        return .black
    }
    
    func verticalCurrentMonthTitleColor() -> UIColor {
        return .red
    }
    
}

extension AvailabilitiesVC: VADayViewAppearanceDelegate {
    
    func textColor(for state: VADayState) -> UIColor {
        switch state {
        case .out:
            return UIColor(red: 214 / 255, green: 214 / 255, blue: 219 / 255, alpha: 1.0)
        case .selected:
            return .white
        case .unavailable:
            return .lightGray
//        case .currentDate:
//            return .red
        default:
            return .black
        }
    }
    
    func textBackgroundColor(for state: VADayState) -> UIColor {
        switch state {
        case .selected:
            return .blue
//        case .currentDate :
//            return .green
        default:
            return .clear
        }
    }
    
    func shape() -> VADayShape {
        return .circle
    }
    
    func dotBottomVerticalOffset(for state: VADayState) -> CGFloat {
        switch state {
        case .selected:
            return 2
        default:
            return -7
        }
    }
    
}

extension AvailabilitiesVC: VACalendarViewDelegate {
    
    func selectedDate(_ date: Date) {
        print("hello123456789")
        let endFormatter = DateFormatter()
        endFormatter.dateFormat = "yyyy-MM-dd"
        let dtTime = endFormatter.string(from: date)
        selectedDateCalender = date
        strDate = dtTime
        wsGetAvailability()
        calendarView.selectDates([date])
//        tblAvaibility.reloadData()

    }
    
    func selectedDates(_ dates: [Date]) {
        calendarView.startDate = dates.last ?? Date()
        print(dates)
        print(calendarView.startDate)
        
    }
}
