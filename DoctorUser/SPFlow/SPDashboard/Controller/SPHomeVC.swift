//
//  SPHomeVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/21/21.
//

import UIKit
import BSStackView
import AVFoundation

class SPHomeVC: UIViewController {
    
    //MARK: - Outlet
    @IBOutlet var cvBanner: UICollectionView!
    @IBOutlet var cvChats: UICollectionView!
    @IBOutlet var viewApprove: UIView!
    @IBOutlet weak var tblRating : UITableView!
    @IBOutlet weak var heightConstTblRating : NSLayoutConstraint!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var viewChat: UIView!
    @IBOutlet weak var viewRatingReview: UIView!
    @IBOutlet weak var lblApprove: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    //MARK: - Variable
//    var viewOfHeader = CustomHeaderView.instanceFromNib()
    var views =  [UIView]()
    var views2 =  [UIView]()
    var arrImgs = 3
    var arrAdvertisement:[Advertisement] = []
    var arrReviews:[Reviews] = []
    var arrChats:[Last_chat_list] = []
    var avplayer:AVPlayer?
    var videoIndex = 0
    //12B9AE
    //18 185 174
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        lblCount.isHidden = true
       // wsGetProfileData()
        viewApprove.isHidden =  true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblUserName.text = preferenceHelper.getUserName()
        imgUser.downloadedFrom(link: preferenceHelper.getUserPic())
        wsGetProfileData()
        wsGetHomeDashboard()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imgUser.applyCornerRadiusWith(radius: 10)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        avplayer?.pause()
    }
    //MARK:- Button Action
    @IBAction func btnProfileAction(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.SPProfile, bundle: nil).instantiateViewController(withIdentifier: "SPMenuVC") as! SPMenuVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @IBAction func btnNotificationAction(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.SPProfile, bundle: nil).instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @IBAction func btnSearchAction(_ sender: Any) {
//        let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "ServiceProviderListVC") as! ServiceProviderListVC
//        self.navigationController?.pushViewController(vc, animated: true)

    }
    @IBAction func btnOkAction(_ sender: Any) {
    }
    
    @IBAction func btnViewAllAction(_ sender: UIButton) {
        print("View All")
        if sender.tag == 0 {
            let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "ChatListVC") as! ChatListVC
            self.pushToVC(vc)
        } else {
        let vc = UIStoryboard(name: STORYBOARDNAME.SPProfile, bundle: nil).instantiateViewController(withIdentifier: "SPRatingListVC") as! SPRatingListVC
        self.pushToVC(vc)
    }
    }
    
    
    func translate(_ direction: BSStackViewItemDirection) -> String? {
        switch direction {
        case BSStackViewItemDirection.front:
            return "front"
        case BSStackViewItemDirection.back:
            return "back"
        default:
            break
        }
        return ""
    }
    func setHomeData() {
        DispatchQueue.main.async {
            self.tblRating.reloadData()
            
            self.tblRating.layoutIfNeeded()
            self.heightConstTblRating.constant = self.tblRating.contentSize.height
        }
        self.cvBanner.reloadData()
        self.cvChats.reloadData()
        self.cvBanner.layoutIfNeeded()
        self.cvChats.layoutIfNeeded()
        viewChat.isHidden = arrChats.isEmpty
        viewRatingReview.isHidden = arrReviews.isEmpty
        
    }
    //MARK: - API Calling
    func wsGetHomeDashboard(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.SP_HOME_DASHBOARD, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response) {
                self.arrChats.removeAll()
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelSPHome.self, from: data!)
                    self.arrAdvertisement = responseModel.data?.advertisement ?? []
                    self.viewApprove.isHidden = responseModel.approval_text?.isEmpty ?? false
                    self.lblApprove.text = responseModel.approval_text
                    self.arrReviews = responseModel.data?.reviews ?? []
                    self.lblCount.text = responseModel.notification_count
                    if responseModel.notification_count == "0" {
                        self.lblCount.isHidden = true
                    } else {
                        self.lblCount.isHidden = false
                    }
                    for chat in responseModel.data!.last_chat_list! {
                        if !chat.lastMsg!.isEmpty {
                            self.arrChats.append(chat)
                        }
                    }
                   // self.arrChats = responseModel.data?.last_chat_list ?? []
                    self.setHomeData()
                }
                catch {
                    
                }
            }
        }
    }
    func wsGetProfileData(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_USER_PROFILE, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam,isShowLoading: false) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelLogin.self, from: data!)
                    AppSingleton.shared.user = responseModel.data
                    preferenceHelper.setUserName((AppSingleton.shared.user?.first_name ?? "") + " " + (AppSingleton.shared.user?.last_name ?? ""))
                    preferenceHelper.setUserPic(AppSingleton.shared.user?.profile_image ?? "")
                    self.lblUserName.text = preferenceHelper.getUserName()
                    self.imgUser.downloadedFrom(link: preferenceHelper.getUserPic())
                }
                catch {
                    
                }
            }
        }
    }
}
//MARK:- StackView Protocol
extension SPHomeVC : BSStackViewProtocol{
    func stackView(_ stackView: BSStackView?, willSendItem item: UIView?, direction: BSStackViewItemDirection) {
        print(String(format: "stackView willSendItem %ld direction %@", item?.tag ?? 0, translate(direction)!))
    }
    func stackView(_ stackView: BSStackView?, didSendItem item: UIView?, direction: BSStackViewItemDirection) {
            print(String(format: "stackView didSendItem %ld direction %@", item?.tag ?? 0, translate(direction)!))
    }
}
//MARK: - TableView Delegate Method
extension SPHomeVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReviews.count > 2 ? 2:arrReviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SPTVCell
        cell.setSPRating(data: arrReviews[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}

//MARK: - CollectionView Method
extension SPHomeVC : UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == cvBanner {
        pageControl.numberOfPages = arrAdvertisement.count
        pageControl.isHidden = !(arrAdvertisement.count > 1)
        return self.arrAdvertisement.count
        } else {
            return arrChats.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          if collectionView == cvBanner {
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SPHomeCVCell
              if (arrAdvertisement[indexPath.item].file_type ?? "") == "1" {
                  cell.imgView.isHidden = false
                  cell.imgView.downloadedFrom(link: arrAdvertisement[indexPath.item].image ?? "")
              }
              else {
                  cell.imgView.isHidden = true
                  if indexPath.item == videoIndex {
                      DispatchQueue.main.asyncAfter(deadline: .now()+0.3) { [self] in
                          cell.layoutIfNeeded()
                          self.avplayer = AVPlayer.init(url: URL.init(string: (self.arrAdvertisement[indexPath.item].image ?? ""))!)
                        //  avplayer = AVPlayer.init(url: URL.init(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4")!)
                          cell.layoutIfNeeded()
                          let avLayer = AVPlayerLayer.init(player: self.avplayer)
                          avLayer.name = "avLayer"
                          avLayer.frame = cell.contentView.bounds
                          avLayer.videoGravity = .resizeAspectFill
                          avLayer.cornerRadius = 10
                          avLayer.masksToBounds = true
                          cell.contentView.layer.sublayers?.removeAll(where: {$0.name == "avLayer"})
                          cell.contentView.layer.insertSublayer(avLayer, at: 0)
                          cell.contentView.layer.cornerRadius = 10
                          
                          self.avplayer?.play()
                      }
                      
                  }
              }
              
              
              return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ChatCVCell
            cell.setChatData(data: arrChats[indexPath.row])
            return cell
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == cvChats {
            let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            vc.conversationId = arrChats[indexPath.item].conversation_id ?? ""
            vc.recipient_id = arrChats[indexPath.item].user_id ?? ""
            vc.receiver_name = arrChats[indexPath.item].sender_name ?? ""
            self.pushToVC(vc)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView ==  cvBanner{
            return CGSize(width: collectionView.frame.size.width - 10 , height: collectionView.frame.size.height)
        }else{
            return CGSize(width: collectionView.frame.size.width / 3 - 10 , height: collectionView.frame.height)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == cvBanner {
            print("Did scrollViewDidEndDecelerating :- \(Int(scrollView.contentOffset.x) / Int(scrollView.frame.width))")
            let index = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
            if  (arrAdvertisement[index].file_type ?? "") == "1"
            {
               // let cell = cvBanner.cellForItem(at: IndexPath.init(item: index, section: 0)) as! SPHomeCVCell
                avplayer?.pause()
               // cell.contentView.layer.sublayers?.removeAll(where: {$0.name == "avLayer"})
            } else {
                let cell = cvBanner.cellForItem(at: IndexPath.init(item: index, section: 0)) as! SPHomeCVCell
                cell.imgView.isHidden = true
                if avplayer == nil {
                    avplayer = AVPlayer.init(url: URL.init(string: (arrAdvertisement[index].image ?? ""))!)
                }
                else {
                    avplayer?.pause()
                    let item = AVPlayerItem.init(url: URL.init(string: (arrAdvertisement[index].image ?? ""))!)
                    avplayer?.replaceCurrentItem(with: item)
                }
              //  avplayer = AVPlayer.init(url: URL.init(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4")!)
                let avLayer = AVPlayerLayer.init(player: avplayer)
                avLayer.name = "avLayer"
                avLayer.frame = cell.contentView.bounds
                avLayer.videoGravity = .resizeAspectFill
                avLayer.cornerRadius = 10
                avLayer.masksToBounds = true
                cell.contentView.layer.sublayers?.removeAll(where: {$0.name == "avLayer"})
                cell.contentView.layer.insertSublayer(avLayer, at: 0)
                cell.contentView.layer.cornerRadius = 10
                cell.layoutIfNeeded()
                videoIndex = index
                avplayer?.play()
            }
            pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        }
        
     
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        print("Did End :- \(Int(scrollView.contentOffset.x) / Int(scrollView.frame.width))")
        pageControl?.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    
}

