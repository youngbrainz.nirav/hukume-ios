//
//  SPSessionDetailsVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/27/21.
//

import UIKit
import Cosmos

class SPSessionDetailsVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblRating : UILabel!
    @IBOutlet weak var btnCancel : UIButton!
    @IBOutlet weak var viewReview : UIView!
    @IBOutlet weak var viewRating : CosmosView!
    
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var stkChats: UIStackView!
    
    @IBOutlet weak var viewColor : UIView!
    @IBOutlet weak var lblAvailable : UILabel!
    @IBOutlet weak var scrView: UIScrollView!
    
    @IBOutlet weak var btnVideoCall: UIButton!
    
    //MARK:- Varibles
    var isPastSession = false
    var objSession:DataSessionList?
    var recipient_id = ""
    var isFromPush = false
    var sessionId = ""
    var sessionDetail:DataSessionDetail?
    var isStartSession = false
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewRating.settings.fillMode = .half
        // Do any additional setup after loading the view.
        self.lblDescription.text = ""
        self.lblRating.text = ""
        
        if isPastSession{
            
            self.viewReview.isHidden =  false
            self.btnCancel.isHidden =  true
            lblAvailable.text = "Active"
            viewColor.backgroundColor = UIColor(red: 92 / 255, green: 222 / 255, blue: 131 / 255 , alpha: 1.0)
            lblAvailable.text = "Completed"
            viewColor.backgroundColor = UIColor(red: 254 / 255, green: 201 / 255, blue: 58 / 255 , alpha: 1.0)
        }else{
            self.viewReview.isHidden =  true
            self.btnCancel.isHidden =  false
            self.btnCancel.setTitle("Start Session", for: .normal)
            lblAvailable.text = "Active"
        }
        self.btnCancel.setTitle("Start Session", for: .normal)
        
        scrView.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        wsGetSessionDetail()
    }
    
    
    //MARK:- Button Action
    @IBAction func btnChatAction(_ sender: Any) {
        wsStartConversation()
    }
    
    @IBAction func btnVideoCallAction(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "VideoChatViewController") as! VideoChatViewController
        vc.user_id = preferenceHelper.getUserId()
        vc.Token = self.sessionDetail?.agora_token ?? ""
        vc.channelName = self.sessionDetail?.channelName ?? ""
        vc.userName = lblUserName.text!
        vc.sessionId = self.sessionDetail?._id ?? ""
        self.pushToVC(vc)
    }
    @IBAction func btnBackAction(_ sender: Any) {
        if isFromPush {
            appDel.gotoHome()
        } else {
        self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func btnCancelAction(_ sender: Any) {
        
        wsStartSession()
    }
    @IBAction func btnCloseViewAction(_ sender: Any) {
        
    }
    @IBAction func btnCancelConfirmAction(_ sender: Any) {
        
    }
    @IBAction func btnTempAction(_ sender: Any) {
        
    }
    func setSessionDetail(data:DataSessionDetail){
        self.sessionDetail = data
        lblUserName.text = (data.user_name ?? "")
        lblCategory.text = data.category_name ?? " "
        lblRating.text = data.reviews?.description ?? " "
        lblDescription.text = data.description
        lblDate.text = data.booking_date
        lblTime.text = data.start_time
        imgUser.downloadedFrom(link: data.profile_image ?? "")
        viewRating.rating = data.reviews?.rating?.toDouble() ?? 0
        btnCancel.isHidden = data.status == "2"
        recipient_id = data.user_id ?? ""
        viewReview.isHidden = data.reviews?.rating == nil
        btnCancel.isHidden = false
        stkChats.isHidden = false
        btnVideoCall.isHidden = true
        if data.status == "0" {
            viewReview.isHidden = true
            btnCancel.isHidden = false
            lblAvailable.text = "Upcoming"
            viewColor.backgroundColor = UIColor(red: 92 / 255, green: 222 / 255, blue: 131 / 255 , alpha: 1.0)
        }
      else if data.status == "2" {
          btnVideoCall.isHidden = false
            btnCancel.isHidden = true
            lblAvailable.text = "Active"
            viewColor.backgroundColor = UIColor(red: 92 / 255, green: 222 / 255, blue: 131 / 255 , alpha: 1.0)
          viewReview.isHidden = true
        }
        else if data.status == "3" {
            btnCancel.isHidden = true
            lblAvailable.text = "Cancelled"
            viewColor.backgroundColor = .systemRed
            viewReview.isHidden = true
            lblRating.text = " "
        }
        else if data.status == "4" {
            btnCancel.isHidden = true
            lblAvailable.text = "Completed"
            viewColor.backgroundColor = UIColor(red: 254 / 255, green: 201 / 255, blue: 58 / 255 , alpha: 1.0)
        }
//        let currentDate = Utils.convertDatetoString(Date(), "", toFormate: "yyyy-MM-dd HH:mm:ss")
//        let diff = Utils.findDateDiff(time1Str: currentDate, time2Str: data.session_date_time ?? "", toFormate: "yyyy-MM-dd HH:mm:ss")
//        print("differece in min: ",diff)
//        if diff <= 5 || isStartSession {
//            btnCancel.isEnabled = true
//            btnCancel.backgroundColor = .themeColor
//        } else {
//            btnCancel.isEnabled = false
//            btnCancel.backgroundColor = .themeColor.withAlphaComponent(0.7)
//        }
        //btnCancel.isEnabled = true
    }
   //MARK: - API Calling
    func wsGetSessionDetail(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.session_id] = objSession?._id ?? sessionId
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_SESSION_DETAIL, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            self.scrView.isHidden = false
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelSessionDetail.self, from: data!)
                    self.setSessionDetail(data: (responseModel.data!))
                    
                }
                catch {
                    
                }
                
            }
        }
        
    }
    func wsStartSession(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.booking_id] = objSession?._id ?? sessionId
        dictParam[PARAMS.session_status] = "2"
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.CHANGE_SESSION_BOOKING_STATUS, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response, isSuccessToast: true) {
                self.wsGetSessionDetail()
               
            }
        }
    }
    func wsStartConversation(){
        var dictParam : [String:Any] = [:]
        dictParam[PARAMS.sender_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.sender_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.recipient_id] = recipient_id
        dictParam[PARAMS.session_id] = objSession?._id ?? sessionId
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.START_CONVERSATION, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response) {
                let data = response["data"] as! [String:Any]
                var conversationID = ""
                if let conv_id = data["conversation_id"] as? Int {
                    conversationID = "\(conv_id)"
                } else {
                    conversationID = data["conversation_id"] as? String ?? ""
                }
                let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                vc.conversationId = conversationID
                vc.recipient_id = self.recipient_id
                vc.receiver_name = self.lblUserName.text!
                self.pushToVC(vc)
            }
        }
    }
    
}
