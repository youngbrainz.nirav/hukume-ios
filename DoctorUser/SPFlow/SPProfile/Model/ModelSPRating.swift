//
//  ModelSPRating.swift
//  DoctorUser
//
//  Created by Youngbrainz Mac Air on 15/03/22.
//

import Foundation

struct ModelSPRating : Codable {
    let data : [Reviews]?
    let status : Bool?
    let msg : String?

    enum CodingKeys: String, CodingKey {

        case data = "data"
        case status = "status"
        case msg = "msg"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent([Reviews].self, forKey: .data)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        msg = try values.decodeIfPresent(String.self, forKey: .msg)
    }

}
