//
//  EditProfessionalVC.swift
//  DoctorSP
//
//  Created by VATSAL on 1/10/22.
//

import UIKit
import DropDown
import MobileCoreServices

class EditProfessionalVC: UIViewController, UINavigationControllerDelegate {
    
    //MARK: - Outlets
    @IBOutlet weak var txtDescription : UITextView!
    @IBOutlet weak var txtExperience : UITextField!
    @IBOutlet weak var lblCategory : UILabel!
    @IBOutlet weak var lblSubCategory : UILabel!
    @IBOutlet weak var imgCategoryDown : UIImageView!
    @IBOutlet weak var imgSubCategoryDown : UIImageView!
    
    @IBOutlet weak var viewCategory : UIView!
    @IBOutlet weak var viewSubCategory : UIView!
    
    @IBOutlet weak var btnCategory : UIButton!
    @IBOutlet weak var btnSubCategory : UIButton!
    @IBOutlet weak var lblFileName: UILabel!
    @IBOutlet weak var tblCategory : UITableView!
    @IBOutlet weak var heightConstTblCategory : NSLayoutConstraint!
    
    //MARK: - Varibles
    var strPlaceHolder = "Write some words about you"
    let ddCategory = DropDown()
    let ddSubCategory = DropDown()
    let ddLanguage = DropDown()
    
    var arrCategoryString:[String] = []
    var arrSubCategoryString:[String] = []
    var arrCategory:[Categories] = []
    var arrSubCategory:[Categories] = []
    var imagePicker = UIImagePickerController()
    var arrSelectedSubCategory = [String]()
    var isEdit = false
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtDescription.delegate = self
        txtDescription.text = strPlaceHolder
        txtDescription.textColor = .lightGray
        setDropDowns()
        txtExperience.font = FontHelper.Font(size: 17, style: .SemiBold)
        txtDescription.font = FontHelper.Font(size: 17, style: .SemiBold)
        wsGetCategorySubCategories()
        
        self.lblCategory.textColor = .black
        self.lblSubCategory.textColor = .black
        self.txtDescription.textColor = .black
        RegisterSingleton.shared.documentData = nil
        lblFileName.text = "Attach Document"
        wsGetCategorySubCategories(id: AppSingleton.shared.user?.category_id ?? "")
        setData()
    }
    func setData(){
        lblCategory.text = AppSingleton.shared.user?.category_name
        //lblSubCategory.text = AppSingleton.shared.user?.sub_category_name
        txtExperience.text  = AppSingleton.shared.user?.year_of_experience
        txtDescription.text = AppSingleton.shared.user?.about_you
        arrSubCategoryString = AppSingleton.shared.user?.sub_category_name?.components(separatedBy: ",") ?? []
        arrSelectedSubCategory = AppSingleton.shared.user?.sub_category_ids?.components(separatedBy: ",") ?? []
     //   arrSubCategory = AppSingleton.shared.user?.sub_category_data ?? []
        RegisterSingleton.shared.sub_category_ids = AppSingleton.shared.user?.sub_category_ids ?? ""
       // tblCategory.reloadData()
        heightConstTblCategory.constant = tblCategory.contentSize.height
        DispatchQueue.main.async {
            self.heightConstTblCategory.constant = self.tblCategory.contentSize.height
        }
        RegisterSingleton.shared.category_id = AppSingleton.shared.user?.category_id ?? ""
    
    }
  
    
    //MARK: - Set DropDowns
    func setDropDowns(){
        //Category
//        ddCategory.direction = .bottom

//        
//        self.ddCategory.anchorView = btnCategory
//        self.ddCategory.dataSource = arrCategoryString
//        self.ddCategory.bottomOffset = CGPoint(x: 0, y:(self.ddCategory.anchorView?.plainView.bounds.height)!)
//        self.ddCategory.topOffset = CGPoint(x: 0, y:-(self.ddCategory.anchorView?.plainView.bounds.height)!)
//        ddCategory.selectionAction = { [unowned self] (index: Int, item: String) in
//            print("Selected item: \(item) at index: \(index)")
//            self.lblCategory.text = self.arrCategoryString[index]
//        }
//        
//        
//        
//        //SubCategory
//        self.ddSubCategory.anchorView = viewSubCategory
//        self.ddSubCategory.dataSource = arrSubCategoryString
//        self.ddSubCategory.bottomOffset = CGPoint(x: 0, y:(self.ddSubCategory.anchorView?.plainView.bounds.height)!)
//        self.ddSubCategory.topOffset = CGPoint(x: 0, y:-(self.ddSubCategory.anchorView?.plainView.bounds.height)!)
//        ddSubCategory.selectionAction = { [unowned self] (index: Int, item: String) in
//            print("Selected item: \(item) at index: \(index)")
//            self.lblSubCategory.text = self.arrSubCategoryString[index]
//        }
        
    }
    func setCategory(){
        arrCategoryString.removeAll()
        arrSubCategoryString.removeAll()
        
        
        for cat in arrCategory {
            arrCategoryString.append(cat.name!)
        }
        
        //Category
        self.ddCategory.anchorView = btnCategory
        self.ddCategory.dataSource = arrCategoryString
        self.ddCategory.bottomOffset = CGPoint(x: 0, y:(self.ddCategory.anchorView?.plainView.bounds.height)!)
        self.ddCategory.topOffset = CGPoint(x: 0, y:-(self.ddCategory.anchorView?.plainView.bounds.height)!)
        ddCategory.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if RegisterSingleton.shared.category_id == self.arrCategory[index]._id! {return}
            arrSelectedSubCategory.removeAll()
            RegisterSingleton.shared.sub_category_ids = ""
            self.lblCategory.text = self.arrCategoryString[index]
            
            RegisterSingleton.shared.category_id = self.arrCategory[index]._id!
            self.wsGetCategorySubCategories(id: self.arrCategory[index]._id!)
            if RegisterSingleton.shared.category_id == AppSingleton.shared.user?.category_id {
                arrSelectedSubCategory = AppSingleton.shared.user?.sub_category_ids?.components(separatedBy: ",") ?? []
            }
        }
        
        
        
        
    }
    func setSubCategory(){
        arrSubCategoryString.removeAll()
        for sub in arrSubCategory {
            
            arrSubCategoryString.append(sub.name!)
        }
        tblCategory.reloadData()
        heightConstTblCategory.constant = tblCategory.contentSize.height
        DispatchQueue.main.async {
            self.heightConstTblCategory.constant = self.tblCategory.contentSize.height
        }
        
    }
    
    func wsGetCategorySubCategories(id:String=""){
        let dictParam:[String:Any] = id.isEmpty ? [:]:[PARAMS.category_id:id]
       
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_SP_CATEGORIES_SUB_CATEGORIES, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelCategories.self, from: data!)
                    
                    
                    if id.isEmpty {
                        self.arrCategory = responseModel.data?.categories ?? []
                        self.setCategory()
                    } else {
                        self.arrSubCategory = responseModel.data?.sub_categories ?? []
                        self.setSubCategory()
                    }
                    
                } catch {
                    
                }
               
            }
        }
    }
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUpdateAction(_ sender: Any) {
        if checkValidation() {
            if arrSelectedSubCategory.count > 0 {
            RegisterSingleton.shared.sub_category_ids = ""
            for sub in arrSelectedSubCategory {
                RegisterSingleton.shared.sub_category_ids = RegisterSingleton.shared.sub_category_ids.isEmpty ? sub:(RegisterSingleton.shared.sub_category_ids+","+sub)
            }
            }
            print("Subcategories: ",RegisterSingleton.shared.sub_category_ids)
            wsUpdateProfile()
        }
   
    }

    @IBAction func btnAttachDocument(_ sender: Any) {
        //openDocumnetPicker()
        alertForDocument()
    }
    @IBAction func btnDropDownAction(_ sender: UIButton) {
        
        
        if sender.tag ==  1{
            print("category")
            self.lblCategory.textColor = .black
            viewCategory.backgroundColor =  .white
            imgCategoryDown.image =  UIImage(named: "ic_down_black")
//            viewFirstName.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            ddCategory.show()
            
        }else if sender.tag == 2{
            print("sub category")
            self.lblSubCategory.textColor = .black
            viewSubCategory.backgroundColor =  .white
            imgSubCategoryDown.image =  UIImage(named: "ic_down_black")
            ddSubCategory.show()
        }
        
    }
    @IBAction func btnAddCategoryAction(_ sender: Any) {
        
    }
    func checkValidation()->Bool{
        if txtExperience.isEmpty {
            
            Common().showAlert(strMsg: "Please enter years of experience.", view: self)
           return false
        }
        else if txtDescription.text == strPlaceHolder {
            
            Common().showAlert(strMsg: "Please enter some words about you.", view: self)
            return false
        }
        else if lblCategory.text!.lowercased() == "select category" {
           Common().showAlert(strMsg: "Please select category.", view: self)
           return false
        } else if arrSelectedSubCategory.isEmpty {
           Common().showAlert(strMsg: "Please select sub category.", view: self)
           return false
       }
//       else if lblFileName.text?.lowercased() == "Attached Document".lowercased() {
//               Common().showAlert(strMsg: "Please attach degree certificate.", view: self)
//               return false
//
//       }
        return true
    }
    func wsUpdateProfile(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.year_of_experience] = txtExperience.text
        dictParam[PARAMS.category_id] = RegisterSingleton.shared.category_id
        dictParam[PARAMS.sub_category_ids] = RegisterSingleton.shared.sub_category_ids
        dictParam[PARAMS.about_you] = txtDescription.text
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
  
        
        if RegisterSingleton.shared.documentData != nil {
            alamofire.getResponseFromURL(url: WebService.UPDATE_USER_PROFILE, paramData: dictParam, pdf: RegisterSingleton.shared.documentData!, isPdf: RegisterSingleton.shared.isPdf) { (response, data, error) -> (Void) in
                if Parser.isSuccess(response: response, isSuccessToast: true) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        } else {
            alamofire.getResponseFromURL(url: WebService.UPDATE_USER_PROFILE, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
                if Parser.isSuccess(response: response, isSuccessToast: true) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
}
//MARK: - Textview Delegate Methods
extension EditProfessionalVC : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == strPlaceHolder {
            txtDescription.textColor = .black
            textView.text = nil
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = strPlaceHolder
            txtDescription.textColor = .lightGray
        }
    }

}
extension EditProfessionalVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSubCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CategoryTVCell
        if arrSelectedSubCategory.contains(arrSubCategory[indexPath.row]._id ?? "") {
            cell.btnCheck.isSelected = true
        } else {
            cell.btnCheck.isSelected = false
        }
        cell.lblSubcategory.text = arrSubCategory[indexPath.row].name
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! CategoryTVCell
        cell.btnCheck.isSelected = !cell.btnCheck.isSelected
        if self.arrSelectedSubCategory.contains(arrSubCategory[indexPath.row]._id ?? ""){
            self.arrSelectedSubCategory = self.arrSelectedSubCategory.filter{$0 != arrSubCategory[indexPath.row]._id}
        }else{
            self.arrSelectedSubCategory.append(arrSubCategory[indexPath.row]._id ?? "")
            
        }
        
    }
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 60
//    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
extension EditProfessionalVC:UIDocumentPickerDelegate,UIImagePickerControllerDelegate {
    func alertForDocument(){
        let alert = UIAlertController(title: "Choose Document", message: nil, preferredStyle: .actionSheet)
//        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
//            self.openCamera()
//        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        alert.addAction(UIAlertAction(title: "Document", style: .default, handler: { _ in
            self.openDocumnetPicker()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera()
        {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                    imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    imagePicker.allowsEditing = false
                DispatchQueue.main.async {
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
                }else
            {
                let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }

        func openGallary()
        {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                    self.present(imagePicker, animated: true, completion: nil)
                }
        }
    func openDocumnetPicker(){
        let documentPicker = UIDocumentPickerViewController.init(documentTypes: [String.init(kUTTypePDF), String.init(kUTTypePNG), String.init(kUTTypeJPEG),"com.microsoft.word.doc", "org.openxmlformats.wordprocessingml.document"], in: .import)
        documentPicker.delegate = self
        documentPicker.allowsMultipleSelection = false
        
        self.present(documentPicker, animated: true, completion: nil)
    }
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        print(urls.first)
        let fileSize = urls.first?.fileSize()
        print(fileSize)
        if fileSize! > 5 {
            Common().showAlert(strMsg: "File size must be 5 MB or less.", view: self)
            return
        }
        lblFileName.text = urls.first?.lastPathComponent
        if urls.first?.lastPathComponent.hasSuffix(".pdf") ?? false {
            RegisterSingleton.shared.isPdf = true
        }
        RegisterSingleton.shared.degree_certificate = lblFileName.text!
        RegisterSingleton.shared.documentData = try! Data.init(contentsOf: urls.first!)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }
        let imageSize = image.getSizeIn(.megabyte)
        print(imageSize)
        if imageSize > 5 {
            Common().showAlert(strMsg: "File size must be 5 MB or less.", view: self)
            return
        }
        if let imageUrl = info[.imageURL] as? URL {
            
            lblFileName.text = imageUrl.lastPathComponent
                }
        RegisterSingleton.shared.degree_certificate = lblFileName.text!
        RegisterSingleton.shared.documentData = image.jpegData(compressionQuality: 0.5)
        RegisterSingleton.shared.isPdf = false
        // print out the image size as a test
        print(image.size)
    }
}
