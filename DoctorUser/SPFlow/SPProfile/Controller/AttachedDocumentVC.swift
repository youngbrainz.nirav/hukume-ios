//
//  AttachedDocumentVC.swift
//  DoctorUser
//
//  Created by Youngbrainz Mac Air on 21/04/22.
//

import UIKit
import PDFKit
import WebKit

class AttachedDocumentVC: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var blankView: UIView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var viewPdf: PDFView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var viewMain: UIView!
    var tranform = CGAffineTransform.init()
    var titleName = ""
    override func viewDidLoad() {
        super.viewDidLoad()
     
       // lblTitle.text = titleName
        // Do any additional setup after loading the view.
    }
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initialSetup()
        setupImgView()
    }
    func initialSetup(){
        if AppSingleton.shared.user?.degree_certificate?.hasSuffix(".pdf") ?? false {
            viewPdf.isHidden = false
            imgView.isHidden = true
            setPdfData()
        } else if AppSingleton.shared.user?.degree_certificate?.hasSuffix(".doc") ?? false || AppSingleton.shared.user?.degree_certificate?.hasSuffix(".docx") ?? false {
            guard let url = URL.init(string: (AppSingleton.shared.user?.degree_certificate?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!) else {return}
            let webView = WKWebView.init(frame: viewPdf.frame)
            webView.load(URLRequest.init(url: url))
            self.viewMain.addSubview(webView)
            viewPdf.isHidden = true
            imgView.isHidden = true
        }
        else {
            viewPdf.isHidden = true
            imgView.isHidden = false
            imgView.downloadedFrom(link: AppSingleton.shared.user?.degree_certificate ?? "",placeHolder: "",isIndicator: true)
        }
    }
    func setPdfData(){
        guard let url = URL.init(string: (AppSingleton.shared.user?.degree_certificate ?? "").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) else {return}
        viewPdf.document = PDFDocument.init(url: url)
        viewPdf.minScaleFactor = viewPdf.scaleFactorForSizeToFit
        viewPdf.autoScales = true
    }
    func setupImgView(){
        imgView.isUserInteractionEnabled = true
        imgView.addGestureRecognizer(UIPinchGestureRecognizer.init(target: self, action: #selector(handleZoom(gesture:))))
        imgView.addGestureRecognizer(UIPanGestureRecognizer.init(target: self, action: #selector(handleMove(gesture:))))
    }
    @objc func handleZoom(gesture:UIPinchGestureRecognizer) {
        self.view.bringSubviewToFront(navigationView)
        self.view.bringSubviewToFront(blankView)
        let scale = gesture.scale
        if gesture.state == .began {
            tranform = imgView.transform
        }
        if gesture.state == .changed {
        imgView.transform = tranform.scaledBy(x: scale, y: scale)
        }
    }
    @objc func handleMove(gesture:UIPanGestureRecognizer) {
        self.view.bringSubviewToFront(navigationView)
        self.view.bringSubviewToFront(blankView)
        let translation = gesture.translation(in: self.view)
        
        if gesture.state == .began {
            tranform = imgView.transform
        }
        if gesture.state == .changed {
            imgView.transform = tranform.translatedBy(x: translation.x, y: translation.y)
        }
        
        
    }

   
    @IBAction func btnCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
