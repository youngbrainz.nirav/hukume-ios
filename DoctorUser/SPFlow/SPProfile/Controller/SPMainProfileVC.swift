//
//  ProfileVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/21/21.
//

import UIKit
import PDFKit

class SPMainProfileVC: UIViewController {

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblSubCategory: UILabel!
    
    @IBOutlet weak var lblAttachedDocument: UILabel!
    @IBOutlet weak var viewPdf: PDFView!
    @IBOutlet weak var lblYearsOfExp: UILabel!
    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    
    @IBOutlet weak var viewForDocument: UIView!
    @IBOutlet weak var imgDocument: UIImageView!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var btnProfile1: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        wsGetProfileData()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imgUser.applyCornerRadiusWith(radius: 10)
    }
    //MARK:- Button Action
    @IBAction func btnAttachedDocumentAction(_ sender: Any) {
        if AppSingleton.shared.user?.degree_certificate?.isEmpty ?? true {return}
//        if AppSingleton.shared.user?.degree_certificate?.hasSuffix(".doc") ?? false || AppSingleton.shared.user?.degree_certificate?.hasSuffix(".docx") ?? false {
//            let url = URL.init(string: (AppSingleton.shared.user?.degree_certificate)!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
//            let items:[Any] = [url]
//            
//            print(url)
//            let activityController = UIActivityViewController.init(activityItems: items, applicationActivities: nil)
//            activityController.excludedActivityTypes = [.mail,.message]
//            self.present(activityController, animated: true, completion: nil)
//            return
////            let documentInteractionController = UIDocumentInteractionController()
////            documentInteractionController.url = url
////            documentInteractionController.uti = url?.uti
////            documentInteractionController.presentOptionsMenu(from: CGRect.init(x: 0, y: UIScreen.main.bounds.height/2, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height/2), in: self.view, animated: true)
////            return
//        }
        let storyboard = UIStoryboard.init(name: STORYBOARDNAME.SPProfile, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AttachedDocumentVC") as! AttachedDocumentVC
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
//        viewForDocument.isHidden = false
//        if AppSingleton.shared.user?.degree_certificate?.hasSuffix(".pdf") ?? false {
//            viewPdf.isHidden = false
//            imgDocument.isHidden = true
//            setPdfData()
//        } else {
//            viewPdf.isHidden = true
//            imgDocument.isHidden = false
//            imgDocument.downloadedFrom(link: AppSingleton.shared.user?.degree_certificate ?? "",placeHolder: "",isIndicator: true)
//        }
    }
    
    @IBAction func btnClosePopUp(_ sender: Any) {
        viewForDocument.isHidden = true
    }
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnEditProfileAction(_ sender: Any) {
        let button = sender as! UIButton
        let btnTag = button.tag
        if btnTag == 1{
        let vc = UIStoryboard(name: STORYBOARDNAME.SPProfile, bundle: nil).instantiateViewController(withIdentifier: "EditSPProfileVC") as! EditSPProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
        }else if btnTag == 2{
            let vc = UIStoryboard(name: STORYBOARDNAME.SPProfile, bundle: nil).instantiateViewController(withIdentifier: "EditProfessionalVC") as! EditProfessionalVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func setPdfData(){
        guard let url = URL.init(string: AppSingleton.shared.user?.degree_certificate ?? "") else {return}
        viewPdf.document = PDFDocument.init(url: url)
        viewPdf.minScaleFactor = viewPdf.scaleFactorForSizeToFit
        //viewPdf.autoScales = true
    }
    func setProfileData(){
        lblUserName.text = preferenceHelper.getUserName()
        lblEmail.text = AppSingleton.shared.user?.email_id
        lblPhone.text = (AppSingleton.shared.user?.country_code ?? "") + (AppSingleton.shared.user?.mobile_no ?? "")
        lblAddress.text = AppSingleton.shared.user?.address
        imgUser.downloadedFrom(link: AppSingleton.shared.user?.profile_image ?? "")
        lblCategory.text = AppSingleton.shared.user?.category_name
        lblSubCategory.text = AppSingleton.shared.user?.sub_category_name
        lblYearsOfExp.text  = AppSingleton.shared.user?.year_of_experience
        lblAbout.text = AppSingleton.shared.user?.about_you
        lblRating.text = AppSingleton.shared.user?.avg_rating ?? "0"
        btnProfile.isHidden = AppSingleton.shared.user?.admin_approval == "0"
        btnProfile1.isHidden = AppSingleton.shared.user?.admin_approval == "0"
        lblAttachedDocument.text = AppSingleton.shared.user?.degree_certificate_name?.isEmpty ?? true ? "No document attached":AppSingleton.shared.user?.degree_certificate_name
    }
    func wsGetProfileData(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_USER_PROFILE, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelLogin.self, from: data!)
                    AppSingleton.shared.user = responseModel.data
                    preferenceHelper.setUserName((AppSingleton.shared.user?.first_name ?? "") + " " + (AppSingleton.shared.user?.last_name ?? ""))
                    preferenceHelper.setUserPic(AppSingleton.shared.user?.profile_image ?? "")
                    self.setProfileData()
                }
                catch {
                    
                }
            }
        }
    }
}
