//
//  SPRatingListVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/27/21.
//

import UIKit

class SPRatingListVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tblRating : UITableView!
    
    //MARK:- Varibles
    var arrReviews:[Reviews] = []
    //MARK:- View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        wsGetUserRating()
        // Do any additional setup after loading the view.
    
    }
    
    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - API Calling
    func wsGetUserRating(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.GET_USER_REVIEW_LIST, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { response, data, error in
            if Parser.isSuccess(response: response) {
                let jsonDecoder = JSONDecoder()
                do {
                    let responseModel = try jsonDecoder.decode(ModelSPRating.self, from: data!)
                    self.arrReviews = responseModel.data ?? []
                    self.tblRating.reloadData()
                }
                catch {
                    
                }
            }
        }
    }
    
}
//MARK:- TableView Delegate Method
extension SPRatingListVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SPTVCell
        cell.setSPRating(data: arrReviews[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}


