//
//  MenuVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/21/21.
//

import UIKit


struct MenuItem {
    let text: String
    var subItems: [String]?
    var isExpanded = false
    init(_ text: String, items: [String]? = nil) {
        self.text = text
        self.subItems = items
    }
}

class SPMenuVC: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var tblMenu : UITableView!
    @IBOutlet weak var viewBottom : UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    
    //MARK: - Varibles
    
    private let imgOpen = UIImage(named: "ic_Back_Right")
    private let imgClose = UIImage(named: "ic_selected_down_arrow")
    private var arrMenu = [MenuItem]()
    
    var selectedIndex =  -1
    
    
    
    
    //MARK: - View Lifc Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.tblMenu.tableFooterView =  viewBottom
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let whitSpace =  "     "
        self.arrMenu.removeAll()
        self.arrMenu.append(MenuItem("My Sessions"))
        self.arrMenu.append(MenuItem("Subscription"))
        //self.arrMenu.append(MenuItem("Settings", items: ["\(whitSpace)Availabilities"]))// "\(whitSpace)Payment"
        self.arrMenu.append(MenuItem("Availability"))
        self.arrMenu.append(MenuItem("About Us"))
        self.arrMenu.append(MenuItem("Share App"))
        self.arrMenu.append(MenuItem("Review & Rating to App"))
        self.arrMenu.append(MenuItem("Deactivate Account"))
        if AppSingleton.shared.user?.isSubscribedUserAutoPlan == 1 {
        self.arrMenu.append(MenuItem("Cancel Subscription"))
        }
        self.tblMenu.reloadData()
        
        lblUserName.text = preferenceHelper.getUserName()
        imgUser.downloadedFrom(link: preferenceHelper.getUserPic())
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        imgUser.applyCornerRadiusWith(radius: 10)
    }
    
    //MARK: - Button Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnViewProfileAction(_ sender: Any) {
        let vc = UIStoryboard(name: STORYBOARDNAME.SPProfile, bundle: nil).instantiateViewController(withIdentifier: "SPMainProfileVC") as! SPMainProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnLogoutAction(_ sender: Any) {
        print("logout")
        alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "Are you sure you want to logout?") { (status) in
            if status {
                self.wsLogout()
            }
        }
    }
    
    //MARK:- API Calling
    func wsLogout(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.LOGOUT_USER, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                appDel.gotoLogin()
            }
        }
    }
    func wsDeactivateAccount(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.DEACTIVATE_ACCOUNT, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                appDel.gotoLogin()
            }
        }
    }
    func wsCancelSubscription(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        let alamofire = AlamofireHelper.init()
        alamofire.getResponseFromURL(url: WebService.UNSUBSCRIBE_PLAN, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response) {
                AppSingleton.shared.user?.isSubscribedUserAutoPlan = 0
                self.arrMenu.removeLast()
                self.tblMenu.reloadData()
            }
        }
    }
    
}
//MARK: - TableView Delegate Method
extension SPMenuVC : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrMenu.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let item = self.arrMenu[section]
        if item.isExpanded, let count = item.subItems?.count {
            return count + 1
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MenuTVCell
        let item = self.arrMenu[indexPath.section]
        var imageView: UIImageView?
        if indexPath.row > 0, let text = item.subItems?[indexPath.row - 1] {
            cell.lblName.text =  text
            cell.lblName.textColor = .black
            cell.btnRight.isHidden = true
        } else {
            cell.lblName.text = item.text
            cell.btnRight.isHidden = false
            
            if item.subItems != nil {
                imageView = UIImageView(image: item.isExpanded ? self.imgClose : self.imgOpen)
                if  item.isExpanded{
                            cell.lblName.textColor = UIColor(red: 22 / 255, green: 100 / 255, blue: 252 / 255 , alpha:1.0)
                        cell.btnRight.setImage(UIImage(named: "ic_selected_down_arrow"), for: .normal)
                    }else{
                        cell.lblName.textColor = .black
                        cell.btnRight.setImage(UIImage(named: "ic_Back_Right"), for: .normal)
                    }
                }else{
                    cell.lblName.textColor = .black
                    cell.btnRight.setImage(UIImage(named: "ic_Back_Right"), for: .normal)
                }
        }
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.arrMenu[indexPath.section]
        if indexPath.row == 0 && item.subItems != nil {
            self.arrMenu[indexPath.section].isExpanded = !item.isExpanded
            let indexSet = IndexSet(integer: indexPath.section)
            tableView.reloadSections(indexSet, with: .automatic)
            
            print(" Expandable")
            
            
        } else {
            // non-expandable menu item tapped
            print("non Expandable")
            if indexPath.section == 0 {
                print("Session list")
                let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "SPSessionListVC") as! SPSessionListVC
                self.pushToVC(vc)
            }else if indexPath.section == 1 && AppSingleton.shared.user?.admin_approval == "1"{
                print("Revenu")
//                let vc = UIStoryboard(name: STORYBOARDNAME.SPPayment, bundle: nil).instantiateViewController(withIdentifier: "SPPaymentHistoryListVC") as! SPPaymentHistoryListVC
                let vc = UIStoryboard(name: STORYBOARDNAME.SPPayment, bundle: nil).instantiateViewController(withIdentifier: "SubscriptionVC") as! SubscriptionVC
                vc.isFromMenu = true
                self.pushToVC(vc)
            }else if indexPath.section == 2 && AppSingleton.shared.user?.admin_approval == "1"{
                print("indexPath :- \(indexPath), item.isExpanded : \(item.isExpanded)")
             //   if indexPath.row == 1{
                    print("Availabilities ")
                    let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "AvailabilitiesVC") as! AvailabilitiesVC
                    self.pushToVC(vc)
                    
             //   }
//                else if indexPath.row == 2{
//                    print("Payment")
//                    let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "SPSelectCategoryVC") as! SPSelectCategoryVC
//                    self.pushToVC(vc)
//
//                }
                
            }else if indexPath.section == 3{
                print("About")
                let vc = UIStoryboard(name: STORYBOARDNAME.SPProfile, bundle: nil).instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                self.pushToVC(vc)
            }else if indexPath.section == 4{
                print("Share")
                guard let url = URL.init(string: APP_URL) else {return}
                        if UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.open(url)
                        }
            }else if indexPath.section == 5{
                print("Rate")
                guard let url = URL.init(string: APP_URL) else {return}
                        if UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.open(url)
                        }
            }
            else if indexPath.section == 6{
                print("Deactivate")
                alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "If you deactivate or remove your account, all your records including payment will be removed and you will be required to sign up again if you like to use the app. Do you want to proceed ?") { (status) in
                    if status {
                        self.wsDeactivateAccount()
                    }
                }
            }
            else if indexPath.section == 7{
                print("cancel")
                alertTwoButton(title: "", titleButtonAccept: "Yes", titleButtonReject: "No", message: "If you cancel your subscription you will not have full access to the app. Do you want to proceed ?") { status in
                    if status {
                        self.wsCancelSubscription()
                    }
                }
            }
            self.tblMenu.reloadData()
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        let item = self.dataSource[indexPath.section]
        //        if item.isExpanded{
        //            return 10
        //        }else{
        return 70
        //        }
        
        
    }
    
    
    
}

