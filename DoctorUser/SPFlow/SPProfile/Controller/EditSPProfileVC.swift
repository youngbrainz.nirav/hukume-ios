//
//  EditSPProfileVC.swift
//  DoctorUser
//
//  Created by VATSAL on 12/21/21.
//

import UIKit
import CountryPickerView
import GooglePlaces
import GooglePlacePicker
class EditSPProfileVC: UIViewController,CountryPickerViewDelegate,GMSAutocompleteViewControllerDelegate {

    
    //MARK:- Outlets
    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var lblFirstName : UILabel!
    @IBOutlet weak var lblLastName : UILabel!
    @IBOutlet weak var lblPhone : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var lblAddress : UILabel!
    
    @IBOutlet weak var txtFirstName : UITextField!
    @IBOutlet weak var txtLastName : UITextField!
    @IBOutlet weak var txtPhone : UITextField!
    @IBOutlet weak var txtEmail : UITextField!
    @IBOutlet weak var txtAddress : UITextField!
    
    @IBOutlet weak var viewFirstName : UIView!
    @IBOutlet weak var viewLastName : UIView!
    @IBOutlet weak var viewPhone : UIView!
    @IBOutlet weak var viewEmail : UIView!
    @IBOutlet weak var viewAddress : UIView!
    @IBOutlet weak var btnCountryCode: UIButton!
    let countryPickerView_1 = CountryPickerView()
    //MARK:- Varibles
    var imagePicker = UIImagePickerController()
    var isImageChanged = false
    var country_code_info = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setData()
    }
    //MARK:- Set Data
    func setData(){
       
        btnCountryCode.setTitle("  "+(AppSingleton.shared.user?.country_code ?? ""), for: .normal)
        let code = countryPickerView_1.countries.filter({$0.phoneCode == (AppSingleton.shared.user?.country_code ?? "")})
        
        btnCountryCode.setImage(code.first?.flag, for: .normal)
        country_code_info = code.first?.code ?? ""
        countryPickerView_1.delegate = self
        btnCountryCode.titleLabel?.font = FontHelper.Font(size: 17, style: .SemiBold)
        txtFirstName.delegate =  self
        txtLastName.delegate =  self
        txtPhone.delegate =  self
        txtEmail.delegate =  self
        txtAddress.delegate =  self
        txtEmail.isUserInteractionEnabled = false
        txtPhone.isUserInteractionEnabled = false
        btnCountryCode.isUserInteractionEnabled = false
        txtFirstName.text = AppSingleton.shared.user?.first_name
        txtLastName.text = AppSingleton.shared.user?.last_name
        txtPhone.text = AppSingleton.shared.user?.mobile_no
        txtEmail.text = AppSingleton.shared.user?.email_id
        txtAddress.text = AppSingleton.shared.user?.address
        imgProfile.downloadedFrom(link: AppSingleton.shared.user?.profile_image ?? "")
        
    }

    //MARK:- Button Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChangePasswordAction(_ sender: Any) {
        
        let vc = UIStoryboard(name: STORYBOARDNAME.SPProfile, bundle: nil).instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnUpdateProfileAction(_ sender: Any) {
        if checkValidation() {
            wsUpdateProfile()
        }
    }
    @IBAction func btnImgeAction(_ sender: UIButton)
    {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func onClickBtnCountryCode(_ sender: Any) {
        countryPickerView_1.showCountriesList(from: self)
        
    }
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        btnCountryCode.setTitle("  "+country.phoneCode, for: .normal)
        btnCountryCode.setImage(country.flag, for: .normal)
        country_code_info = country.code
    }

    //MARK:- Camera and Gallery Function
    func openCamera()
        {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                    imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    imagePicker.allowsEditing = false
                DispatchQueue.main.async {
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
                    
                }else
            {
                let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }

        func openGallary()
        {
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                    imagePicker.delegate = self
                    imagePicker.allowsEditing = false
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                    self.present(imagePicker, animated: true, completion: nil)
                }
        }
}


//MARK:- TextField Delegate Methods
extension EditSPProfileVC : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtAddress {
            openAutoCompletePlace()
            return false
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtFirstName{
            self.lblFirstName.isHidden =  false
            viewFirstName.backgroundColor =  .white
            self.txtFirstName.placeholder = ""
        }else if textField == txtLastName{
            self.lblLastName.isHidden =  false
            viewLastName.backgroundColor =  .white
            self.txtLastName.placeholder = ""
        }else if textField == txtPhone{
            self.lblPhone.isHidden =  false
            viewPhone.backgroundColor =  .white
            self.txtPhone.placeholder = ""
        }else if textField == txtEmail{
            self.lblEmail.isHidden =  false
            viewEmail.backgroundColor =  .white
            self.txtEmail.placeholder = ""
        }else if textField == txtAddress{
            self.lblAddress.isHidden =  false
            viewAddress.backgroundColor =  .white
            self.txtAddress.placeholder = ""
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtFirstName || textField == txtLastName {
            if !string.isAllChar() || string.isEmpty {
                return true
            } else {
                return false
            }
        }
        if textField == txtPhone {
            if (string.isAllDigits()) || string.isEmpty   {
                return true
            } else {
               return false
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtFirstName{
            if txtFirstName.text ==  ""{
               // self.lblFirstName.isHidden =  true
                self.txtFirstName.placeholder = "First Name"
                viewFirstName.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblFirstName.isHidden =  false
                viewFirstName.backgroundColor =  .white

            }
            
        }else if textField == txtLastName{
            if txtLastName.text ==  ""{
               // self.lblLastName.isHidden =  true
                self.txtLastName.placeholder = "Last Name"
                viewLastName.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblLastName.isHidden =  false
                viewLastName.backgroundColor =  .white

            }
            
        }else if textField == txtPhone{
            if txtPhone.text ==  ""{
               // self.lblPhone.isHidden =  true
                self.txtPhone.placeholder = "Phone Number"
                viewPhone.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblPhone.isHidden =  false
                viewPhone.backgroundColor =  .white

            }
            
        }else if textField == txtEmail{
            if txtEmail.text ==  ""{
              //  self.lblEmail.isHidden =  true
                self.txtEmail.placeholder = "Email"
                viewEmail.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblEmail.isHidden =  false
                viewEmail.backgroundColor =  .white

            }
            
        }else if textField == txtAddress{
            if txtAddress.text ==  ""{
              //  self.lblAddress.isHidden =  true
                self.txtAddress.placeholder = "Address"
                viewAddress.backgroundColor =  UIColor(red: 239 / 255, green: 239 / 255, blue: 239 / 255, alpha: 1.0)
            }else{
                self.lblAddress.isHidden =  false
                viewAddress.backgroundColor =  .white

            }
            
        }
    }
    func openAutoCompletePlace(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        txtAddress.text = place.formattedAddress
        print(place)
        AppSingleton.shared.user?.latitude = String(place.coordinate.latitude)
        AppSingleton.shared.user?.longitude = String(place.coordinate.longitude)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("cancelled")
        dismiss(animated: true, completion: nil)
    }
    func checkValidation()->Bool{
       
        if txtFirstName.isEmpty {
            Common().showAlert(strMsg: "Please enter first name.", view: self)
            return false
        }
        else if txtLastName.isEmpty{
             
            Common().showAlert(strMsg: "Please enter last name.", view: self)
             return false
         }
        else if txtPhone.isEmpty{
             
            Common().showAlert(strMsg: "Please enter phone number.", view: self)
             return false
         }
        else if txtEmail.isEmpty{
             
            Common().showAlert(strMsg: "Please enter email.", view: self)
             return false
         }
        
       else if txtAddress.isEmpty{
            
        Common().showAlert(strMsg: "Please enter address.", view: self)
            return false
        }
       else if !txtEmail.text!.isEmail() {
           Common().showAlert(strMsg: "Please enter valid email.", view: self)
           return false
       }
       else if txtPhone.text!.count < 8 {
           Common().showAlert(strMsg: "Phone number should be at least 8 digit.", view: self)
           return false
       }
        return true
        
    }
    func wsUpdateProfile(){
        var dictParam:[String:Any] = [:]
        dictParam[PARAMS.loginuser_id] = preferenceHelper.getUserId()
        dictParam[PARAMS.session_token] = preferenceHelper.getSessionToken()
        dictParam[PARAMS.mobile_no] = txtPhone.text
        dictParam[PARAMS.country_code] = btnCountryCode.titleLabel?.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        dictParam[PARAMS.first_name] = txtFirstName.text
        dictParam[PARAMS.last_name] = txtLastName.text
        dictParam[PARAMS.address] = txtAddress.text
        dictParam[PARAMS.email_id] = txtEmail.text
        dictParam[PARAMS.country_code] = btnCountryCode.titleLabel?.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        dictParam[PARAMS.user_type] = preferenceHelper.getUserType()
        dictParam[PARAMS.latitude] = AppSingleton.shared.user?.latitude
        dictParam[PARAMS.longitude] = AppSingleton.shared.user?.longitude
        dictParam[PARAMS.country_code_info] = country_code_info
        let alamofire = AlamofireHelper.init()
        if isImageChanged {
            alamofire.getResponseFromURL(url: WebService.UPDATE_USER_PROFILE, paramData: dictParam, image: imgProfile.image) { (response, data, error) -> (Void) in
                if Parser.isSuccess(response: response, isSuccessToast: true) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        } else {
        alamofire.getResponseFromURL(url: WebService.UPDATE_USER_PROFILE, methodName: AlamofireHelper.POST_METHOD, paramData: dictParam) { (response, data, error) -> (Void) in
            if Parser.isSuccess(response: response, isSuccessToast: true) {
                self.navigationController?.popViewController(animated: true)
            }
        }
        }
    }
}

//MARK:- ImageDelegate Methods
extension EditSPProfileVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }
        let imageSize = image.getSizeIn(.megabyte)
        print(imageSize)
        if imageSize > 5 {
            Common().showAlert(strMsg: "File size must be 5 MB or less.", view: self)
            return
        }
        self.imgProfile.image = image
        isImageChanged = true


        // print out the image size as a test
        print(image.size)
    }
    
}
