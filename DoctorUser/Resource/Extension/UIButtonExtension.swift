//
//  UIButtonExtension.swift
//

import Foundation
import UIKit

extension UIButton{
    
    func alignTextUnderImage() {
        guard let imageView = imageView else {
            return
        }
        
        self.contentVerticalAlignment = .top
        self.contentHorizontalAlignment = .center
        let imageLeftOffset = (self.bounds.width - imageView.bounds.width) / 2 //put image in center
        let titleTopOffset = imageView.bounds.height + 5
        self.imageEdgeInsets = UIEdgeInsets.init(top: 10, left: imageLeftOffset, bottom: 0, right: 0)
        self.titleEdgeInsets = UIEdgeInsets.init(top: titleTopOffset, left: -imageView.bounds.width, bottom: 0, right: 0)
    }

}
extension UIButton {
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.setBackgroundImage(colorImage, for: forState)
    }
    
}
