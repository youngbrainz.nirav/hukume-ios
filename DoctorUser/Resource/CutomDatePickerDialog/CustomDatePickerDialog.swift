//
//  CustomPhotoDialog.swift
//  edelivery
//
//  Created by Elluminati on 22/02/17.
//  Copyright © 2017 Elluminati. All rights reserved.
//

import Foundation
import UIKit


public class CustomDatePickerDialog: UIView
{
   //MARK:- OUTLETS
    @IBOutlet weak var stkDialog: UIStackView!
    @IBOutlet weak var stkBtns: UIStackView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    //MARK:Variables
    var onClickRightButton : ((_ selectedDate:Date) -> Void)? = nil
    var onClickLeftButton : (() -> Void)? = nil
    static let  datePickerDialog = "dialogForDatePicker"
    
    
    
    public static func  showCustomDatePickerDialog
        (title:String,
         titleLeftButton:String,
         titleRightButton:String,
         mode:UIDatePicker.Mode = .date) ->
        CustomDatePickerDialog
     {
        let view = UINib(nibName: datePickerDialog, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomDatePickerDialog
        view.setLocalization()
        let frame = (Common.keyWindow?.frame)!;
        view.frame = frame;
        view.lblTitle.textColor = UIColor.black
        view.lblTitle.text = title;
        view.btnLeft.setTitle(titleLeftButton.capitalized, for: UIControl.State.normal)
        view.btnRight.setTitle(titleRightButton.capitalized, for: UIControl.State.normal)
        view.datePicker.datePickerMode = mode
        view.datePicker.setValue(false, forKey: "highlightsToday")
        view.datePicker.setValue(UIColor.black, forKeyPath: "textColor")
        Common.keyWindow?.addSubview(view)
        Common.keyWindow?.bringSubviewToFront(view);
        return view;
    }
    func setLocalization(){
        lblTitle.textColor = UIColor.black
       
        btnLeft.titleLabel?.font =   FontHelper.Font(size: 17, style: .Bold)
        btnRight.titleLabel?.font =   FontHelper.Font(size: 17, style: .Bold)
        
        self.backgroundColor = UIColor.gray.withAlphaComponent(0.7)
        self.alertView.backgroundColor = UIColor.white
        self.alertView.setRound(withBorderColor: .clear, andCornerRadious: 10.0, borderWidth: 1.0)
    }
    
    func setMinDate(mindate:Date)
    {
       datePicker.minimumDate = mindate
       datePicker.date = mindate
    }
    func setDate(date:Date) {
        datePicker.setDate(date, animated: false)
    }
    func setMaxDate(maxdate:Date)
    {
    datePicker.maximumDate = maxdate
    }
    
    //ActionMethods
    
    @IBAction func onClickBtnLeft(_ sender: Any)
    {
        if self.onClickLeftButton != nil
        {
            self.onClickLeftButton!();
        }
        
    }
    @IBAction func onClickBtnRight(_ sender: Any)
    {
        if self.onClickRightButton != nil
        {
            self.onClickRightButton!(datePicker.date)
        }
        
    }
    func setDatePickerForFutureTrip(mode:UIDatePicker.Mode = .dateAndTime)
    {
        self.datePicker.datePickerMode = mode
        self.datePicker.locale = Locale.current
        let gregorian = Calendar.init(identifier: Calendar.Identifier.gregorian)
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        
        var dateComponents = DateComponents.init()
        dateComponents.day = 90
        let currentDate = Date()
        let maxDate = gregorian.date(byAdding: dateComponents, to: currentDate)
        let minimumDate:Date = Date()
        datePicker.minimumDate = minimumDate
        
        datePicker.maximumDate = maxDate!
        datePicker.calendar = gregorian
        datePicker.timeZone = TimeZone.init(identifier: TimeZone.current.identifier)!
    }
    
   
}


