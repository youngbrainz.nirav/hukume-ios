//
//  Constants.swift
//  DoctorUser
//
//  Created by ThisIsAnkur on 01/02/22.
//

import Foundation
let googleAPIKey = "AIzaSyD3zlf76Nyjc2Hf_qWstNB_0wR3eAQzRnQ"
let preferenceHelper = PreferenceHelper.shared
let APP_URL = "https://itunes.apple.com/us/app/id1630756553?ls=1&mt=8"
struct WebService {
    static let BASE_URL = "https://ybtest.co.in/ondemand/api/"
    static let REGISTER_USER = "registerUser"
    static let LOGIN_USER = "loginUser"
    static let GET_USER_PROFILE = "getUserProfile"
    static let UPDATE_USER_PROFILE = "updateUserProfile"
    static let LOGOUT_USER = "logout"
    static let FORGOT_PASSWORD = "forgotPassword"
    static let GET_HOME_CATEGORY_LIST = "getHomePageCategoryList"
    static let CHECK_USER_EXIST = "checkUserExistOrNot"
    static let USER_HOME_DASHBOARD = "userHomeDashboard"
    static let GET_SP_CATEGORIES_SUB_CATEGORIES = "getCategorySubCategory"
    static let CHANGE_PASSWORD = "changePassword"
    static let DEACTIVATE_ACCOUNT = "deactivateAccount"
    static let ADD_NEW_AVAILABILITY = "addNewAvailability"
    static let GET_AVAILABILITY_LIST = "getAvailabilityList"
    //static let GET_AVAILABILITY_LIST_DATE_WISE = "getAvailabilityListDateWise"
    static let REMOVE_AVAILABILITY = "removeAvailability"
    static let ADD_USER_CARD = "addUserCard"
    static let GET_USER_CARD = "getUserCard"
    static let DELETE_USER_CARD = "deleteUserCard"
    static let MAKE_SESSION_BOOKING = "makeSessionBooking"
    static let CHANGE_SESSION_BOOKING_STATUS = "changeSessionBookingStatus"
    static let GET_SUBSCRIPTION_PLAN = "getSubscriptionPlans"
    static let GET_USER_SESSION_LIST = "getUserSessionList"
    static let MAKE_SUBSCRIPTION_PAYMENT = "makeSubscriptionPayment"
    static let ADD_REVIEW = "addReview"
    static let GET_PROVIDER_PAYMENT_HISTORY = "getProviderCurrentPaymentHistory"
    static let GET_USER_PAYMENT_HISTORY = "getUserPaymentHistory"
    static let GET_SERVICE_PROVIDER_LIST = "getServiceProviderList"
    static let GET_PAGES = "getPages"
    static let GET_SP_DETAIL = "getServiceProviderDetail"
    static let GET_USER_REVIEW_LIST = "getUserReviewList"
    static let GET_SESSION_DETAIL = "getSessionDetail"
    static let IS_CUSTOMER_SUBSCRIBED = "IsCustomerSubscribed"
    static let SP_HOME_DASHBOARD = "providerHomeDashboard"
    static let START_CONVERSATION = "startConversation"
    static let GET_CONVERSATION_LIST = "getConversationList"
    static let LAST_CONVERSATION = "lastConversation"
    static let GET_NOTIFICATIONS = "getNotificationList"
    static let DELETE_NOTIFICATION = "deleteNotification"
    static let UNSUBSCRIBE_PLAN = "unsubscribePlan"
    static let READ_MSG = "readMessages"
    static let SEND_OTP = "sendOTP"
  //  static let AGORA_TOKEN = "AgoraToken"
}

struct PARAMS {
    static let device_type = "device_type"
    static let device_token = "device_token"
    static let mobile_no = "mobile_no"
    static let country_code = "country_code"
    static let password = "password"
    static let user_type = "user_type"
    static let email_id = "email_id"
    static let first_name = "first_name"
    static let last_name = "last_name"
    static let address = "address"
    static let loginuser_id = "loginuser_id"
    static let session_token = "session_token"
    static let category_id = "category_id"
    static let sub_category_ids = "sub_category_ids"
    static let year_of_experience = "year_of_experience"
    static let about_you = "about_you"
    static let old_password = "old_password"
    static let new_password = "new_password"
    static let business_name = "business_name"
    static let degree_certificate = "degree_certificate"
    static let availability_date = "availability_date"
    static let availability_from_time = "availability_from_time"
    static let total_availability_min = "total_availability_min"
    static let break_time = "break_time"
    static let availability_id = "availability_id"
    static let sub_category_id = "sub_category_id"
    static let card_number = "card_number"
    static let holder_name = "holder_name"
    static let expiry_date = "expiry_date"
    static let cvv = "cvv"
    static let card_id = "card_id"
    static let booking_date = "booking_date"
    static let description = "description"
    static let service_provider_id = "service_provider_id"
    static let booking_id = "booking_id"
    static let session_status = "session_status"
    static let latitude = "latitude"
    static let longitude = "longitude"
    static let subscription_plan_id = "subscription_plan_id"
    static let page_type = "page_type"
    static let is_save_card = "is_save_card"
    static let rating = "rating"
    static let country_code_info = "country_code_info"
    static let user_id = "user_id"
    static let session_id = "session_id"
    static let search_keyword = "search_keyword"
    static let sender_id = "sender_id"
    static let sender_type = "sender_type"
    static let recipient_id = "recipient_id"
    static let timezone = "timezone"
    static let conversation_id = "conversation_id"
    static let lastMsg = "lastMsg"
    static let notification_id = "notification_id"
    static let user_name = "user_name"
    //static let channelName = "channelName"
   // static let uid = "uid"
}
class PreferenceHelper {
    static let shared = PreferenceHelper()
    let ph = UserDefaults.standard
    func setUserType(value:String) {
        ph.setValue(value, forKey: UserDefaultKey.userType)
        ph.synchronize()
    }
    func getUserType() -> String {
        return ph.value(forKey: UserDefaultKey.userType) as? String ?? ""
    }
    func setDeviceToken(_ value:String){
        ph.setValue(value, forKey: UserDefaultKey.deviceToken)
        ph.synchronize()
    }
    func getDeviceToken()->String {
        return ph.value(forKey: UserDefaultKey.deviceToken) as? String ?? "1234"
    }
    func setUserId(_ value:String){
        ph.setValue(value, forKey: "UserId")
        ph.synchronize()
    }
    func getUserId()->String {
        return ph.value(forKey: "UserId") as? String ?? ""
    }
    func setSessionToken(_ value:String){
        ph.setValue(value, forKey: "SessionToken")
        ph.synchronize()
    }
    func getSessionToken()->String {
        return ph.value(forKey: "SessionToken") as? String ?? ""
    }
    func setUserName(_ value:String){
        ph.setValue(value, forKey: "UserName")
        ph.synchronize()
    }
    func getUserName()->String {
        return ph.value(forKey: "UserName") as? String ?? ""
    }
    func setUserPic(_ value:String){
        ph.setValue(value, forKey: "UserPic")
        ph.synchronize()
    }
    func getUserPic()->String {
        return ph.value(forKey: "UserPic") as? String ?? ""
    }
    func setIsSubscription(_ value:Bool) {
        ph.setValue(value, forKey: "IsSubscription")
        ph.synchronize()
    }
    func getIsSubscription()->Bool {
        return ph.value(forKey: "IsSubscription") as? Bool ?? true
    }
    
}

struct FontHelper {
    
    enum FontStyle:String {
        case SemiBold = "Semibold"
        case Bold = "Bold"
        case Regular = ""
    }
    enum FontSize:CGFloat{
        case Regular = 17.0
        case Medium = 20.0
        case Large = 22.0
    }
    static func Font(size:CGFloat=FontSize.Regular.rawValue,style:FontStyle=FontStyle.Regular)->UIFont{
        if style.rawValue.isEmpty {
            return UIFont.init(name: "SegoeUI", size: size)!
        } else {
            return UIFont.init(name: "SegoeUI-\(style.rawValue)", size: size)!
        }
    }
    
    
    
}

class AppSingleton {
    static let shared = AppSingleton()
    var user:DataUser?
    var bookingObject:BookingObject = BookingObject()
    var filterObject:FilterSPObject = FilterSPObject()
}

class BookingObject {
    var category_id = ""
    var sub_category_id = ""
    var booking_date = ""
    var availability_id = ""
    var description = ""
    var service_provider_id = ""
    var available_dates:[String] = []
}
class FilterSPObject {
    var category_id = ""
    var sub_category_id = ""
    var rating = ""
    var latitude = ""
    var longitude = ""
    var search_keyword = ""
    var isFilter = false
    func resetFilter(){
        category_id = ""
        sub_category_id = ""
        rating = ""
        latitude = ""
        longitude = ""
        search_keyword = ""
        isFilter = false
    }
}
class RegisterSingleton {
    static let shared = RegisterSingleton()
    var country_code = ""
    var mobile_no = ""
    var password = ""
    var email_id = ""
    var first_name = ""
    var last_name = ""
    var address = ""
    var category_id = ""
    var sub_category_ids = ""
    var year_of_experience = ""
    var about_you = ""
    var business_name = ""
    var documentData:Data?
    var isPdf = false
    var latitude = ""
    var longitude = ""
    var country_code_info = ""
    var degree_certificate = ""
}

class Parser {
    
    static func isSuccess(response:[String:Any],isErrorToast:Bool=true,isSuccessToast:Bool=false)->Bool {
        if let status = response["status"] as? Int {
            if status == 1 {
                if isSuccessToast {
                    Common().showAlert(strMsg: response["msg"] as? String ?? "data submited successully", view: Common.keyWindow!.rootViewController!)
                }
                return true
            } else {
                if (response["msg"] as? String ?? "") == "session expired." {
                    appDel.gotoLogin()
                }
                if isErrorToast {
                    Common().showAlert(strMsg: response["msg"] as? String ?? "something went wrong!", view: Common.keyWindow!.rootViewController!)
                }
                
                return false
            }
        }
        return false
    }
}
