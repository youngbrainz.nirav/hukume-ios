//
//  AppDelegate.swift
//  DoctorUser
//
//  Created by VATSAL on 12/19/21.
//

import UIKit
import IQKeyboardManagerSwift
import GooglePlaces
import GoogleMaps
import Firebase
import FirebaseDatabase
import FirebaseStorage
import UserNotifications
import FirebaseMessaging

@main
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var ref:DatabaseReference!
    let gcmMessageIDKey = "gcm.message_id"
    var isAppInBackground = false
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        ref = Database.database().reference(withPath: "Conversation")
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        GMSServices.provideAPIKey(googleAPIKey)
        GMSPlacesClient.provideAPIKey(googleAPIKey)
        setupPushNotification(application, launchOptions)
        application.applicationIconBadgeNumber = 0
        let pushNotificationData = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable:Any]
        if pushNotificationData != nil {
            self.manageAllPushNotification(data: pushNotificationData as! [String:AnyHashable])
        }
        return true
    }
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            return .portrait
        case .pad:
            return .all
        
        @unknown default:
            return .portrait
        }
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      //  Messaging.messaging().apnsToken = deviceToken
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        //preferenceHelper.setDeviceToken(token)
    }
    func setupPushNotification(_ application:UIApplication, _ launchOptions:[UIApplication.LaunchOptionsKey:Any]?) {
        let notification = UNUserNotificationCenter.current()
        notification.delegate = self
        notification.requestAuthorization(options: [.alert, .sound, .badge]) { (isGrant, error) in
            if error == nil {
                if isGrant {
                    DispatchQueue.main.async {
                        application.registerForRemoteNotifications()
                    }
                    
                }
            }
        }
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        if let nav = self.window?.rootViewController as? UINavigationController {
            if nav.visibleViewController is HomeVC {
                let homevc = nav.visibleViewController as! HomeVC
                homevc.wsUserHomeDashboard()
            } else if nav.visibleViewController is SPHomeVC {
                let homevc = nav.visibleViewController as! SPHomeVC
                homevc.wsGetHomeDashboard()
            }
            else if nav.visibleViewController is NotificationListVC {
                let notivc = nav.visibleViewController as! NotificationListVC
                notivc.wsGetNotifications()
            } else if nav.visibleViewController is ChatVC {
                let chatvc = nav.visibleViewController as! ChatVC
                
                if let cId = userInfo["conversation_id"] {
                    if chatvc.conversationId == "\(cId)" {
                        return
                        
                    }
                }
            } else if nav.visibleViewController is ChatListVC {
                let vc = nav.visibleViewController as! ChatListVC
                if let tag = userInfo["tag"] {
                    if "\(tag)" == PUSH_NOTIFICATION.user_chat || "\(tag)" == PUSH_NOTIFICATION.provider_chat {
                        vc.wsGetChatList()
                    }
                }
                
            } else if nav.visibleViewController is SPSessionDetailsVC {
                let vc = nav.visibleViewController as! SPSessionDetailsVC
                vc.wsGetSessionDetail()
            }
            else if nav.visibleViewController is SessionDetailsVC {
               let vc = nav.visibleViewController as! SessionDetailsVC
               vc.wsGetSessionDetail()
           }
            else if nav.visibleViewController is VideoChatViewController {
               let vc = nav.visibleViewController as! VideoChatViewController
                if let tag = userInfo["tag"] {
                    if "\(tag)" == PUSH_NOTIFICATION.complete_session_user || "\(tag)" == PUSH_NOTIFICATION.complete_session_provider {
                        vc.btnBackAction(self)
                    }
                }
           }
           
        }
        completionHandler([.alert, .sound])
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
        let dicData:[String:String] = ["token":fcmToken ?? "1234"]
        NotificationCenter.default.post(name: NSNotification.Name("FCMToken"), object: nil, userInfo: dicData)
        preferenceHelper.setDeviceToken(fcmToken ?? "1234")
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        // ...
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print full message.
        print(userInfo)
        manageAllPushNotification(data: userInfo as! [String:AnyHashable])
        
        completionHandler()
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
//        if application.applicationState == .background {
//            isAppInBackground = true
//        }
        // Print full message.
        print(userInfo)
        if let nav = self.window?.rootViewController as? UINavigationController {
            if nav.visibleViewController is HomeVC {
                let homevc = nav.visibleViewController as! HomeVC
                homevc.wsUserHomeDashboard()
            } else if nav.visibleViewController is SPHomeVC {
                let homevc = nav.visibleViewController as! SPHomeVC
                homevc.wsGetHomeDashboard()
            }
            else if nav.visibleViewController is NotificationListVC {
                let notivc = nav.visibleViewController as! NotificationListVC
                notivc.wsGetNotifications()
            }
            else if nav.visibleViewController is ChatVC {
                let chatvc = nav.visibleViewController as! ChatVC
                if let cId = userInfo["conversation_id"] {
                    if chatvc.conversationId == "\(cId)" {
                        return
                        
                    }
                }
            } else if nav.visibleViewController is ChatListVC {
                let vc = nav.visibleViewController as! ChatListVC
                vc.wsGetChatList()
            }
            else if nav.visibleViewController is SPSessionDetailsVC {
               let vc = nav.visibleViewController as! SPSessionDetailsVC
               vc.wsGetSessionDetail()
           }
           else if nav.visibleViewController is SessionDetailsVC {
              let vc = nav.visibleViewController as! SessionDetailsVC
              vc.wsGetSessionDetail()
          }
        }
        completionHandler(UIBackgroundFetchResult.newData)
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
//        if isAppInBackground {
           // isAppInBackground = false
            if let nav = self.window?.rootViewController as? UINavigationController {
                if nav.visibleViewController is HomeVC {
                    let homevc = nav.visibleViewController as! HomeVC
                    homevc.wsUserHomeDashboard()
                } else if nav.visibleViewController is SPHomeVC {
                    let homevc = nav.visibleViewController as! SPHomeVC
                    homevc.wsGetHomeDashboard()
                }
                else if nav.visibleViewController is NotificationListVC {
                    let notivc = nav.visibleViewController as! NotificationListVC
                    notivc.wsGetNotifications()
                } else if nav.visibleViewController is ChatListVC {
                    let vc = nav.visibleViewController as! ChatListVC
                    vc.wsGetChatList()
                }
                else if nav.visibleViewController is SPSessionDetailsVC {
                   let vc = nav.visibleViewController as! SPSessionDetailsVC
                   vc.wsGetSessionDetail()
               }
               else if nav.visibleViewController is SessionDetailsVC {
                  let vc = nav.visibleViewController as! SessionDetailsVC
                  vc.wsGetSessionDetail()
              }
            }
//        }
    }
    
    func manageAllPushNotification(data:[String : AnyHashable]) {
        if UIApplication.shared.applicationIconBadgeNumber == 0{
            
        }else{
            UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber - 1
        }
        if let id = data["tag"] {
            let id = "\(id)"
            switch id {
            case PUSH_NOTIFICATION.book_session:
                
                if let session_id = data["session_id"] {
                    self.gotoSessionDetail(id: "\(session_id)", isPastSession: false)
                }
                break
            case PUSH_NOTIFICATION.start_session:
                if let session_id = data["session_id"] {
                    self.gotoSessionDetail(id: "\(session_id)", isPastSession: false,isSP: false)
                }
                break
            case PUSH_NOTIFICATION.user_chat:
                gotoChat(data: data,isSP: false)
                break
            case PUSH_NOTIFICATION.provider_chat:
                gotoChat(data: data)
                break
            case PUSH_NOTIFICATION.admin_approve_provider_account:
                self.gotoHome()
                break
            case PUSH_NOTIFICATION.complete_session_provider:
                if let session_id = data["session_id"] {
                    self.gotoSessionDetail(id: "\(session_id)", isPastSession: true)
                }
                break
            case PUSH_NOTIFICATION.complete_session_user:
                if let session_id = data["session_id"] {
                    self.gotoSessionDetail(id: "\(session_id)", isPastSession: true,isSP: false)
                }
                break
            case PUSH_NOTIFICATION.cancel_session_provider:
                if let session_id = data["session_id"] {
                    self.gotoSessionDetail(id: "\(session_id)", isPastSession: true)
                }
                break
            case PUSH_NOTIFICATION.cancel_session_user:
                if let session_id = data["session_id"] {
                    self.gotoSessionDetail(id: "\(session_id)", isPastSession: true,isSP: false)
                }
                break
            case PUSH_NOTIFICATION.session_reminder_user:
                if let session_id = data["session_id"] {
                    self.gotoSessionDetail(id: "\(session_id)", isPastSession: true,isSP: false,isStartSession: true)
                }
                break
            case PUSH_NOTIFICATION.session_reminder_provider:
                if let session_id = data["session_id"] {
                    self.gotoSessionDetail(id: "\(session_id)", isPastSession: true,isStartSession: true)
                }
                break
                
            default:
                break
            }
        }
    }
    func gotoSessionDetail(id:String,isPastSession:Bool,isSP:Bool=true,isStartSession:Bool=false) {
        
        if isSP {
            let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "SPSessionDetailsVC") as! SPSessionDetailsVC
            
            vc.isPastSession = isPastSession
            vc.sessionId = id
            vc.isStartSession = isStartSession
            if self.window?.rootViewController is UINavigationController {
                
                let nav = self.window?.rootViewController as! UINavigationController
                if let spvc = nav.visibleViewController as? SPSessionDetailsVC {
                    if spvc.sessionId == id {
                        spvc.wsGetSessionDetail()
                        return
                        
                    }
                }
                nav.pushViewController(vc, animated: true)
            } else {
                
                vc.isFromPush = true
                let nav = UINavigationController.init(rootViewController: vc)
                nav.isNavigationBarHidden = true
                self.window?.rootViewController = nav
                self.window?.makeKeyAndVisible()
                
                
            }
        }
        else {
            
            let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "SessionDetailsVC") as! SessionDetailsVC
            //
            vc.isPastSession = isPastSession
            vc.sessionId = id
            vc.isStartSession = isStartSession
            if self.window?.rootViewController is UINavigationController {
                let nav = self.window?.rootViewController as! UINavigationController
                if let spvc = nav.visibleViewController as? SessionDetailsVC {
                    if spvc.sessionId == id {
                        spvc.wsGetSessionDetail()
                        return
                        
                    }
                }
                nav.pushViewController(vc, animated: true)
            } else {
                vc.isFromPush = true
                let nav = UINavigationController.init(rootViewController: vc)
                nav.isNavigationBarHidden = true
                self.window?.rootViewController = nav
                self.window?.makeKeyAndVisible()
                
            }
        }
        
    }
    func gotoChat(data:[AnyHashable : Any],isSP:Bool=true){
        
        
        //guard let data1 = data["chat_data"] as? [String:Any], let c_id = data1["conversation_id"] as? String, let r_id = data1["user_id"] as? String, let r_name = data1["sender_name"] as? String else {return}
        guard let c_id = data["conversation_id"], let r_id = data["user_id"], let r_name = data["user_name"] else {return}
        if self.window?.rootViewController is UINavigationController {
            let nav = self.window?.rootViewController as! UINavigationController
            
            if let cvc = nav.visibleViewController as? ChatVC {
                if "\(c_id)" == cvc.conversationId {
                    return
                }
            }
        }
        if isSP {
            let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            vc.conversationId = "\(c_id)"
            vc.recipient_id = "\(r_id)"
            vc.receiver_name = "\(r_name)"
            if self.window?.rootViewController is UINavigationController {
                let nav = self.window?.rootViewController as! UINavigationController
                nav.pushViewController(vc, animated: true)
            }else {
                vc.isFromPush = true
                let nav = UINavigationController.init(rootViewController: vc)
                nav.isNavigationBarHidden = true
                self.window?.rootViewController = nav
                self.window?.makeKeyAndVisible()
            }
        } else {
            let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            vc.conversationId = "\(c_id)"
            vc.recipient_id = "\(r_id)"
            vc.receiver_name = "\(r_name)"
            if self.window?.rootViewController is UINavigationController {
                let nav = self.window?.rootViewController as! UINavigationController
                nav.pushViewController(vc, animated: true)
            }else {
                vc.isFromPush = true
                let nav = UINavigationController.init(rootViewController: vc)
                nav.isNavigationBarHidden = true
                self.window?.rootViewController = nav
                self.window?.makeKeyAndVisible()
            }
        }
        
    }
    func gotoHome(){
        
        if preferenceHelper.getUserType() == UserDefaultKeyForValue.Customeruser{
            let vc = UIStoryboard(name: STORYBOARDNAME.Dashboard, bundle: nil).instantiateInitialViewController()
            Common.keyWindow!.rootViewController = vc
        }else{
            if preferenceHelper.getIsSubscription() {
                let vc = UIStoryboard(name: STORYBOARDNAME.SPDashboard, bundle: nil).instantiateInitialViewController()
                Common.keyWindow!.rootViewController = vc
            } else {
                
                let vc = UIStoryboard(name: STORYBOARDNAME.SPPayment, bundle: nil).instantiateViewController(withIdentifier: "SubscriptionVC") as! SubscriptionVC
                let nav = UINavigationController.init(rootViewController: vc)
                nav.isNavigationBarHidden = true
                Common.keyWindow!.rootViewController = nav
            }
        }
    }
    func gotoLogin(){
        preferenceHelper.setUserType(value: "")
        preferenceHelper.setUserId("")
        preferenceHelper.setSessionToken("")
        let homeViewController = UIStoryboard(name: STORYBOARDNAME.Authentication, bundle: nil).instantiateViewController(withIdentifier: "UserSelectionVC") as! UserSelectionVC
        let nav = UINavigationController(rootViewController: homeViewController)
        Common.keyWindow!.rootViewController = nav
        nav.isNavigationBarHidden = true
    }
    
}

struct PUSH_NOTIFICATION {
    static let book_session = "1"
    static let start_session = "2"
    static let user_chat = "4"
    static let provider_chat = "3"
    static let admin_approve_provider_account = "5"
    static let complete_session_provider = "6"
    static let complete_session_user = "7"
    static let cancel_session_provider = "8"
    static let cancel_session_user = "9"
    static let session_reminder_user = "11"
    static let session_reminder_provider = "10"
    
    
}
